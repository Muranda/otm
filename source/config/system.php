<?php
return [
    "APP_NAME" => env('APP_NAME'),
    "APP_ENV" => env('APP_ENV'),
    "APP_DEBUG" => env('APP_DEBUG'),
    "APP_URL" => env('http://sandbox.propertybook.co.zw/otm')
];
