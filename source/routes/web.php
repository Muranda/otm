<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SiteController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[SiteController::class,'index'])->name('homePage');


Route::get('/competition',[SiteController::class,'competition'])->name('competition');

Route::get('/new-look',[SiteController::class,'newLook'])->name('newLook');

Route::post('/competition-sub',[SiteController::class,'competitionSub'])->name('competitionSub');

Route::get('/searchLocations',[SiteController::class,'propertyLocationSearch'])->name('searchLocations');

Route::get('/sell-with-us',function (){
    return redirect('');//TODO redirect to cloud registration page
})->name('sellWithUs');

Route::get('/home2',[SiteController::class,'home'])->name('home2');

Route::get('/record-stats',[SiteController::class,'recordStats'])->name('recordStats');

Route::get('/report-property',[SiteController::class,'reportProperty'])->name('reportProperty');

Route::get('{filter}',[SiteController::class,'listings'])
    ->where('filter','for-sale|to-rent')
    ->name('allListings');

Route::get('/all-properties/{filter}',[SiteController::class,'allProperties'])->name('all-properties');

Route::get('/propertyLocationSearch',[SiteController::class,'propertyLocationSearch'])->name('propertyLocationSearch');

Route::get('/propertyAgencySearch',[SiteController::class,'propertyAgencySearch'])->name('propertyAgencySearch');

Route::match(['get','post'],'/findProperties/{filter}',[SiteController::class,'findProperties'])->name('findProperties');

Route::get('/properties/{filter}/{province}/{town?}/{area?}/{suburb?}',[SiteController::class,'filterProperties'])
    ->where('filter','for-sale|to-rent')
    ->name('filterProperties');
Route::post('/save-search',[SiteController::class,'saveSearch'])->name('saveSearch');

Route::get('/cancel-alert/{alertID}',[SiteController::class,'cancelAlert'])->name('cancelAlert');

Route::post('/searchDevelopments',[SiteController::class,'searchDevelopments'])->name('searchDevelopments');

Route::get('/developments/{search?}',[SiteController::class,'getDevelopments'])->name('developments');

Route::get('/commercial/{filter}',[SiteController::class,'getCommercial'])->name('commercialProperties');

Route::get('/listed-agencies',[SiteController::class,'getAgencies'])->name('listedAgencies');

Route::get('/listed-agencies/{agency}',[SiteController::class,'viewAgency'])->name('viewAgency');

Route::get('/listed-agencies/{agency}/properties/{filter}',[SiteController::class,'viewAgencyProperties'])->name('viewAgencyProperties');

Route::get('/listed-agencies/{agency}/{branch}/properties/{filter?}',[SiteController::class,'viewBranch'])->name('viewBranch');

Route::post('/findAgency',[SiteController::class,'findAgency'])->name('findAgency');

Route::get('/agency-search-results',[SiteController::class,'filterAgencies'])->name('filterAgencies');

Route::get('/recordBranchContact/{type}/{branchID}',[SiteController::class,'recordBranchContact'])->name('recordBranchContact');

Route::get('/{filter}/{province}/{town}/{area}/{suburb}/{refCode}',[SiteController::class,'getPropertyWithArea'])
    ->where('filter','for-sale|to-rent')
    ->name('propertyWithArea');

Route::get('/{filter}/{province}/{town}/{suburb}/{refCode}',[SiteController::class,'getPropertyWithoutArea'])
    ->where('filter','for-sale|to-rent')
    ->name('propertyWithoutArea');

Route::get('/magazine/{mag?}',[SiteController::class,'getMagazines'])->name('magazine');

Route::get('/blog/{article?}',[SiteController::class,'getBlog'])->name('blog');

Route::post('/newsletterSubscription',[SiteController::class,'newsletterSubscription'])->name('newsletterSubscription');

Route::get('/info/{page}',[SiteController::class,'getGeneralPage'])->name('generalPage');

Route::get('/special/{type}',[SiteController::class,'specialPage'])->name('specialPage');

Route::match(['get','post'],'/voting-actions/{type}/{id?}',[SiteController::class,'votingActions'])->name('votingActions');

Route::post('/submit-feedback',[SiteController::class,'submitFeedback'])->name('submitFeedback');

Route::match(['get','post'],'/sendEnquiryEmail/{propertyID}',[SiteController::class,'sendEnquiryEmail'])->name('sendEnquiryEmail');

Route::get('/property-sliders/{propertyID}',[SiteController::class,'getPropertySliders'])->name('getPropertySliders');

Route::any('/logPropertyEnquiry/{type}/{propertyID}',[SiteController::class,'logPropertyEnquiry'])->name('logPropertyEnquiry');

Route::get('/change-currency',[SiteController::class,'changeCurrency'])->name('changeCurrency');

Route::get('/view-map/{refCode}',[SiteController::class,'viewMap'])->name('viewMap');

Route::get('/advanced-search/{filter}/{search}',[SiteController::class,'advancedSearch'])->name('advancedSearch');

Route::get('/clear-app',function (){
    \Illuminate\Support\Facades\Artisan::call('cache:clear');
    \Illuminate\Support\Facades\Artisan::call('view:clear');
    \Illuminate\Support\Facades\Artisan::call('clear-compiled');
    echo 'Done';
});

Route::get('/property-list',[SiteController::class,'returnPages'])->name('propertyListing');

