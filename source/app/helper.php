<?php

use App\Jobs\LogPropertyStats;
use App\Models\Agency;
use App\Models\FeaturedView;
use App\Models\Property;
use App\Models\Suburb;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Cache;
use App\Models\PropertyAgent;
use \App\Models\User;
use Carbon\Carbon;
use App\Models\PropertyFeature;
use \App\Models\BannerAdvert;

/**
 * Return link to Cloud platform
 * @return string
 */
function cloudURL(){
    return 'https://cloud.propertybook.co.zw';
}

/**
 * Return link to Cloud platform
 * @return string
 */
function cloudRegURL(){
    return 'https://cloud.propertybook.co.zw/registration';
}

/**
 * Return link to the cloud images folder
 * @return string
 */
function imageURL(){
    return 'https://cloud.propertybook.co.zw/assets/images/';
}

function pbLink(){
    return 'https://propertybook.co.zw/';
}

/*
 * function will bring back the 5 locations that have be having the most properties
 */
function popularLocationsSidebar(){
    $suburbs = Cache::tags(['locations'])->remember('omt-sidebar-locations',60*6,function(){
        return Suburb::with([
            'area' => function ($query) {
                $query->select('id', 'name', 'slug', 'town_id');
                $query->with([
                    'town' => function ($query) {
                        $query->select('id', 'slug', 'province_id');
                        $query->with([
                            'province' => function ($query) {
                                $query->select('id', 'slug');
                            }
                        ]);
                    }
                ]);
            }
        ])
            ->select('id', 'name', 'slug', 'area_id')
            ->withCount([
                'properties'
            ])
            ->orderBy('properties_count', 'desc')
            ->take(5)
            ->get();
    });
    return $suburbs;
}

/**
 * Return link to the cloud property images folder
 * @return string
 */
function propertyImages(){
    return imageURL().'properties/';
}

/**
 * Function will return the property size SI unit
 */
function propertySize($size){
    if($size < 4046.86){
        return $size.'sqm';
    }
    elseif($size < 10000){
        return round($size/4046.86,2).'ac';
    }
    else{
        return round($size/10000,2).'ha';
    }
}

/**
 * Return site instance ID
 * @return int
 */
function siteID(){
    return 3;
}

/**
 * @return int
 * this is will return
 */
function pbID(){
    return 1;
}

/**
 * @return array|mixed
 */
function siteData(){
    return Cache::tags('sites')->rememberForever('site_helper_'.siteID(),function (){
        return \App\Models\Site::where('id',siteID())->first();
    });
}

/**
 * Return link for a given property
 * @param Property $property
 * @return string
 */
function propertyLink(Property $property){
    return Cache::tags('property_details_'.$property->id)->rememberForever('property_details_link_'.$property->id,function () use ($property){
        if($property->suburb->area->slug == $property->suburb->slug){
            $string =  Str::slug($property->offer->label)
                .'/'.$property->suburb->area->town->province->slug
                .'/'.$property->suburb->area->town->slug
                .'/'.$property->suburb->slug
                .'/'.strtolower($property->ref_code);
        }
        else{
            $string = Str::slug($property->offer->label)
                .'/'.$property->suburb->area->town->province->slug
                .'/'.$property->suburb->area->town->slug
                .'/'.$property->suburb->area->slug
                .'/'.$property->suburb->slug
                .'/'.strtolower($property->ref_code);
        }
        return siteData()->domain.'/'.$string;
    });
}
/**
 * Return appropriate image for a property
 * @param $folder
 * @param $file
 * @param Property $property
 * @param $type
 * @return string
 */
function resolvePropertyImage($folder,$file,Property $property,$type){
    $filter = Str::slug($property->offer->label);
    if(!$file){
        //dd($property->images);
        if ($filter == 'for-sale') {
            $land = [5,6,7];
            if (in_array($property->type_id, $land)) {
                if ($type == 'standard') {
                    $link = $folder . '/land-for-sale.jpg';
                }
                elseif ($type == 'webp') {
                    $link = $folder . '/land-for-sale.webp';
                }
            } else {
                if ($type == 'standard') {
                    $link = $folder . '/for-sale.jpg';
                }
                elseif ($type == 'webp') {
                    $link = $folder . '/for-sale.webp';
                }
            }
        }
        elseif ($filter == 'to-rent') {
            if ($type == 'standard') {
                $link = $folder . '/to-rent.jpg';
            }
            elseif ($type == 'webp') {
                $link = $folder . '/to-rent.webp';
            }
        }
        return propertyImages().$link;
    }
    else{
        return Cache::tags('property_details_'.strtolower($property->ref_code))->rememberForever('property_images_'.$property->id.'_'.$folder.'_'.$file.'_'.$type,function () use ($folder,$file,$property,$type,$filter){
            if($type == 'webp'){
                $ext = substr($file, strrpos($file, '.') + 1);
                $nFile = str_replace($ext,'webp',$file);

                $handle = @fopen(propertyImages().$folder.'/'.$nFile,'r');

                if($handle){
                    $link = $folder .'/'.str_replace($ext,'webp',$nFile);
                }
                else{
                    $handle = @fopen(propertyImages().$folder.'/'.$file,'r');

                    if($handle){
                        $link = $folder . '/' . $file;
                    }
                    else{
                        if ($filter == 'for-sale') {
                            $land = [5,6,7];
                            if (in_array($property->type_id, $land)) {
                                $link = $folder . '/land-for-sale.webp';
                            } else {
                                $link = $folder . '/for-sale.webp';
                            }
                        }
                        elseif ($filter == 'to-rent') {
                            $link = $folder . '/to-rent.webp';
                        }
                    }
                }
            }
            else{
                $handle = @fopen(propertyImages().$folder.'/'.$file,'r');
                if($handle){
                    $link = $folder . '/' . $file;
                }
                else{
                    if ($filter == 'for-sale') {
                        $land = [5,6,7];
                        if (in_array($property->type_id, $land)) {
                            $link = $folder . '/land-for-sale.jpg';
                        } else {
                            $link = $folder . '/for-sale.jpg';
                        }
                    }
                    elseif ($filter == 'to-rent') {
                        $link = $folder . '/to-rent.webp';
                    }
                }
            }
            return propertyImages().$link;
        });
    }
}
/**
 * Display ribbon on property
 * @param $status
 * @param $filter
 * @param $created_at
 * @param string $property
 * @return string
 */
function propertyStatusRibbon($status, $filter, $created_at, $property = '')
{
    if ($status == 2) {
        if (strtotime($created_at . ' +2 weeks') >= strtotime('now')) {
            return '<div class="ribbon new' . $property . '">new</div>';
        }
    } elseif ($status == 3) {
        if ($filter == 'for-sale') {
            return '<div class="ribbon taken' . $property . '">sold</div>';
        } elseif ($filter == 'to-rent') {
            return '<div class="ribbon taken' . $property . '">rented</div>';
        }
    } elseif ($status == 4) {
        return '<div class="ribbon new' . $property . '">reduced price</div>';
    } elseif ($status == 5) {
        return '<div class="ribbon new' . $property . '">on show</div>';
    } elseif ($status == 6) {
        return '<div class="ribbon holding' . $property . '">on hold</div>';
    } elseif ($status == 7) {
        return '<div class="ribbon holding' . $property . '">offer pending</div>';
    } elseif ($status == 8) {
        return '<div class="ribbon sole' . $property . '">sole mandate</div>';
    }
}

/**
 * Display appropriate property price
 * @param Property $property
 * @param null $console
 * @return string
 */
function displayPrice(Property $property,$console = null)
{
    if ($property->offer_id == 5 || $property->offer_id == 6 || $property->usd_price == 0) {
        return 'POA';
    }
    else {
        if(!$console){
            if(Session::get('selected_currency') == 'usd'){
                $price = 'USD '.number_format($property->usd_price);
                $pref = 'usd';
            }
            else{
                $price = 'ZWL '.number_format($property->zwl_price);
                $pref = 'zwl';
            }
        }
        else{
            $price = 'USD '.number_format($property->usd_price);
            $pref = 'zwl';
        }
        return Cache::tags('property_details_'.$property->id)->rememberForever('property_price_'.$property->id.'_'.$pref,function () use($property,$console,$price){
            return $price.$property->offer->suffix;
        });
    }

}

/**
 * Get webp version of an image
 * @param $type
 * @param $image
 * @return string|string[]
 */
function getWebPImage($type, $image)
{
    $ext = substr($image, strrpos($image, '.') + 1);
    if ($type == 'agencyLogo') {
        $link = imageURL().'agency-logos/'.$image;
    } elseif ($type == 'magazine') {
        $link = URL::asset('/assets/images/magazines/' . $image);
    } elseif ($type == 'blog-main') {
        $link = imageURL().'blog/'.$image;
    } elseif ($type == 'blog-thumb') {
        $link = imageURL().'/blog/thumbnail/'.$image;
    } elseif ($type == 'general-page') {
        $link = imageURL().'/pages/'.$image;
    }
    return  str_replace($ext,'webp',$link);
}

function saveFeaturedView($propertyID){
    $view = new FeaturedView();
    $view->site_id = siteID();
    $view->property_id = $propertyID;
    $view->session_id = Session::getId();
    $view->save();
}

/**
 * Return values for area dropdown on search
 * @return array
 */
function areaFilters(){
    return [1,1.5,2,2.5,3,4,5,10];
}

/**
 * Append prefix to ref code
 * @param $refCode
 * @return string
 */
function displayRefCode($refCode){
    return 'PB-'.$refCode;
}

/**
 * Record a stat for a property
 * @param $type
 * @param Property $property
 */
function logPropertyStat($type,Property $property){
    dispatch((new LogPropertyStats($type, $property))->onQueue('low-priority'));
    return '';
}

/**
 * Check for an agency's branches
 * @param $type
 * @param Agency $agency
 * @return string
 */
function checkForBranches($type, Agency $agency)
{
    if ($type == 'desktop') {
        $text = 'View Branches';
    } elseif ($type == 'mobile') {
        $text = 'Branches';
    }

    if (count($agency->branches) > 1) {
        return '<a href="' . route('viewAgency', ['agency' => $agency->slug]) . '" class="btn btn-pb-outline">' . $text . '</a>';
    }
}

/**
 * Get the count of properties for an agency/branch
 * @param $level
 * @param $type
 * @param $id
 * @return mixed
 */
function getAgencyPropertyCount($level, $type, $id)
{
    if ($level == 'agency') {
        $properties = Property::select('id')
            ->whereHas('agents',function($query) use ($id){
                $query->select('property_id','agent_1');
                $query->whereHas('agentDetail',function($query) use ($id){
                    $query->select('user_id');
                    $query->where('agency_id',$id);
                });
            })
            ->whereHas('offer',function($query) use ($type){
                $query->select('id');
                $query->where('label',$type);
            })
            ->whereHas('statuses',function ($query){
                $query->where('status_id','>',1);
            })
            ->whereHas('sites',function ($query){
                $query->where('site_id',siteID());
            })
            ->get()
            ->count();
    } elseif ($level == 'branch') {
        $properties = Property::select('id')
            ->whereHas('agents',function($query) use ($id){
                $query->select('property_id','agent_1');
                $query->whereHas('agentDetail',function($query) use ($id){
                    $query->select('user_id');
                    $query->where('branch_id',$id);
                });
            })
            ->whereHas('offer',function($query) use ($type){
                $query->select('id');
                $query->where('label',$type);
            })
            ->whereHas('statuses',function ($query){
                $query->where('status_id','>',1);
            })
            ->whereHas('sites',function ($query){
                $query->where('site_id',siteID());
            })
            ->get()
            ->count();
    }
    return $properties;
}

function generatePropertyTitle($type_id,$type,$label,$suburb){
    $residential = [1,4,5,8,9,17];
    return $type.' '.$label.' in '.$suburb;
}

/**
 * Convert a given string to slug text that can be applied to a DB column
 * @param $key -the supplied string
 * @return string|string[]
 */
function dbColumn($key){
    return str_replace(' ','_',str_replace('/','_',strtolower($key)));
}

/**
 * Return details for agents for a property
 * @param PropertyAgent $propertyAgent
 * @param $refCode
 * @param $title
 * @param $link
 * @return string
 */
function propertyAgentDetails(PropertyAgent $propertyAgent,$refCode,$title,$link){
    $details = '';
    foreach ($propertyAgent->toArray() as $key => $value){
        if(!in_array($key,['id','property_id','created_at','updated_at','user','agent_detail']) && $value){
            $user = User::select('name','surname','email','phone_number','country_code')->where('id',$value)->first();
            if(!empty($user)){
                $text = 'Hi '.$user->name.'. Please contact me about *'.$refCode.'* listed on'. config('system.APP_URL') .' - '.$title.' - '.$link;
                $details .= '<div class="agent-contacts">';
                    $details .= '<div class="author-name">';
                        $details .= $user->name.' '.$user->surname.'<br>';
                    $details .= '</div>';
                    $details .= '<div class="agent-detail">';
                        $details .= ' <span class="mobile-number btn">'.$user->country_code.($user->country_code == '263' ? ' '.substr($user->phone_number,0,3).' '.substr($user->phone_number,3,3).' '.substr($user->phone_number,6) : $user->phone_number).'</span><br>';
                        $details .= '<a class="call-btn " onclick="logClick(\'click_to_call\',\''.str_replace(' ','',$user->country_code.$user->phone_number).'\',\''.$text.'\')"><i class="fa fa-phone"></i> Call</a>';
                        $details .= '<a class="whatsapp-btn" onclick="logClick(\'click_to_whatsapp\',\''.str_replace(' ','',$user->country_code.$user->phone_number).'\',\''.$text.'\')"><i class="fab fa-whatsapp"></i> WhatsApp</a>';
                    $details .= '</div>';
                    $details .= '</hr>';
                $details .= '</div>';
            }
        }
    }
    return $details;
}

function showReportButton($created_at){
    $date = Carbon::parse($created_at);
    $today = Carbon::now();

    $diff = $date->diffInDays($today);

    return ($diff > 30 ? true : false);
}

/**
 * Generate a link for sharing a property to WhatsApp
 * @param $link
 * @param $title
 * @param null $type
 * @return string
 */
function createWhatsAppLink($link,$title,$type = null){
    if($type == null){
        $text = 'Hi, check out this property - '.$title.' - '.$link;
    }
    else{
        $text = 'Hi, check out '.$title.' - '.$link;
    }

    return 'https://wa.me/?text='.urlencode($text);
}

/**
 * Return priority listing properties to display in the sidebar
 * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Query\Builder[]|\Illuminate\Support\Collection|Property[]
 */
function sidebarPriorityListings(){
    return Property::with([
        'offer:id,label,suffix',
        'suburb' => function ($query) {
            $query->select('id', 'name', 'slug', 'area_id');
            $query->with([
                'area' => function ($query) {
                    $query->select('id', 'name', 'slug', 'town_id');
                    $query->with([
                        'town' => function ($query) {
                            $query->select('id', 'name', 'slug', 'province_id');
                            $query->with([
                                'province' => function ($query) {
                                    $query->select('id', 'slug');
                                }
                            ]);
                        }
                    ]);
                }
            ]);
        },
        'type:id,type',
        'agents' => function ($query) {
            $query->select('id', 'property_id', 'agent_1');
            $query->with([
                'agentDetail' => function ($query) {
                    $query->select('user_id', 'agency_id');
                    $query->with([
                        'agency' => function ($query) {
                            $query->select('id','name','slug','logo','hex_code','text_color','overlay');
                            $query->with([
                                'settings' => function ($query) {
                                    $query->select('agency_id', 'custom_rate', 'rate');
                                }
                            ]);
                        }
                    ]);
                }
            ]);
        },
        'features:property_id,bedrooms,lounges,bathrooms,boreholes',
        'images:property_id,main_image,image_1,image_2,image_3,image_4'
    ])
        ->select('id', 'suburb_id', 'type_id', 'offer_id', 'ref_code', 'description', 'created_at', 'zwl_price','usd_price')
        ->whereHas('statuses',function ($query){
            $query->where('status_id','>',1);
        })
        ->whereHas('priority',function ($query){
            $query->where('site_id',siteID());
        })
        ->orderBy(DB::raw('RAND()'))
        ->take(5)
        ->get();
}

/**
 * Return developer admins
 * @return mixed
 */
function devAdmins(){
    return User::whereIn('id',config('system.DEV_ADMINS'))->get();
}

function generateIframe(string $type,string $link){
    if($type == 'youtube'){
        $video_id = explode("?v=", $link);
        if (empty($video_id[1])){
            $video_id = explode("/v/", $link);
        }
        else{
            $video_id = explode("&", $video_id[1]);
        }

        $video_id = $video_id[0];
        $vid_link = 'https://www.youtube.com/embed/'.$video_id;
    }
    else{
        $video_id = explode("?v=", $link);
        if (empty($video_id[1])){
            $video_id = explode("&v=", $link);
        }
        else{
            $video_id = explode("&", $video_id[1]);
        }

        $video_id = $video_id[0];
        $vid_link = 'http://www.facebook.com/video/embed?video_id='.$video_id;
    }
    return '<iframe width="560" height="315" src="'.$vid_link.'" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
}

function iconDisplay(PropertyFeature $feature){
    $features = [];

    if($feature->bedrooms){
        $features['bedrooms'] = $feature->bedrooms;
    }

    if($feature->bathrooms){
        $features['bathrooms'] = $feature->bathrooms;
    }

    if($feature->lounges){
        $features['lounges'] = $feature->lounges;
    }

    if($feature->boreholes){
        $feature['boreholes'] = $feature->boreholes;
    }

    return $features;
}

function checkBannerDisplay($position){
    return Cache::tags('banners')->remember('banner_advert_'.siteID().'_'.$position,20*60,function () use ($position){
        return BannerAdvert::whereHas('addOnSite',function ($query) use ($position){
            $query->where('addon_id',$position);
        })
            ->where('start_date','<=',date('Y-m-d'))
            ->where('end_date','>=',date('Y-m-d'))
            ->where('approved',1)
            ->first();
    });
}
