<?php


namespace App\Http\ViewComposers;

use App\Models\BannerAdvert;
use App\Models\PriceFilter;
use App\Models\PropertyType;
use App\Models\Rate;
use App\Models\Site;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;


class SystemComposer
{
    public function compose(View $view)
    {
        $site = Cache::tags('sites')->rememberForever('site_'.siteID(),function (){
            return Site::where('id',siteID())->firstOrFail();
        });


        $propertyTypes = Cache::tags('property_types')->rememberForever('property_types',function (){
            return PropertyType::select('id','type')
                ->orderBy('type','asc')
                ->get();
        });

        $banners = Cache::tags('banners')->remember('site_banners_'.siteID(),6*60,function () use ($site){
            return BannerAdvert::with('addOnSite')
                ->whereHas('addOnSite',function ($query) use ($site){
                    $query->where('site_id',$site->id);
                })
                ->where('start_date','<=',date('Y-m-d'))
                ->where('end_date','>=',date('Y-m-d'))
                ->where('approved',1)
                ->get();
        });

        $sidebarAds = [
            [
                'image' => 'earthen-fire-banner.jpg',
                'link' => 'https://www.facebook.com/EarthenFire',
                'company' => 'Earthen Fire'
            ],
            [
                'image' => 'swh-banner.jpeg',
                'link' => 'http://www.swh.co.zw/',
                'company' => 'Steel Warehouse'
            ],
            [
                'image' => 'tandem-banner.jpg',
                'link' => '#',
                'company' => 'Tandem Lawn Mowers'
            ]
        ];

        $prices = Cache::tags('price_filter')->rememberForever('price_dropdown_'.siteID(),function () use ($site){
            $filters = PriceFilter::where('site_id',$site->id)
                ->orderBy('usd')
                ->get();

            if($filters->count() == 0){
                $filters = PriceFilter::whereNull('site_id')
                    ->orderBy('usd')
                    ->get();
            }
            return $filters;
        });

        if(!Session::has('selected_currency')){
            Session::put('selected_currency','usd');
        }

        $rate = Cache::tags('rates')->remember('pricing_rates',18*6,function (){
            return Rate::first();
        });


        $view->with([
            'globalPropertyTypes' => $propertyTypes,
            'priceFilters' => $prices,
            'selected_currency' => Session::get('selected_currency'),
            'globalRate' => $rate->standard,
            'voting_enabled' => false,
            'voting_year' => Carbon::now()->subYear()->format('Y'),
            'sidebarAds' => $sidebarAds,
            'banners' => $banners,
            'site' => $site
        ]);
    }
}
