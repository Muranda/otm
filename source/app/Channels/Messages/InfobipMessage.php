<?php


namespace App\Channels\Messages;


class InfobipMessage
{
    public $content;

    public function content($content){
        $this->content = $content;
        return $this;
    }
}
