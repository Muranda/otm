<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    use HasFactory;
    public function town(){
        return $this->belongsTo(Town::class,'town_id');
    }

    public function suburbs(){
        return $this->hasMany(Suburb::class,'area_id');
    }
}
