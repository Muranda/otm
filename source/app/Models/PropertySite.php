<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PropertySite extends Model
{
    use HasFactory;
    public function property(){
        return $this->belongsTo(Property::class,'property_id');
    }

    public function site(){
        return $this->belongsTo(Site::class,'site_id');
    }
}
