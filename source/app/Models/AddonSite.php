<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AddonSite extends Model
{
    use HasFactory;

    public function addOn(){
        return $this->belongsTo(Addon::class,'addon_id');
    }

    public function site(){
        return $this->belongsTo(Site::class,'site_id');
    }
}
