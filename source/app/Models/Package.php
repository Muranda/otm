<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    use HasFactory;

    public function bandSubs(){
        return $this->hasMany(BandSub::class,'package_id');
    }

    public function packageBands(){
        return $this->hasMany(PackageBand::class,'package_id');
    }
}
