<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BandSub extends Model
{
    use HasFactory;

    public function agency(){
        return $this->belongsTo(Agency::class,'agency_id');
    }

    public function band(){
        return $this->belongsTo(Band::class,'band_id');
    }

    public function package(){
        return $this->belongsTo(Package::class,'package_id');
    }
}
