<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Staudenmeir\EloquentHasManyDeep\HasRelationships;
class Province extends Model
{
    use HasRelationships,HasFactory;
    public function towns(){
        return $this->hasMany(Town::class,'province_id');
    }

    public function properties(){
        return $this->hasManyDeep(Property::class,[Town::class,Area::class,Suburb::class],['province_id','town_id','area_id']);
    }
}
