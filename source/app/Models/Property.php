<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    use HasFactory;
    public function offer(){
        return $this->belongsTo(OfferType::class,'offer_id');
    }

    public function type(){
        return $this->belongsTo(PropertyType::class,'type_id');
    }

    public function suburb(){
        return $this->belongsTo(Suburb::class,'suburb_id');
    }

    public function statuses(){
        return $this->hasMany(PropStatus::class,'property_id');
    }

    public function sites(){
        return $this->hasMany(PropertySite::class,'property_id');
    }

    public function features(){
        return $this->hasOne(PropertyFeature::class,'property_id');
    }

    public function agents(){
        return $this->hasOne(PropertyAgent::class,'property_id');
    }
    public function showdays(){
        return $this->hasMany(PropertyShowday::class,'property_id');
    }

    public function images(){
        return $this->hasOne(PropertyImage::class,'property_id');
    }
    public function featured(){
        return $this->hasMany(FeaturedListing::class,'property_id');
    }

    public function featuredView(){
        return $this->hasMany(FeaturedView::class,'property_id');
    }

    public function priority(){
        return $this->hasMany(PriorityListing::class,'property_id');
    }

    public function attributes(){
        return $this->hasOne(PropertyAttribute::class,'property_id');
    }
}
