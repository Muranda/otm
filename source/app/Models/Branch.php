<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    use HasFactory;

    public function agency(){
        return $this->belongsTo(Agency::class,'agency_id');
    }

    public function suburb(){
        return $this->belongsTo(Suburb::class,'suburb_id');
    }

    public function carbonContacts(){
        return $this->hasMany(CarbonCopyContact::class,'agency_id');
    }
}
