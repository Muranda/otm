<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Addon extends Model
{
    use HasFactory;

    public function addOnSite(){
        return $this->hasMany(AddonSite::class,'addon_id');
    }
}
