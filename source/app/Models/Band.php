<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Band extends Model
{
    use HasFactory;

    public function packageBands(){
        return $this->hasMany(PackageBand::class,'band_id');
    }
}
