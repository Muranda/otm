<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PropertyAgent extends Model
{
    use HasFactory;
    public function property(){
        return $this->belongsTo(Property::class,'property_id');
    }

    public function user(){
        return $this->belongsTo(User::class,'agent_1');
    }

    public function agentDetail(){
        return $this->belongsTo(AgentDetail::class,'agent_1','user_id');
    }
}
