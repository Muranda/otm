<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BannerAdvert extends Model
{
    use HasFactory;

    public function addOnSite(){
        return $this->belongsTo(AddonSite::class,'addon_site_id');
    }
}
