<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BranchStat extends Model
{
    use HasFactory;
    protected $fillable = ['date','site_id','branch_id'];
}
