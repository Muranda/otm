<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Agency extends Model
{
    use HasFactory;

    public function agentDetail(){
        return $this->hasMany(AgentDetail::class,'agency_id');
    }

    public function settings(){
        return $this->hasOne(UserSetting::class,'agency_id');
    }

    public function branches(){
        return $this->hasMany(Branch::class,'agency_id');
    }

    public function bandSub(){
        return $this->hasOne(BandSub::class,'agency_id');
    }

    public function carbonContacts(){
        return $this->hasMany(CarbonCopyContact::class,'agency_id');
    }
}
