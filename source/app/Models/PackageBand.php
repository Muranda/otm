<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PackageBand extends Model
{
    use HasFactory;

    public function package(){
        return $this->belongsTo(Package::class,'package_id');
    }

    public function site(){
        return $this->belongsTo(Site::class,'site_id');
    }

    public function band(){
        return $this->belongsTo(Band::class,'band_id');
    }
}
