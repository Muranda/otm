<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Suburb extends Model
{
    use HasFactory;
    public function area(){
        return $this->belongsTo(Area::class,'area_id');
    }

    public function properties(){
        return $this->hasMany(Property::class,'suburb_id');
    }
}
