<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AgencyStat extends Model
{
    use HasFactory;
    protected $fillable = ['date','site_id','agency_id'];
}
