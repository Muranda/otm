<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PropertySearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'searchText' => 'required',
            'propertyType' => 'nullable',
            'minPrice' => 'nullable',
            'maxPrice' => 'nullable',
            'beds' => 'nullable',
            'baths' => 'nullable',
            'propertyArea' => 'nullable'
        ];
    }

    public function messages()
    {
        return [
            'searchText' => 'Please enter a location or reference code to search'
        ];
    }
}
