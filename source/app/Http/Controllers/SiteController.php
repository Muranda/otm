<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewsletterSubsRequest;
use App\Http\Requests\PropertySearchRequest;
use App\Jobs\LogPropertyStats;
use App\Jobs\SaveUserReport;
use App\Models\Addon;
use App\Models\Agency;
use App\Models\Area;
use App\Models\Band;
use App\Models\Blog;
use App\Models\Branch;
use App\Models\Feature;
use App\Models\FeaturedListing;
use App\Models\FeaturedView;
use App\Models\HomepageLogo;
use App\Models\Package;
use App\Models\Page;
use App\Models\Property;
use App\Models\PropertyEnquiry;
use App\Models\PropertyType;
use App\Models\Province;
use App\Models\Site;
use App\Models\Subscriptions;
use App\Models\Suburb;
use App\Models\Town;
use App\Models\User;
use App\Notifications\CarbonTextNotification;
use App\Notifications\EnquiryNotification;
use App\Notifications\FailedToSubscribeNotification;
use Cornford\Googlmapper\Exceptions\MapperSearchResultException;
use Cornford\Googlmapper\Facades\MapperFacade as Mapper;
use DrewM\MailChimp\MailChimp;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use PhpParser\Node\Stmt\Echo_;

class SiteController extends Controller
{
    private $main_meta,$sales_meta,$rentals_meta,$site;
    public function __construct()
    {
        $this->main_meta = 'Search for thousands of houses, stands, cottages and other property for sale and rent in Zimbabwe. Propertybook.co.zw is Zimbabwe\'s leading property sales and rentals portal with 100\'s of registered real estate agents. New houses for sale and rent in Harare, Zimbabwe uploaded daily.';
        $this->sales_meta = 'Search for houses for sale in Zimbabwe. Cottages, stands, apartments/flats for sale also available. Propertybook.co.zw is Zimbabwe\'s leading property sales and rentals portal with 100\'s of registered real estate agents. New houses for sale and rent in Harare, Zimbabwe uploaded daily.';
        $this->rentals_meta = 'Search for houses to rent in Zimbabwe. Created from the Harare rentals group, propertybook.co.zw is Zimbabwe\'s leading property rentals portal. Find your house for rent in Harare using our registered real estate agents.';
        $this->site = Cache::tags('sites')->rememberForever('site_'.siteID(),function (){
            return Site::where('id',siteID())->firstOrFail();
        });
    }

    /**
     * Display website homepage
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {

        $type = 'for Sale';
        $featured = $this->getFeaturedProperties('home');
        $agencies = Cache::tags('home_logos')->remember('homepage_logos_site_'.siteID(),3*60,function(){
            return HomepageLogo::with('agency')
                ->where(function ($query){
                    $query->where('start_date','<=',date('Y-m-d'));
                    $query->where('expiry_date','>=',date('Y-m-d'));
                })
                ->whereHas('agency',function ($query){
                    $query->where('active',1);
                })
                ->orderBy(DB::raw('RAND()'))
                ->get();
        });
        $salesProperties =  Property::whereHas('statuses',function ($query){
            $query->where('status_id','>',1);
            $query->where('status_id','!=',3);
            })
            ->whereHas('offer', function ($query){
                $query->where('label','for Sale');
            })
            ->take(2)
            ->get();

        $rentalsProperties =  Property::whereHas('statuses',function ($query){
            $query->where('status_id','>',1);
            $query->where('status_id','!=',3);
            })
            ->whereHas('offer', function ($query){
                $query->where('label','to Rent');
            })
            ->take(2)
            ->get();


        return view('index', [
            'type' => $type,
            'active' => 'home',
            'topSection' => $this->locationList('area', $type, 'top', 43),
            'col1' => $this->locationList('area', $type, 'column', 45),
            'col2' => $this->locationList('area', $type, 'column', 44),
            'col3' => $this->locationList('area', $type, 'column', 41),
            'col4' => $this->locationList('province', $type, 'column', 0),
            'pageTitle' => 'Find property for sale and rent in Zimbabwe',
            'hideSearch' => '',
            'featured' => $featured,
            'newProperties' => $salesProperties->merge($rentalsProperties),
            'agencies' => $agencies,
            'home' => true,
            'metaDescription' => $this->main_meta
        ]);
    }

    public function home()
    {
        $type = 'for Sale';
        $properties = Property::with([
            'offer:id,label,suffix',
            'suburb' => function ($query) {
                $query->select('id', 'area_id', 'slug', 'name');
                $query->with(['area' => function ($query) {
                    $query->select('id', 'town_id', 'slug', 'name');
                    $query->with(['town' => function ($query) {
                        $query->select('id', 'province_id', 'slug');
                        $query->with(['province' => function ($query) {
                            $query->select('id', 'slug');
                        }]);
                    }]);
                }]);
            },
            'type:id,type',
            'images:id,property_id,main_image',
            'features:id,property_id,bedrooms,bathrooms,boreholes,lounges',
        ])
            ->select('id', 'status_id', 'suburb_id', 'offer_id', 'type_id', 'title', 'zwl_price','usd_price', 'ref_code','label')
            ->where('status_id', '>', 1)
            ->whereHas('offer', function ($query) use ($type) {
                $query->select('id');
            })
            ->take(6)
            ->orderBy('created_at', 'desc')
            ->get();
        return view('index2', [
            'properties' => $properties,
            'type' => $type,
            'active' => 'home',
            'topSection' => $this->locationList('area', $type, 'top', 43),
            'col1' => $this->locationList('area', $type, 'column', 45),
            'col2' => $this->locationList('area', $type, 'column', 44),
            'col3' => $this->locationList('area', $type, 'column', 41),
            'col4' => $this->locationList('province', $type, 'column', 0),
            'magazine' => $this->homepageMag(),
            'articles' => $this->homepageArticles(),
            'pageTitle' => 'Find property for sale in Zimbabwe',
            'hideSearch' => ''
        ]);
    }

    /**
     * Display the rentals landing page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function rentals()
    {
        $type = 'to Rent';
        $properties = Property::with([
            'offer:id,label,suffix',
            'suburb' => function ($query) {
                $query->select('id', 'area_id', 'slug', 'name');
                $query->with(['area' => function ($query) {
                    $query->select('id', 'town_id', 'slug', 'name');
                    $query->with(['town' => function ($query) {
                        $query->select('id', 'province_id', 'slug');
                        $query->with(['province' => function ($query) {
                            $query->select('id', 'slug');
                        }]);
                    }]);
                }]);
            },
            'type:id,type',
            'images:id,property_id,main_image',
            'features:id,property_id,bedrooms,bathrooms,boreholes,lounges'
        ])
            ->select('id', 'status_id', 'suburb_id', 'offer_id', 'type_id', 'title', 'zwl_price','usd_price', 'ref_code')
            ->where('status_id', '>', 1)
            ->whereHas('offer', function ($query) use ($type) {
                $query->where('label', $type);
                $query->select('id');
            })
            ->take(6)
            ->orderBy('created_at', 'desc')
            ->get();
        return view('index', [
            'properties' => $properties,
            'type' => $type,
            'active' => Str::slug($type),
            'topSection' => $this->locationList('area', $type, 'top', 43),
            'col1' => $this->locationList('area', $type, 'column', 45),
            'col2' => $this->locationList('area', $type, 'column', 44),
            'col3' => $this->locationList('area', $type, 'column', 41),
            'col4' => $this->locationList('province', $type, 'column', 0),
            'magazine' => $this->homepageMag(),
            'articles' => $this->homepageArticles(),
            'pageTitle' => 'Find property to rent in Zimbabwe',
            'hideSearch' => '',
            'metaDescription' => $this->rentals_meta
        ]);
    }

    /**
     * Search for locations to suggest in search bar
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function propertyLocationSearch(Request $request)
    {
        $term = trim($request['q']);
        if (empty($term)) {
            return response()->json([]);
        }

        $results = [];

        $provinces = Province::select('id', 'name')
            ->where('name', 'LIKE', '%' . $term . '%')
            ->get();
        foreach ($provinces as $province) {
            $results[] = [
                'id' => 'Province_' . $province->id,
                'text' => '<span style="font-weight: 600">' . $province->name . '</span><span style="float:right;font-style: italic">Province</span>',
                'display' => $province->name
            ];
        }

        $cities = Town::with([
            'province:id,name'
        ])
            ->select('id', 'province_id', 'name')
            ->where('name', 'LIKE', '%' . $term . '%')
            ->get();
        foreach ($cities as $city) {
            $results[] = [
                'id' => 'Town_' . $city->id,
                'text' => '<span style="font-weight: 600">' . $city->name . ', ' . $city->province->name . '</span><span style="float:right;font-style: italic">City</span>',
                'display' => $city->name
            ];
        }

        $areas = Area::with([
            'town:id,name'
        ])
            ->select('id', 'town_id', 'name')
            ->where('name', 'LIKE', '%' . $term . '%')
            ->get();
        foreach ($areas as $area) {
            $results[] = [
                'id' => 'Area_' . $area->id,
                'text' => '<span style="font-weight: 600">' . $area->name . ', ' . $area->town->name . '</span><span style="float:right;font-style: italic">Area</span>',
                'display' => $area->name
            ];
        }

        $suburbs = Suburb::with([
            'area:id,name'
        ])
            ->select('id', 'area_id', 'name')
            ->where('name', 'LIKE', '%' . $term . '%')
            ->get();
        foreach ($suburbs as $suburb) {
            $results[] = [
                'id' => 'Suburb_' . $suburb->id,
                'text' => '<span style="font-weight: 600">' . $suburb->name . ', ' . $suburb->area->name . '</span><span style="float:right;font-style: italic">Suburb</span>',
                'display' => $suburb->name
            ];
        }

        return response()->json($results);
    }

    /**
     * Comment here
     */
    public function listings(Request $request, $filter)
    {
        if ($filter == 'for-sale') {
            $type = 'for Sale';
        } elseif ($filter == 'to-rent') {
            $type = 'to Rent';
        } else {
            abort(404, 'Not Found');
            $type = '';
        }


        $queries = $request->query();
        if (isset($queries['sortBy'])) {
            if ($queries['sortBy'] == 'most-recent') {
                $col = 'created_at';
                $dir = 'desc';
            } elseif ($queries['sortBy'] == 'price-high') {
                if(Session::get('selected_currency') == 'usd'){
                    $col = 'usd_price';
                }
                else{
                    $col = 'zwl_price';
                }
                $dir = 'desc';
            } elseif ($queries['sortBy'] == 'price-low') {
                if(Session::get('selected_currency') == 'usd'){
                    $col = 'usd_price';
                }
                else{
                    $col = 'zwl_price';
                }
                $dir = 'asc';
            } else {
                $col = 'created_at';
                $dir = 'desc';
            }
            $sortBy = 'sortBy=' . $queries['sortBy'];
        } else {
            $col = 'created_at';
            $dir = 'desc';
            $sortBy = 'sortBy=default';
        }

        if (isset($queries['page']) && $queries['page'] > 1) {
            $skip = ($queries['page'] - 1) * 3;
        } else {
            $skip = 0;
        }

        $featured = $this->getFeaturedProperties($type);


            $properties = Property::with([
            'offer:id,label,suffix',
            'suburb' => function ($query) {
                $query->select('id', 'name', 'slug', 'area_id');
                $query->with([
                    'area' => function ($query) {
                        $query->select('id', 'name', 'slug', 'town_id');
                        $query->with([
                            'town' => function ($query) {
                                $query->select('id', 'name', 'slug', 'province_id');
                                $query->with([
                                    'province' => function ($query) {
                                        $query->select('id', 'slug');
                                    }
                                ]);
                            }
                        ]);
                    }
                ]);
            },
            'type:id,type',
            'agents' => function ($query) {
                $query->select('id', 'property_id', 'agent_1');
                $query->with([
                    'agentDetail' => function ($query) {
                        $query->select('user_id', 'agency_id');
                        $query->with([
                            'agency' => function ($query) {
                                $query->select('id','name','slug','logo','hex_code','text_color','overlay');
                                $query->with([
                                    'settings' => function ($query) {
                                        $query->select('agency_id', 'custom_rate', 'rate');
                                    }
                                ]);
                            }
                        ]);
                    }
                ]);
            },
            'features:property_id,bedrooms,lounges,bathrooms,boreholes',
            'images:property_id,main_image',
            'priority'
        ])
            ->select('id', 'suburb_id', 'type_id', 'offer_id', 'ref_code', 'description', 'created_at', 'usd_price','zwl_price')
            ->whereHas('offer', function ($query) use ($type) {
                $query->select('id');
                $query->where('label', $type);
            })
            ->whereHas('sites',function ($query){
                $query->where('site_id',siteID());
            })
            ->whereHas('statuses',function ($query){
                $query->where('status_id','>',1);
            })
            ->where('active',1)
            ->orderBy($col, $dir)
            ->paginate(15);

        if (preg_match('(default|most-recent)', $sortBy) === 1) {
            $sort_props = $properties->sortByDesc(function ($item) {
                return $item->priority . '#' . $item->created_at;
            });

            $properties = new LengthAwarePaginator($sort_props, $properties->total(), $properties->perPage());
            $properties->setPath(URL::current());
        }

        if ($properties->currentPage() > $properties->lastPage()) {
            abort(404, 'Not Found');
        }

        $sidebarTypes = PropertyType::withCount([
            'properties' => function ($query) use ($filter) {
                $query->whereHas('offer', function ($query) use ($filter) {
                    $query->select('id');
                    if ($filter != '') {
                        $query->where('label', $filter);
                    }
                });
                $query->whereHas('statuses',function ($query){
                    $query->where('status_id','>',1);
                });
            }
        ])
            ->orderBy('properties_count', 'desc')
            ->get();


        $searchQuery = [];


        if ($request->ajax()) {
            $view = view('listing', [
                'pageTitle' => 'Property ' . strtolower($type) . ' in Zimbabwe',
                'properties' => $properties,
                'sortValue' => $sortBy,

            ])
                ->render();
            return [
                'html' => $view,
                'lower_bound' => ($properties->count() > 0 ? ($properties->currentPage() - 1) * $properties->perPage() + 1 : 0),
                'upper_bound' => ($properties->count() < $properties->perPage() ? $properties->total() : ($properties->currentPage() - 1) * $properties->perPage() + $properties->perPage())
            ];
        } else {
            return view('listing', [
                'properties' => $properties,
                'featured' => $featured,
                'active' => $filter,
                'type' => $type,
                'area' => 'Zimbabwe',
                'sidebarTypes' => $sidebarTypes,
                'topSection' => $this->locationList('area', $type, 'top', 43),
                'col1' => $this->locationList('area', $type, 'column', 45),
                'col2' => $this->locationList('area', $type, 'column', 44),
                'col3' => $this->locationList('area', $type, 'column', 41),
                'col4' => $this->locationList('province', $type, 'column', 0),
                'searchQuery' => $searchQuery,
                'sortValue' => $sortBy,
                'pageTitle' => 'Property ' . strtolower($type) . ' in Zimbabwe',
                'metaDescription' => ($type == 'for Sale' ? 'Otm.co.zw has 100’s of properties available for rent in Zimbabwe. List your property for rent on Zimbabwe’s largest free property portal  ' : 'With 1000’s of properties for sale in Zimbabwe, Otm.co.zw is Zimbabwe’s largest free property portal where you can list your property for sale')
            ]);
        }
    }


    /**
     * Catch requests to the old URL and redirect to new one
     * @param Request $request
     * @param $filter
     * @return array|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Throwable
     */
    public function allProperties(Request $request, $filter)
    {
        if($filter == ('for-sale' || 'to-rent')){
            return redirect()->route('allListings',['filter' => $filter]);
        }
        else{
            abort(404, 'Not Found');
        }
    }

    /**
     * Return list of locations to display in the locations list on property listing pages
     * List is based on locations with the most properties
     * @param $type
     * @param $filter
     * @param $section
     * @param $id
     * @return string
     */
    public function locationList($type, $filter, $section, $id)
    {
        if ($type == 'area') {
            if ($section == 'top') {
                $suburbs = Cache::tags(['locations'])->remember('top-suburbs-'.$section.'-'.$id.'_cloud',60*6,function () use ($id,$filter){
                    return Suburb::with([
                        'area' => function ($query) {
                            $query->select('id', 'name', 'slug', 'town_id');
                            $query->with([
                                'town' => function ($query) {
                                    $query->select('id', 'slug', 'province_id');
                                    $query->with([
                                        'province' => function ($query) {
                                            $query->select('id', 'slug');
                                        }
                                    ]);
                                }
                            ]);
                        }
                    ])
                        ->select('id', 'name', 'slug', 'area_id')
                        ->where('area_id', $id)
                        ->withCount([
                            'properties' => function ($query) use ($filter) {
                                $query->whereHas('sites',function ($query){
                                    $query->where('site_id',siteID());
                                });
                                $query->whereHas('offer', function ($query) use ($filter) {
                                    $query->select('id');
                                    if ($filter != '') {
                                        $query->where('label', $filter);
                                    }
                                });
                                $query->whereHas('statuses',function ($query){
                                    $query->where('status_id','>',1);
                                });
                            }
                        ])
                        ->orderBy('properties_count', 'desc')
                        ->take(12)
                        ->get();
                });

                $sub = $suburbs->first();
                $render = '<h6 class="heading"><a href="' . route('filterProperties', ['filter' => Str::slug($filter), 'province' => $sub->area->town->province->slug, 'town' => $sub->area->town->slug, 'area' => $sub->area->slug]) . '">' . $sub->area->name . '</a> </h6>
                        <div class="row">';
                foreach ($suburbs as $place) {
                    if ($place->slug == $place->area->slug) {
                        $link = route('filterProperties', ['filter' => Str::slug($filter), 'province' => $sub->area->town->province->slug, 'town' => $sub->area->town->slug, 'area' => $sub->area->slug]);
                    } else {
                        $link = route('filterProperties', ['filter' => Str::slug($filter), 'province' => $sub->area->town->province->slug, 'town' => $sub->area->town->slug, 'area' => $sub->area->slug, 'suburb' => $place->suburb]);
                    }
                    $render .= '<div class="col-lg-2 col-md-3 col-sm-4 col-6">
                                <p>
                                <a href="' . $link . '">' . $place->name . '</a>
                                </p>
                                </div>';
                }
                $render .= '</div>';
                return $render;
            }
            elseif ($section == 'column') {
                $suburbs = Cache::remember('top-suburbs-'.$section.'-'.$id.'_cloud',60*6,function() use ($filter,$id){
                    return Suburb::with([
                        'area' => function ($query) {
                            $query->select('id', 'name', 'slug', 'town_id');
                            $query->with([
                                'town' => function ($query) {
                                    $query->select('id', 'slug', 'province_id');
                                    $query->with([
                                        'province' => function ($query) {
                                            $query->select('id', 'slug');
                                        }
                                    ]);
                                }
                            ]);
                        }
                    ])
                        ->select('id', 'name', 'slug', 'area_id')
                        ->where('area_id', $id)
                        ->withCount([
                            'properties' => function ($query) use ($filter) {
                                $query->whereHas('sites',function ($query){
                                    $query->where('site_id',siteID());
                                });
                                $query->whereHas('offer', function ($query) use ($filter) {
                                    $query->select('id');
                                    if ($filter != '') {
                                        $query->where('label', $filter);
                                    }
                                });
                                $query->whereHas('statuses',function ($query){
                                    $query->where('status_id','>',1);
                                });
                            }
                        ])
                        ->orderBy('properties_count', 'desc')
                        ->take(4)
                        ->get();
                });

                $sub = $suburbs->first();
                $render = '<h6 class="heading"><a href="' . route('filterProperties', ['filter' => Str::slug($filter), 'province' => $sub->area->town->province->slug, 'town' => $sub->area->town->slug, 'area' => $sub->area->slug]) . '">' . $sub->area->name . '</a> </h6>';

                foreach ($suburbs as $place) {
                    if ($place->slug == $place->area->slug) {
                        $link = route('filterProperties', ['filter' => Str::slug($filter), 'province' => $sub->area->town->province->slug, 'town' => $sub->area->town->slug, 'area' => $sub->area->slug]);
                    } else {
                        $link = route('filterProperties', ['filter' => Str::slug($filter), 'province' => $sub->area->town->province->slug, 'town' => $sub->area->town->slug, 'area' => $sub->area->slug, 'suburb' => $place->slug]);
                    }
                    $render .= '<p>
                                <a href="' . $link . '">' . $place->name . '</a>
                            </p>';
                }
                return $render;
            }
        }
        elseif ($type == 'province') {
            $provinces = Cache::remember('top-provinces'.'_cloud',60*6,function() use ($filter,$id){
                return Province::withCount([
                    'properties' => function ($query) use ($filter) {
                        $query->whereHas('sites',function ($query){
                            $query->where('site_id',siteID());
                        });
                        $query->whereHas('offer', function ($query) use ($filter) {
                            $query->select('id');
                            if ($filter != '') {
                                $query->where('label', $filter);
                            }
                        });
                        $query->whereHas('statuses',function ($query){
                            $query->where('status_id','>',1);
                        });
                    }
                ])
                    ->where('id', '!=', 1)
                    ->orderBy('properties_count', 'desc')
                    ->take(3)
                    ->get();
            });

            $render = '';
            foreach ($provinces as $province) {
                $suburbs = Cache::remember('top-province-suburbs-'.$id.'_cloud',60*6,function() use ($filter,$id,$province){
                    return Suburb::with([
                        'area' => function ($query) {
                            $query->select('id', 'slug', 'town_id');
                            $query->with([
                                'town' => function ($query) {
                                    $query->select('id', 'slug', 'province_id');
                                    $query->with([
                                        'province' => function ($query) {
                                            $query->select('id', 'slug');
                                        }
                                    ]);
                                }
                            ]);
                        }
                    ])
                        ->select('id', 'name', 'slug', 'area_id')
                        ->whereHas('area.town', function ($query) use ($province) {
                            $query->select('id');
                            $query->where('province_id', $province->id);
                        })
                        ->withCount([
                            'properties' => function ($query) use ($filter) {
                                $query->whereHas('sites',function ($query){
                                    $query->where('site_id',siteID());
                                });
                                $query->whereHas('offer', function ($query) use ($filter) {
                                    $query->select('id');
                                    if ($filter != '') {
                                        $query->where('label', $filter);
                                    }
                                });
                                $query->whereHas('statuses',function ($query){
                                    $query->where('status_id','>',1);
                                });
                            }
                        ])
                        ->orderBy('properties_count', 'desc')
                        ->take(4)
                        ->get();
                });

                $view = '<div class="col-lg-2 col-md-3 col-sm-4 col-6">
                        <h6 class="heading"><a href="' . route('filterProperties', ['filter' => Str::slug($filter), 'province' => $province->slug]) . '">' . $province->name . '</a> </h6>
                        ';
                foreach ($suburbs as $suburb) {
                    if ($suburb->slug == $suburb->area->slug) {
                        $link = route('filterProperties', ['filter' => Str::slug($filter), 'province' => $suburb->area->town->province->slug, 'town' => $suburb->area->town->slug, 'area' => $suburb->area->slug]);
                    } else {
                        $link = route('filterProperties', ['filter' => Str::slug($filter), 'province' => $suburb->area->town->province->slug, 'town' => $suburb->area->town->slug, 'area' => $suburb->area->slug, 'suburb' => $suburb->slug]);
                    }
                    $view .= '<p>
                                <a href="' . $link . '">' . $suburb->name . '</a>
                            </p>';
                }
                $view .= '</div>';
                $render .= $view;
            }
            return $render;
        }
    }

    /**
     * Return blog articles to be displayed on the landing pages
     * @return mixed
     */
    public function homepageArticles()
    {
        return Cache::tags('blog')->remember('homepage_blog_'.siteID(),24*60,function (){
            return Blog::select('id', 'slug', 'image', 'title', 'content')
                ->where('site_id',siteID())
                ->where('published', 1)
                ->orderBy('created_at', 'desc')
                ->take(3)
                ->get();
        });
    }

    /**
     * Subscribe to Mailchimp newsletter
     * @param NewsletterSubsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function newsletterSubscription(Request $request)
    {
        if($request['email'] != ''){
            $this->validate($request,[
                'email' => 'email|unique:subscriptions'
            ],[
                'email.email' => 'The email address should be a valid email address',
                'email.unique' => 'This email address already subscribed'
            ]);
        }
        if($request['phone_number']){
            $this->validate($request,[
                'phone_number' => 'numeric',
            ],[
                'phone_number.numeric' => 'Phone should be numeric',

            ]);

        }

        $new_subscriber  = new Subscriptions();
        $new_subscriber->phone_number = $request['phone_number'];
        $new_subscriber->email = $request['email'];
        $new_subscriber->save();

        return response()->json('subscribed',200);
    }

    /**
     * Return results for an agency search
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function propertyAgencySearch(Request $request)
    {
        $term = trim($request['q']);
        if (empty($term)) {
            return response()->json([]);
        }

        $results = [];

        $agencies = Agency::where('name', 'LIKE', '%' . $term . '%')
            ->select('id', 'name')
            ->where('active', 1)
            ->get();
        foreach ($agencies as $agency) {
            $results[] = [
                'id' => $agency->id,
                'text' => '<span style="font-weight: 600">' . $agency->name . '</span>',
            ];
        }
        return response()->json($results);
    }

    /**
     * Build parameters to be used in returning search results
     * @param PropertySearchRequest $request
     * @param $filter
     * @return \Illuminate\Http\RedirectResponse
     */
    public function findProperties(PropertySearchRequest $request, $filter)
    {
        $data = $request->validated();
        //Build Search Parameters
        $parameters = '';
        if($request['propertyType'][0] == null){

        }else{
            if (!empty($request['propertyType'])) {

                $i = 0;
                $last = count($request['propertyType']) - 1;

                $propertyType = 'type=';

                foreach ($request['propertyType'] as $item => $value) {
                    if ($i != $last) {
                        $type = $value . '-';
                    } else {
                        $type = $value;
                    }
                    $propertyType .= $type;
                    $i++;
                }
                $parameters .= $propertyType;
            }
        }

        if (!empty($data['minPrice'])) {
            $parameters .= ($parameters == '' ? '' : '&') . 'minPrice=' . $data['minPrice'];
        }
        if (!empty($data['maxPrice']) && $data['maxPrice'] != 0) {
            $parameters .= ($parameters == '' ? '' : '&') . 'maxPrice=' . $data['maxPrice'];
        }
        if (!empty($data['beds']) && $data['beds'] != 0) {
            $parameters .= ($parameters == '' ? '' : '&') . 'beds=' . $data['beds'];
        }
        if (!empty($data['baths']) && $data['baths'] != 0) {
            $parameters .= ($parameters == '' ? '' : '&') . 'baths=' . $data['baths'];
        }
        if (!empty($data['propertyArea'])) {
            $parameters .= ($parameters == '' ? '' : '&') . 'propertyArea=' . $data['propertyArea'];
        }

        if (count($data['searchText']) == 1) {
            $location = explode('_', $data['searchText'][0]);
            if (count($location) > 1) {
                if ($location[0] == 'Province') {
                    $province = Province::where('id', $location[1])
                        ->select('slug')
                        ->first();
                    return redirect()->route('filterProperties', ['filter' => $filter, 'province' => $province->slug . ($parameters != '' ? '?' . $parameters : '')]);
                } elseif ($location[0] == 'Town') {
                    $town = Town::with([
                        'province:id,slug'
                    ])
                        ->select('id', 'slug', 'province_id')
                        ->where('id', $location[1])
                        ->first();
                    return redirect()->route('filterProperties', ['filter' => $filter, 'province' => $town->province->slug, 'town' => $town->slug . ($parameters != '' ? '?' . $parameters : '')]);
                } elseif ($location[0] == 'Area') {
                    $area = Area::with([
                        'town' => function ($query) {
                            $query->select('id', 'slug', 'province_id');
                            $query->with([
                                'province' => function ($query) {
                                    $query->select('id', 'slug');
                                }
                            ]);
                        }
                    ])
                        ->select('id', 'slug', 'town_id')
                        ->where('id', $location[1])
                        ->first();
                    return redirect()->route('filterProperties', ['filter' => $filter, 'province' => $area->town->province->slug, 'town' => $area->town->slug, 'area' => $area->slug . ($parameters != '' ? '?' . $parameters : '')]);
                } elseif ($location[0] == 'Suburb') {
                    $suburb = Suburb::with([
                        'area' => function ($query) {
                            $query->select('id', 'slug', 'town_id');
                            $query->with([
                                'town' => function ($query) {
                                    $query->select('id', 'slug', 'province_id');
                                    $query->with([
                                        'province' => function ($query) {
                                            $query->select('id', 'slug');
                                        }
                                    ]);
                                }
                            ]);
                        }
                    ])
                        ->select('id', 'slug', 'area_id')
                        ->where('id', $location[1])
                        ->first();
                    return redirect()->route('filterProperties', ['filter' => $filter, 'province' => $suburb->area->town->province->slug, 'town' => $suburb->area->town->slug, 'area' => $suburb->area->slug, 'suburb' => $suburb->slug . ($parameters != '' ? '?' . $parameters : '')]);
                }
            }
        }
        else{
            $query = $request['searchText'];
            $provinces = [];
            $towns = [];
            $areas = [];
            $suburbs = [];
            $refCodes = [];
            if(!empty($query) && count($query) > 0){
                foreach ($query as $item){
                    $location = explode('_',$item);
                    if(count($location) > 1){
                        if($location[0] == 'Province'){
                            $provinces[] = $location[1];
                        }
                        elseif($location[0] == 'Town'){
                            $towns[] = $location[1];
                        }
                        elseif($location[0] == 'Area'){
                            $areas[] = $location[1];
                        }
                        elseif($location[0] == 'Suburb'){
                            $suburbs[] = $location[1];
                        }
                    }
                    else{
                        $refCodes[] = $location;
                    }
                }
            }
            else{
                $provinces[] = 'zimbabwe';
            }

            $search = [
                'provinces' => $provinces,
                'towns' => $towns,
                'areas' => $areas,
                'suburbs' => $suburbs,
                //'refCodes' => $refCodes
            ];
            $search = http_build_query($search);


            return redirect()->route('advancedSearch',['filter' => $filter,'search' => $search.'?'.$parameters]);
        }
    }

    /**
     * Apply parameters to search and return results.
     * Works when only single search location has been submitted
     * @param Request $request
     * @param $filter
     * @param $province
     * @param null $town
     * @param null $area
     * @param null $suburb
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     * @throws \Throwable
     */
    public function filterProperties(Request $request, $filter, $province, $town = null, $area = null, $suburb = null)
    {

        if ($filter == 'for-sale') {
            $type = 'for Sale';
        } elseif ($filter == 'to-rent') {
            $type = 'to Rent';
        }
        else{
            abort(404);
        }

        $queries = $request->query();

        $pTypes = [];
        if (isset($queries['type'])) {
            $cTypes = explode('-', $queries['type']);
            $showTypes = '';
            $i = 1;
            foreach ($cTypes as $cType) {
                $fType = PropertyType::where('id', $cType)
                    ->select('id', 'plural')
                    ->first();
                if (!empty($fType)) {
                    $pTypes[] = $cType;
                    if ((count($cTypes) - $i) == 1) {
                        $showTypes .= $fType->plural . ' and ';
                    } elseif ($i < count($cTypes)) {
                        $showTypes .= $fType->plural . ', ';
                    } else {
                        $showTypes .= $fType->plural . ' ';
                    }
                }
                $i++;
            }
        } else {
            $cTypes = PropertyType::select('id')->get();
            foreach ($cTypes as $cType) {
                $pTypes[] = $cType->id;
            }
            $showTypes = 'Property ';
        }


        if (isset($queries['sortBy'])) {
            if ($queries['sortBy'] == 'most-recent') {
                $col = 'created_at';
                $dir = 'desc';
            } elseif ($queries['sortBy'] == 'price-high') {
                if(Session::get('selected_currency') == 'usd'){
                    $col = 'usd_price';
                }
                else{
                    $col = 'zwl_price';
                }
                $dir = 'desc';
            } elseif ($queries['sortBy'] == 'price-low') {
                if(Session::get('selected_currency') == 'usd'){
                    $col = 'usd_price';
                }
                else{
                    $col = 'zwl_price';
                }
                $dir = 'asc';
            } else {
                $col = 'created_at';
                $dir = 'desc';
            }
            $sortValue = 'sortBy=' . $queries['sortBy'];
        } else {
            $col = 'created_at';
            $dir = 'desc';
            $sortValue = 'sortBy=default';
        }

        if ($suburb) {
            $location = Suburb::select('id', 'name', 'slug', 'area_id')->where('slug', $suburb)->firstOrFail();
            $featured = $this->filteredFeaturedListings($type,'suburb',$suburb,$queries,$pTypes);
            $properties = $this->filteredProperties($type,'suburb',$suburb,$queries,$pTypes,$col,$dir,$request);
            $searchQuery = [
                'suburbs' => [
                    [
                        'id' => 'Suburb_' . $location->id,
                        'display' => $location->name
                    ]
                ],
                'parameters' => $queries
            ];
        }
        elseif ($area) {
            $location = Area::select('id', 'town_id', 'name', 'slug')->where('slug', $area)->firstOrFail();

            $featured = $this->filteredFeaturedListings($type,'area',$area,$queries,$pTypes);

            $properties = $this->filteredProperties($type,'area',$area,$queries,$pTypes,$col,$dir,$request);
            $searchQuery = [
                'areas' => [
                    [
                        'id' => 'Area_' . $location->id,
                        'display' => $location->name
                    ]
                ],
                'parameters' => $queries
            ];
        }
        elseif ($town) {
            $location = Town::select('id', 'province_id', 'name', 'slug')->where('slug', $town)->firstOrFail();

            $featured = $this->filteredFeaturedListings($type,'town',$town,$queries,$pTypes);

            $properties = $this->filteredProperties($type,'town',$town,$queries,$pTypes,$col,$dir,$request);
            $searchQuery = [
                'areas' => [
                    [
                        'id' => 'Town_' . $location->id,
                        'display' => $location->name
                    ]
                ],
                'parameters' => $queries
            ];
        }
        elseif ($province) {
            if($province != 'zimbabwe'){
                $location = Province::select('id', 'name', 'slug')->where('slug', $province)->firstOrFail();
            }
            else{
                $scope = 'global';
                $location = 'Zimbabwe';
            }


            $featured = $this->filteredFeaturedListings($type,'province',$province,$queries,$pTypes);

            $properties = $this->filteredProperties($type,'province',$province,$queries,$pTypes,$col,$dir,$request);
            if($province != 'zimbabwe'){
                $searchQuery = [
                    'areas' => [
                        [
                            'id' => 'Province_' . $location->id,
                            'display' => $location->name
                        ]
                    ],
                    'parameters' => $queries
                ];
            }
            else{
                $searchQuery = [
                    'parameters' => $queries
                ];
            }
        }

        if (preg_match('(default|most-recent)', $sortValue) === 1) {
            $sort_props = $properties->sortByDesc(function ($item) {
                return $item->priority . '#' . $item->created_at;
            });
            $properties = new LengthAwarePaginator($sort_props, $properties->total(), $properties->perPage());
            $properties->setPath(URL::current());
        }
        if ($properties->currentPage() > $properties->lastPage()) {
            abort(404, 'Not Found');
        }

        if ($request->ajax()) {
            return view('listing',[
                    'pageTitle' => $showTypes . strtolower($type) . ' in ' . (isset($scope) ? $location : $location->name),
                    'properties' => $properties,
                    'sortValue' => $sortValue,
                    'metaDescription' => 'Find  your ' . $showTypes . $type . ' in ' . (isset($scope) ? $location : $location->name),
                    'active' => $filter,
                    'pageName' => 'listing'
            ]);

        }
        else {
            $sidebarTypes = PropertyType::withCount([
                'properties' => function ($query) use ($type, $province, $town, $area, $suburb) {
                    $query->whereHas('offer', function ($query) use ($type) {
                        $query->select('id');
                        $query->where('label', $type);
                    });
                    $query->whereHas('suburb', function ($query) use ($province, $town, $area, $suburb) {
                        $query->select('id');
                        if ($suburb) {
                            $query->where('slug', $suburb);
                        } else {
                            $query->whereHas('area', function ($query) use ($province, $town, $area) {
                                $query->select('id');
                                if ($area) {
                                    $query->where('slug', $area);
                                } else {
                                    $query->whereHas('town', function ($query) use ($province, $town) {
                                        $query->select('id');
                                        if ($town) {
                                            $query->where('slug', $town);
                                        } else {
                                            $query->whereHas('province', function ($query) use ($province) {
                                                $query->select('id');
                                                if ($province) {
                                                    $query->where('slug', $province);
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                    $query->whereHas('statuses',function($query){
                        $query->where('status_id','>',1);
                    });
                }
            ])
                ->whereHas('properties',function($query){
                    $query->whereHas('sites',function ($query){
                        $query->where('site_id',siteID());
                    });
                })
                ->orderBy('properties_count', 'desc')
                ->get();

            return view('listing', [
                'properties' => $properties,
                'active' => $filter,
                'type' => $type,
                'area' => (isset($scope) ? $location : $location->name),
                'sidebarTypes' => $sidebarTypes,
                'breadcrumb' => $this->breadcrumb($filter, $province, $town, $area, $suburb),
                'typeLink' => $this->typeLink($filter, $province, $town, $area, $suburb),
                'searchQuery' => $searchQuery,
                'sortValue' => $sortValue,
                'featured' => $featured,
                'pageTitle' => ($showTypes == '' ? 'Property ' : $showTypes) . strtolower($type) . ' in ' . (isset($scope) ? $location : $location->name),
                'metaDescription' => 'Search for '. $showTypes .' ' .$type. ' in '.(isset($scope) ? $location : $location->name) .'. Otm.co.zw is Zimbabwe’s largest free property sales portal'
            ]);
        }

    }

    /**
     * Apply search parameters and return results
     * Works when multiple locations have been submitted
     * @param Request $request
     * @param $filter
     * @param $search
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     * @throws \Throwable
     */
    public function advancedSearch(Request $request,$filter,$search){
        if($filter == 'for-sale'){
            $type = 'for Sale';
        }
        elseif($filter == 'to-rent'){
            $type = 'to Rent';
        }

        $searchItems = explode('&',$search);
        $provinces = [];
        $provs = [];
        $towns = [];
        $town = [];
        $areas = [];
        $area = [];
        $suburbs = [];
        $suburb = [];
        $refCodes = [];
        $refCode = [];
        $propLocations = '';
        $i = 1;
        foreach ($searchItems as $item){
            $loc = explode('=',$item);
            if(substr($loc[0],0,9) == 'provinces'){
                if(isset($loc[1])){
                    $provinces[] = $loc[1];
                    $place = Province::select('id','name')->where('id',$loc[1])->first();
                    if(!empty($place)){
                        $provs[] = [
                            'id' => 'Province_'.$place->id,
                            'display' => $place->name
                        ];
                        if((count($searchItems) - $i) == 1){
                            $propLocations .= $place->name.' and ';
                        }
                        elseif($i < count($searchItems)){
                            $propLocations .= $place->name.', ';
                        }
                        else{
                            $propLocations .= $place->name.' ';
                        }
                    }
                }
                else{
                    return redirect()->route('allListings',['filter' => $filter]);
                }
            }
            elseif(substr($loc[0],0,5) == 'towns'){
                if(isset($loc[1])){
                    $towns[] = $loc[1];
                    $place = Town::select('id','name')->where('id',$loc[1])->first();
                    if(!empty($place)){
                        $town[] = [
                            'id' => 'Town_'.$place->id,
                            'display' => $place->name
                        ];
                        if((count($searchItems) - $i) == 1){
                            $propLocations .= $place->name.' and ';
                        }
                        elseif($i < count($searchItems)){
                            $propLocations .= $place->name.', ';
                        }
                        else{
                            $propLocations .= $place->name.' ';
                        }
                    }
                }
                else{
                    return redirect()->route('allListings',['filter' => $filter]);
                }
            }
            elseif(substr($loc[0],0,5) == 'areas'){
                if(isset($loc[1])){
                    $areas[] = $loc[1];
                    $place = Area::select('id','name')->where('id',$loc[1])->first();
                    if(!empty($place)){
                        $area[] = [
                            'id' => 'Area_'.$place->id,
                            'display' => $place->name
                        ];
                        if((count($searchItems) - $i) == 1){
                            $propLocations .= $place->name.' and ';
                        }
                        elseif($i < count($searchItems)){
                            $propLocations .= $place->name.', ';
                        }
                        else{
                            $propLocations .= $place->name.' ';
                        }
                    }
                }
                else{
                    return redirect()->route('allListings',['filter' => $filter]);
                }
            }
            elseif(substr($loc[0],0,7) == 'suburbs'){
                if(isset($loc[1])){
                    $suburbs[] = $loc[1];
                    $place = Suburb::select('id','name')->where('id',$loc[1])->first();
                    if(!empty($place)){
                        $suburb[] = [
                            'id' => 'Suburb_'.$place->id,
                            'display' => $place->name
                        ];
                        if((count($searchItems) - $i) == 1){
                            $propLocations .= $place->name.' and ';
                        }
                        elseif($i < count($searchItems)){
                            $propLocations .= $place->name.', ';
                        }
                        else{
                            $propLocations .= $place->name.' ';
                        }
                    }
                }
                else{
                    return redirect()->route('allListings',['filter' => $filter]);
                }
            }
            elseif(substr($loc[0],0,8) == 'refCodes'){
                return redirect()->route('allListings',['filter' => $filter]);
            }
            $i++;
        }

        $queries = $request->query();
        $pTypes = [];
        if(isset($queries['type'])){
            $cTypes = explode('-',$queries['type']);
            $showTypes = '';
            $i = 1;
            foreach ($cTypes as $cType){
                $pTypes[] = $cType;
                $fType = PropertyType::select('plural')->where('id',$cType)->first();
                if(!empty($fType)){
                    if((count($cTypes) - $i) == 1){
                        $showTypes .= $fType->plural.' and ';
                    }
                    elseif($i < count($cTypes)){
                        $showTypes .= $fType->plural.', ';
                    }
                    else{
                        $showTypes .= $fType->plural.' ';
                    }
                }
                $i++;
            }
        }
        else{
            $cTypes = PropertyType::select('id')->get();
            foreach ($cTypes as $cType){
                $pTypes[] = $cType->id;
            }
            $showTypes = 'Property ';
        }

        if(isset($queries['sortBy'])){
            if($queries['sortBy'] == 'most-recent'){
                $col = 'created_at';
                $dir = 'desc';
            }
            elseif($queries['sortBy'] == 'price-high'){
                if(Session::get('selected_currency') == 'usd'){
                    $col = 'usd_price';
                }
                else{
                    $col = 'zwl_price';
                }
                $dir = 'desc';
            }
            elseif($queries['sortBy'] == 'price-low'){
                if(Session::get('selected_currency') == 'usd'){
                    $col = 'usd_price';
                }
                else{
                    $col = 'zwl_price';
                }
                $dir = 'asc';
            }
            else{
                $col = 'created_at';
                $dir = 'desc';
            }
            $sortBy = 'sortBy='.$queries['sortBy'];
        }
        else{
            $col = 'created_at';
            $dir = 'desc';
            $sortBy = 'sortBy=default';
        }

        if(isset($queries['page']) && $queries['page'] > 1){
            $skip = ($queries['page'] - 1) * 3;
        }
        else{
            $skip = 0;
        }

        if(count($refCodes) > 0){
            $featured = Property::with([
                'offer:id,label,suffix',
                'suburb' => function ($query) {
                    $query->select('id', 'name', 'slug', 'area_id');
                    $query->with([
                        'area' => function ($query) {
                            $query->select('id', 'name', 'slug', 'town_id');
                            $query->with([
                                'town' => function ($query) {
                                    $query->select('id', 'name', 'slug', 'province_id');
                                    $query->with([
                                        'province' => function ($query) {
                                            $query->select('id', 'slug');
                                        }
                                    ]);
                                }
                            ]);
                        }
                    ]);
                },
                'type:id,type',
                'agents' => function ($query) {
                    $query->select('id', 'property_id', 'agent_1');
                    $query->with([
                        'agentDetail' => function ($query) {
                            $query->select('id', 'user_id', 'agency_id');
                            $query->with([
                                'agency' => function ($query) {
                                    $query->select('id', 'slug', 'name', 'hex_code', 'overlay','text_color', 'logo');
                                    $query->with([
                                        'settings' => function ($query) {
                                            $query->select('id', 'agency_id', 'custom_rate', 'rate');
                                        }
                                    ]);
                                }
                            ]);
                        }
                    ]);
                },
                'features:property_id,bedrooms,lounges,bathrooms,boreholes',
                'images:property_id,main_image,image_1,image_2,image_3,image_4'
            ])
                ->select('id', 'suburb_id', 'type_id', 'offer_id', 'ref_code', 'description', 'created_at', 'zwl_price','usd_price')
                ->where('featured',1)
                ->whereIn('type_id',$pTypes)
                ->whereHas('offer',function ($query) use ($type){
                    $query->select('id');
                    $query->where('label',$type);
                })
                ->where('status_id','>',1)
                ->where(function ($query) use ($refCodes){
                    foreach ($refCodes as $refCode){
                        $query->orWhere('ref_code','LIKE','%'.$refCode.'%');
                    }
                })
                ->orderBy(DB::raw(DB::raw('RAND()')))
                ->take(3)
                ->get();

            $properties = Property::with([
                'offer:id,label,suffix',
                'suburb' => function ($query) {
                    $query->select('id', 'name', 'slug', 'area_id');
                    $query->with([
                        'area' => function ($query) {
                            $query->select('id', 'name', 'slug', 'town_id');
                            $query->with([
                                'town' => function ($query) {
                                    $query->select('id', 'name', 'slug', 'province_id');
                                    $query->with([
                                        'province' => function ($query) {
                                            $query->select('id', 'slug');
                                        }
                                    ]);
                                }
                            ]);
                        }
                    ]);
                },
                'type:id,type',
                'agents' => function ($query) {
                    $query->select('id', 'property_id', 'agent_1');
                    $query->with([
                        'agentDetail' => function ($query) {
                            $query->select('id', 'user_id', 'agency_id');
                            $query->with([
                                'agency' => function ($query) {
                                    $query->select('id','slug', 'name', 'logo');
                                    $query->with([
                                        'settings' => function ($query) {
                                            $query->select('id', 'agency_id', 'custom_rate', 'rate');
                                        }
                                    ]);
                                }
                            ]);
                        }
                    ]);
                },
                'features:property_id,bedrooms,lounges,bathrooms,boreholes',
                'images:property_id,main_image'
            ])
                ->select('id', 'suburb_id', 'type_id', 'offer_id', 'ref_code', 'description', 'created_at', 'zwl_price','usd_price')
                ->where(function ($query) use ($refCodes){
                    foreach ($refCodes as $refCode){
                        $query->orWhere('ref_code','LIKE','%'.$refCode.'%');
                    }
                })
                ->whereHas('features',function ($query) use($queries){
                    if(isset($queries['beds'])){
                        $query->where('bedrooms','>=',$queries['beds']);
                    }
                    if(isset($queries['baths'])){
                        $query->where('bathrooms','>=',$queries['baths']);
                    }
                    if(isset($queries['propertyArea'])){
                        $query->where('property_size', '>=',$queries['propertyArea']);
                    }
                })
                ->whereIn('type_id',$pTypes)
                ->whereHas('offer',function ($query) use ($type){
                    $query->where('label',$type);
                })
                ->where('status_id','>',1)
                ->where(function ($query) use ($queries){
                    if(isset($queries['minPrice'])){
                        if(Session::get('selected_currency') == 'usd'){
                            $query->where('usd_price', '>=', $queries['minPrice']);
                        }
                        else{
                            $query->where('zwl_price', '>=', $queries['minPrice']);
                        }
                    }
                    if(isset($queries['maxPrice'])  && $queries['maxPrice'] > 0){
                        if(Session::get('selected_currency') == 'usd'){
                            $query->where('usd_price', '<=', $queries['maxPrice']);
                        }
                        else{
                            $query->where('zwl_price', '<=', $queries['maxPrice']);
                        }
                    }
                })
                ->orderBy($col,$dir)
                ->paginate(15)
                ->appends($request->query());

            if(preg_match('(default|most-recent)',$sortBy) === 1){
                //$sort_props = $properties->sortByDesc('created_at')->sortByDesc('priority');
                $sort_props = $properties->sortByDesc(function ($item){
                    return $item->priority.'#'.$item->created_at;
                });
                $properties = new LengthAwarePaginator($sort_props,$properties->total(),$properties->perPage());
                $properties->setPath(URL::current());
            }

            if($properties->currentPage() > $properties->lastPage()){
                abort(404,'Not Found');
            }

            $pageTitle = $showTypes.strtolower($type).' in '.$propLocations;
            $metaDescription = 'Find your '.$showTypes.strtolower($type).' in '.$propLocations;

        }
        else{
            $featured = $this->advancedFeaturedFilter($type,$suburbs,$areas,$towns,$provinces,$queries,$pTypes);

            $properties = Property::with([
                'offer:id,label,suffix',
                'suburb' => function ($query) {
                    $query->select('id', 'name', 'slug', 'area_id');
                    $query->with([
                        'area' => function ($query) {
                            $query->select('id', 'name', 'slug', 'town_id');
                            $query->with([
                                'town' => function ($query) {
                                    $query->select('id', 'name', 'slug', 'province_id');
                                    $query->with([
                                        'province' => function ($query) {
                                            $query->select('id', 'slug');
                                        }
                                    ]);
                                }
                            ]);
                        }
                    ]);
                },
                'type:id,type',
                'agents' => function ($query) {
                    $query->select('id', 'property_id', 'agent_1');
                    $query->with([
                        'agentDetail' => function ($query) {
                            $query->select('id', 'user_id', 'agency_id');
                            $query->with([
                                'agency' => function ($query) {
                                    $query->select('id','name','slug','logo');
                                    $query->with([
                                        'settings' => function ($query) {
                                            $query->select('id', 'agency_id', 'custom_rate', 'rate');
                                        }
                                    ]);
                                }
                            ]);
                        }
                    ]);
                },
                'features:property_id,bedrooms,lounges,bathrooms,boreholes',
                'images:property_id,main_image'
            ])
                ->select('id', 'suburb_id', 'type_id', 'offer_id', 'ref_code', 'description', 'created_at', 'zwl_price','usd_price')
                ->whereIn('type_id',$pTypes)
                ->whereHas('offer',function ($query) use ($type){
                    $query->where('label',$type);
                })
                ->whereHas('statuses',function ($query){
                    $query->where('status_id','>',1);
                })
                ->whereHas('suburb',function ($query) use ($suburbs,$areas,$towns,$provinces){
                    if(count($suburbs) > 0){
                        $query->whereIn('id',$suburbs);
                        if(count($areas) > 0){
                            $query->orWhereHas('area',function ($query) use ($areas){
                                $query->whereIn('id',$areas);
                            });
                        }
                        if(count($towns) > 0){
                            $query->orWhereHas('area.town',function ($query) use ($towns){
                                $query->whereIn('id',$towns);
                            });
                        }
                        if(count($provinces) > 0){
                            $query->orWhereHas('area.town.province',function ($query) use ($provinces){
                                $query->whereIn('id',$provinces);
                            });
                        }
                    }
                    else{
                        if(count($areas) > 0){
                            $query->whereHas('area',function ($query) use ($areas){
                                $query->whereIn('id',$areas);
                            });
                            if(count($towns) > 0){
                                $query->orWhereHas('area.town',function ($query) use ($towns){
                                    $query->whereIn('id',$towns);
                                });
                            }
                            if(count($provinces) > 0){
                                $query->orWhereHas('area.town.province',function ($query) use ($provinces){
                                    $query->whereIn('id',$provinces);
                                });
                            }
                        }
                        else{
                            if(count($towns) > 0){
                                $query->whereHas('area.town',function ($query) use ($towns){
                                    $query->whereIn('id',$towns);
                                });
                                if(count($provinces) > 0){
                                    $query->orWhereHas('area.town.province',function ($query) use ($provinces){
                                        $query->whereIn('id',$provinces);
                                    });
                                }
                            }
                            else{
                                if(count($provinces) > 0){
                                    $query->whereHas('area.town.province',function ($query) use ($provinces){
                                        $query->whereIn('id',$provinces);
                                    });
                                }
                            }
                        }

                    }
                })
                ->whereHas('features',function ($query) use($queries){
                    if(isset($queries['beds'])){
                        $query->where('bedrooms','>=',$queries['beds']);
                    }
                    if(isset($queries['baths'])){
                        $query->where('bathrooms','>=',$queries['baths']);
                    }
                    if(isset($queries['propertyArea'])){
                        $query->where('property_size', '>=',$queries['propertyArea']);
                    }
                })
                ->whereHas('sites',function ($query){
                    $query->where('site_id',siteID());
                })
                ->where(function ($query) use ($queries){
                    if(isset($queries['minPrice'])){
                        if(Session::get('selected_currency') == 'usd'){
                            $query->where('usd_price', '>=', $queries['minPrice']);
                        }
                        else{
                            $query->where('zwl_price', '>=', $queries['minPrice']);
                        }
                    }
                    if(isset($queries['maxPrice'])  && $queries['maxPrice'] > 0){
                        if(Session::get('selected_currency') == 'usd'){
                            $query->where('usd_price', '<=', $queries['maxPrice']);
                        }
                        else{
                            $query->where('zwl_price', '<=', $queries['maxPrice']);
                        }
                    }
                })
                ->orderBy($col,$dir)
                ->paginate(15)
                ->appends($request->query());
            if(preg_match('(default|most-recent)',$sortBy) === 1){
                $sort_props = $properties->sortByDesc(function ($item){
                    return $item->priority.'#'.$item->created_at;
                });
                $properties = new LengthAwarePaginator($sort_props,$properties->total(),$properties->perPage());
                $properties->setPath(URL::current());
            }

            if($properties->currentPage() > $properties->lastPage()){
                abort(404,'Not Found');
            }

            $pageTitle = $showTypes.strtolower($type).' in '.$propLocations;
            $metaDescription = 'Find your '.$showTypes.$type.' in '.$propLocations;
        }

        if($request->ajax()){
            $view = view('listing', [
                'properties' => $properties,
                'sortValue' => $sortBy,
            ])
                ->render();

            return response()->json([
                'html' => $view,
                'lower_bound' => ($properties->count() > 0 ? ($properties->currentPage() - 1) * $properties->perPage() + 1 : 0),
                'upper_bound' => ($properties->count() < $properties->perPage() ? $properties->total() : ($properties->currentPage() - 1) * $properties->perPage() + $properties->perPage())
            ], 200);
        }
        else{
            $searchQuery = [
                'provinces' => $provs,
                'towns' => $town,
                'areas' => $area,
                'suburbs' => $suburb,
                'parameters' => $queries
            ];

            return view('listings',[
                'properties' => $properties,
                'active' => $filter,
                'type' => $type,
                'area' => 'Multiple Areas',
                'searchQuery' => $searchQuery,
                'sortValue' => $sortBy,
                'featured' => $featured,
                'pageTitle' => $pageTitle,
                'metaDescription' => $metaDescription
            ]);
        }

    }

    /**
     * @param $filter
     * @param $province
     * @param null $town
     * @param null $area
     * @param null $suburb
     * @return string
     */
    public function breadcrumb($filter, $province, $town = null, $area = null, $suburb = null)
    {
        $breadcrumb = '<a href="' . route('filterProperties', ['filter' => $filter, 'province' => $province]) . '" class="breadcrumb-item ' . (!$town ? 'last' : '') . '">' . ucwords(str_replace('-', ' ', $province)) . '</a>';
        if ($town) {
            $breadcrumb .= '<a href="' . route('filterProperties', ['filter' => $filter, 'province' => $province, 'town' => $town]) . '" class="breadcrumb-item ' . (!$area ? 'last' : '') . '">' . ucwords(str_replace('-', ' ', $town)) . '</a>';
        }
        if ($area) {
            $breadcrumb .= '<a href="' . route('filterProperties', ['filter' => $filter, 'province' => $province, 'town' => $town, 'area' => $area]) . '" class="breadcrumb-item ' . (!$suburb ? 'last' : '') . '">' . ucwords(str_replace('-', ' ', $area)) . '</a>';
        }
        if ($suburb) {
            $breadcrumb .= '<a href="' . route('filterProperties', ['filter' => $filter, 'province' => $province, 'town' => $town, 'area' => $area, 'suburb' => $suburb]) . '" class="breadcrumb-item last">' . ucwords(str_replace('-', ' ', $suburb)) . '</a>';
        }
        return $breadcrumb;
    }

    /**
     * Generate the type link
     * @param $filter
     * @param $province
     * @param null $town
     * @param null $area
     * @param null $suburb
     * @return string
     */
    public function typeLink($filter, $province, $town = null, $area = null, $suburb = null)
    {
        if ($suburb) {
            $typeLink = route('filterProperties', ['filter' => $filter, 'province' => $province, 'town' => $town, 'area' => $area, 'suburb' => $suburb]);
        } elseif ($area) {
            $typeLink = route('filterProperties', ['filter' => $filter, 'province' => $province, 'town' => $town, 'area' => $area]);
        } elseif ($town) {
            $typeLink = route('filterProperties', ['filter' => $filter, 'province' => $province, 'town' => $town]);
        } else {
            $typeLink = route('filterProperties', ['filter' => $filter, 'province' => $province]);
        }
        return $typeLink;
    }

    /**
     * Get only developments
     * @param Request $request
     * @param null $search
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     * @throws \Throwable
     */
    public function getDevelopments(Request $request, $search = null)
    {
        $queries = $request->query();
        if (isset($queries['sortBy'])) {
            if ($queries['sortBy'] == 'most-recent') {
                $col = 'created_at';
                $dir = 'desc';
            } elseif ($queries['sortBy'] == 'price-high') {
                if(Session::get('selected_currency') == 'usd'){
                    $col = 'usd_price';
                }
                else{
                    $col = 'zwl_price';
                }
                $dir = 'desc';
            } elseif ($queries['sortBy'] == 'price-low') {
                if(Session::get('selected_currency') == 'usd'){
                    $col = 'usd_price';
                }
                else{
                    $col = 'zwl_price';
                }
                $dir = 'asc';
            } else {
                $col = 'created_at';
                $dir = 'desc';
            }
            $sortBy = 'sortBy=' . $queries['sortBy'];
        } else {
            $col = 'created_at';
            $dir = 'desc';
            $sortBy = 'sortBy=default';
        }

        if (isset($queries['page']) && $queries['page'] > 1) {
            $skip = ($queries['page'] - 1) * 3;
        } else {
            $skip = 0;
        }

        $featured = FeaturedListing::with('property')
            ->where('site_id',siteID())
            ->where('start_date','<=',date('Y-m-d'))
            ->where('expiry_date','>=',date('Y-m-d'))
            ->where('pb_approval',1)
            ->whereHas('property',function ($query){
                $query->whereHas('attributes',function($query){
                   $query->where('new_development',1);
                });
                $query->whereHas('statuses',function ($query){
                    $query->where('status_id','>',1);
                    $query->where('status_id','!=',3);
                });
                $query->where('active',1);
            })
            ->orderBy(DB::raw('RAND()'))
            ->take(3)
            ->get();

        $locations = '';

        if (!$search) {
            $properties = Property::with([
                'offer:id,label,suffix',
                'suburb' => function ($query) {
                    $query->select('id', 'name', 'slug', 'area_id');
                    $query->with([
                        'area' => function ($query) {
                            $query->select('id', 'name', 'slug', 'town_id');
                            $query->with([
                                'town' => function ($query) {
                                    $query->select('id', 'name', 'slug', 'province_id');
                                    $query->with([
                                        'province' => function ($query) {
                                            $query->select('id', 'slug');
                                        }
                                    ]);
                                }
                            ]);
                        }
                    ]);
                },
                'type:id,type',
                'agents' => function ($query) {
                    $query->select('id', 'property_id', 'agent_1');
                    $query->with([
                        'agentDetail' => function ($query) {
                            $query->select('user_id', 'agency_id');
                            $query->with([
                                'agency' => function ($query) {
                                    $query->select('id','name','slug','logo','hex_code','text_color','overlay');
                                    $query->with([
                                        'settings' => function ($query) {
                                            $query->select('agency_id', 'custom_rate', 'rate');
                                        }
                                    ]);
                                }
                            ]);
                        }
                    ]);
                },
                'features:property_id,bedrooms,lounges,bathrooms,boreholes',
                'images:property_id,main_image'
            ])
                ->select('id', 'suburb_id', 'type_id', 'offer_id', 'ref_code', 'description', 'created_at', 'zwl_price','usd_price')
                ->whereHas('attributes',function ($query){
                    $query->where('new_development',1);
                })
                ->whereHas('statuses',function ($query){
                    $query->where('status_id','>',1);
                })
                ->whereHas('sites',function ($query){
                    $query->where('site_id',siteID());
                })
                ->where('active',1)
                ->orderBy($col, $dir)
                ->paginate(15);
            $searchQuery = [];
        } else {
            $searchItems = explode('&', $search);
            $provinces = [];
            $provs = [];
            $towns = [];
            $twns = [];
            $areas = [];
            $ars = [];
            $suburbs = [];
            $sbs = [];
            $refCodes = [];
            foreach ($searchItems as $item) {
                $loc = explode('=', $item);
                if (substr($loc[0], 0, 9) == 'provinces') {
                    $provinces[] = $loc[1];
                    $lg = Province::select('id', 'name')->where('id', $loc[1])->first();
                    if (!empty($lg)) {
                        $locations .= $lg->name . ', ';
                        $provs[] = [
                            'id' => $lg->id,
                            'display' => $lg->name
                        ];
                    }
                } elseif (substr($loc[0], 0, 5) == 'towns') {
                    $towns[] = $loc[1];
                    $lg = Town::select('id', 'name')->where('id', $loc[1])->first();
                    if (!empty($lg)) {
                        $locations .= $lg->name . ', ';
                        $twns[] = [
                            'id' => $lg->id,
                            'display' => $lg->name
                        ];
                    }
                } elseif (substr($loc[0], 0, 5) == 'areas') {
                    $areas[] = $loc[1];
                    $lg = Area::select('id', 'name')->where('id', $loc[1])->first();
                    if (!empty($lg)) {
                        $locations .= $lg->name . ', ';
                        $ars[] = [
                            'id' => $lg->id,
                            'display' => $lg->name
                        ];
                    }
                } elseif (substr($loc[0], 0, 7) == 'suburbs') {
                    $suburbs[] = $loc[1];
                    $lg = Suburb::select('id', 'name')->where('id', $loc[1])->first();
                    if (!empty($lg)) {
                        $locations .= $lg->name . ', ';
                        $sbs[] = [
                            'id' => $lg->id,
                            'display' => $lg->name
                        ];
                    }
                } elseif (substr($loc[0], 0, 8) == 'refCodes') {
                    return redirect()->route('developments');
                }
            }

            if (count($refCodes) > 0) {
                return redirect()->route('developments');
            } else {
                $properties = Property::with([
                    'offer:id,label,suffix',
                    'suburb' => function ($query) {
                        $query->select('id', 'name', 'slug', 'area_id');
                        $query->with([
                            'area' => function ($query) {
                                $query->select('id', 'name', 'slug', 'town_id');
                                $query->with([
                                    'town' => function ($query) {
                                        $query->select('id', 'name', 'slug', 'province_id');
                                        $query->with([
                                            'province' => function ($query) {
                                                $query->select('id', 'slug');
                                            }
                                        ]);
                                    }
                                ]);
                            }
                        ]);
                    },
                    'type:id,type',
                    'agents' => function ($query) {
                        $query->select('id', 'property_id', 'agent_1');
                        $query->with([
                            'agentDetail' => function ($query) {
                                $query->select('user_id', 'agency_id');
                                $query->with([
                                    'agency' => function ($query) {
                                        $query->select('id','name','slug','logo','hex_code','text_color','overlay');
                                        $query->with([
                                            'settings' => function ($query) {
                                                $query->select('agency_id', 'custom_rate', 'rate');
                                            }
                                        ]);
                                    }
                                ]);
                            }
                        ]);
                    },
                    'features:property_id,bedrooms,lounges,bathrooms,boreholes',
                    'images:property_id,main_image'
                ])
                    ->select('id', 'suburb_id', 'type_id', 'offer_id', 'ref_code', 'description', 'created_at', 'zwl_price','usd_price')
                    ->whereHas('features', function ($query) use ($queries) {
                        if (isset($queries['beds'])) {
                            $query->where('bedrooms', '>=', $queries['beds']);
                        }
                        if (isset($queries['baths'])) {
                            $query->where('bathrooms', '>=', $queries['baths']);
                        }
                    })
                    ->whereHas('attributes',function ($query){
                        $query->where('new_development',1);
                    })
                    ->whereHas('statuses',function($query){
                        $query->where('status_id','>',1);
                    })
                    ->whereHas('suburb', function ($query) use ($suburbs, $areas, $towns, $provinces) {
                        $query->select('id', 'area_id');
                        if (count($suburbs) > 0) {
                            $query->whereIn('id', $suburbs);
                        }
                        if (count($suburbs) > 0) {
                            if (count($areas) > 0) {
                                $query->orWhereHas('area', function ($query) use ($areas, $towns, $provinces) {
                                    $query->whereIn('id', $areas);
                                    $query->select('id', 'town_id');
                                    if (count($towns) > 0) {
                                        $query->whereHas('town', function ($query) use ($towns, $provinces) {
                                            $query->select('id', 'province_id');
                                            $query->whereIn('id', $towns);
                                            if (count($provinces) > 0) {
                                                $query->whereHas('province', function ($query) use ($provinces) {
                                                    $query->select('id');
                                                    $query->whereIn('id', $provinces);
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        } else {
                            if (count($areas) > 0) {
                                $query->whereHas('area', function ($query) use ($areas) {
                                    $query->whereIn('id', $areas);
                                });
                                if (count($towns) > 0) {
                                    $query->orWhereHas('area.town', function ($query) use ($towns) {
                                        $query->whereIn('id', $towns);
                                    });
                                }
                                if (count($provinces) > 0) {
                                    $query->orWhereHas('area.town.province', function ($query) use ($provinces) {
                                        $query->whereIn('id', $provinces);
                                    });
                                }
                            } else {
                                if (count($towns) > 0) {
                                    $query->whereHas('area.town', function ($query) use ($towns) {
                                        $query->whereIn('id', $towns);
                                    });
                                    if (count($provinces) > 0) {
                                        $query->orWhereHas('area.town.province', function ($query) use ($provinces) {
                                            $query->whereIn('id', $provinces);
                                        });
                                    }
                                } else {
                                    if (count($provinces) > 0) {
                                        $query->whereHas('area.town.province', function ($query) use ($provinces) {
                                            $query->whereIn('id', $provinces);
                                        });
                                    }
                                }
                            }

                        }
                    })
                    ->where(function ($query) use ($queries) {
                        if (isset($queries['minPrice'])) {
                            if(Session::get('selected_currency') == 'usd'){
                                $query->where('usd_price', '>=', $queries['minPrice']);
                            }
                            else{
                                $query->where('zwl_price', '>=', $queries['minPrice']);
                            }
                        }
                        if (isset($queries['maxPrice']) && $queries['maxPrice'] > 0) {
                            if(Session::get('selected_currency') == 'usd'){
                                $query->where('usd_price', '<=', $queries['maxPrice']);
                            }
                            else{
                                $query->where('zwl_price', '<=', $queries['maxPrice']);
                            }
                        }
                    })
                    ->whereHas('sites',function($query){
                        $query->where('site_id',siteID());
                    })
                    ->where('active',1)
                    ->orderBy($col, $dir)
                    ->paginate(15)
                    ->appends($request->query());
            }
            $searchQuery = [
                'provinces' => $provs,
                'towns' => $twns,
                'areas' => $ars,
                'suburbs' => $sbs,
                'parameters' => $queries
            ];
        }
        if (preg_match('(default|most-recent)', $sortBy) === 1) {
            $sort_props = $properties->sortByDesc(function ($item) {
                return $item->priority . '#' . $item->created_at;
            });
            $properties = new LengthAwarePaginator($sort_props, $properties->total(), $properties->perPage());
            $properties->setPath(URL::current());
        }
        if ($properties->currentPage() > $properties->lastPage()) {
            abort(404, 'Not Found');
        }

        if ($request->ajax()) {
            $view = view('listing', [
                'properties' => $properties,
                'sortValue' => $sortBy,
            ])
                ->render();

            return response()->json([
                'html' => $view,
                'lower_bound' => ($properties->count() > 0 ? ($properties->currentPage() - 1) * $properties->perPage() + 1 : 0),
                'upper_bound' => ($properties->count() < $properties->perPage() ? $properties->total() : ($properties->currentPage() - 1) * $properties->perPage() + $properties->perPage())
            ], 200);
        } else {
            if ($locations != '') {
                $places = $locations;
            } else {
                $places = 'Zimbabwe';
            }
            $pageTitle = 'Property Developments';
            $pageImage = URL::asset('/assets/img/generic-site-image.jpg');
            $metaDescription = 'Find your Property Developments in ' . $places;
            $breadcrumb = '<a href="' . route('developments') . '" class="breadcrumb-item last">Developments</a>';
            return view('listings', [
                'properties' => $properties,
                'active' => 'developments',
                'type' => 'for Sale',
                'listingType' => 'Developments',
                'featured' => $featured,
                'sortValue' => $sortBy,
                'searchQuery' => $searchQuery,
                'pageTitle' => $pageTitle,
                'pageImage' => $pageImage,
                'metaDescription' => $metaDescription,
                'area' => '',
                'breadcrumb' => $breadcrumb
            ]);
        }

    }

    /**
     * Search developments
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function searchDevelopments(Request $request)
    {
        $parameters = '';
        if (!empty($request['minPrice'])) {
            $parameters .= ($parameters == '' ? '' : '&') . 'minPrice=' . $request['minPrice'];
        }
        if (!empty($request['maxPrice'])) {
            $parameters .= ($parameters == '' ? '' : '&') . 'maxPrice=' . $request['maxPrice'];
        }
        if (!empty($request['beds'])) {
            $parameters .= ($parameters == '' ? '' : '&') . 'beds=' . $request['beds'];
        }
        if (!empty($request['baths'])) {
            $parameters .= ($parameters == '' ? '' : '&') . 'baths=' . $request['baths'];
        }
        if (!empty($request['propertyArea'])) {
            $parameters .= ($parameters == '' ? '' : '&') . 'propertyArea=' . $request['propertyArea'];
        }

        $query = $request['searchText'];
        $provinces = [];
        $towns = [];
        $areas = [];
        $suburbs = [];
        $refCodes = [];
        foreach ($query as $item) {
            $location = explode('_', $item);
            if (count($location) > 1) {
                if ($location[0] == 'Province') {
                    $provinces[] = $location[1];
                } elseif ($location[0] == 'Town') {
                    $towns[] = $location[1];
                } elseif ($location[0] == 'Area') {
                    $areas[] = $location[1];
                } elseif ($location[0] == 'Suburb') {
                    $suburbs[] = $location[1];
                }
            } else {
                $refCodes[] = $location;
            }
        }

        $search = [
            'provinces' => $provinces,
            'towns' => $towns,
            'areas' => $areas,
            'suburbs' => $suburbs,
            //'refCodes' => $refCodes
        ];
        $search = http_build_query($search);
        return redirect()->route('developments', ['search' => $search . ($parameters != '' ? '?' . $parameters : '')]);
    }

    /**
     * Display list of agencies
     * Can be filtered based on first letter of the name
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAgencies(Request $request)
    {
        $queries = $request->query();
        if (isset($queries['filter'])) {
            $filter = $queries['filter'];
        } else {
            $filter = '';
        }
        $letters = range('A', 'Z');
        $agencies = Cache::tags('agencies')->rememberForever('listed_agencies-'.$filter.'_'.siteID(),function () use ($filter){
            return Agency::with([
                'branches:id,agency_id'
            ])
                ->select('id','name','slug','logo','overview')
                ->where('active', 1)
                ->whereHas('bandSub',function ($query){
                    $query->whereHas('package',function ($query){
                        $query->whereHas('packageBands',function ($query){
                            $query->where('site_id',siteID());
                        });
                    });
                })
                ->where(function ($query) use ($filter) {
                    if ($filter != '') {
                        $query->where('name', 'LIKE', $filter . '%');
                    }
                })
                ->orderBy('name')
                ->get();
        });

        $pageTitle = 'Registered Real Estate Agents';
        $pageImage = URL::asset('/assets/fronted/img/generic-site-image.jpg');
        $metaDescription = 'Registered Real Estate Agents in Zimbabwe. Sell or rent your property using professional registered real estate agents in Zimbabwe.';
        return view('agencies', [
            'agencies' => $agencies,
            'type' => 'agencies',
            'active' => 'agents',
            'letters' => $letters,
            'currentLetter' => $filter,
            'pageTitle' => $pageTitle,
            'pageImage' => $pageImage,
            'metaDescription' => $metaDescription
        ]);
    }

    /**
     * Build search parameters for for search agencies
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function findAgency(Request $request){
        if(empty($request['searchText'])){
            return redirect()->route('listedAgencies');
        }
        elseif(count($request['searchText']) == 1){
            $agency = Agency::select('slug')->where('id',$request['searchText']['0'])->firstOrFail();
            return redirect()->route('viewAgency',['agency' => $agency->slug]);
        }
        else{
            $agencies = '';
            $last = count($request['searchText']) - 1;
            foreach ($request['searchText'] as $key => $search){
                if($key != $last){
                    $agencies .= $search.'-';
                }
                else{
                    $agencies .= $search;
                }
            }
            return redirect()->route('filterAgencies',['agencies' => $agencies]);
        }
    }

    /**
     * Search for agencies based on supplied parameters
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function filterAgencies(Request $request){
        $query = $request->query();

        $IDs = explode('-',$query['agencies']);

        $agencies = Agency::with([
            'branches:id,agency_id'
        ])
            ->select('id','name','slug','logo','overview')
            ->where('active',1)
            ->whereIn('id',$IDs)
            ->orderBy('name','asc')
            ->paginate(15);
        $pageTitle = 'Registered Real Estate Agents';
        $pageImage = URL::asset('/assets/fronted/img/generic-site-image.jpg');
        $metaDescription = 'Registered Real Estate Agents in Zimbabwe. Sell or rent your property using professional registered real estate agents in Zimbabwe.';
        $letters = range('A','Z');
        return view('agencies',[
            'agencies' => $agencies,
            'type' => 'agencies',
            'active' => 'agents',
            'pageTitle' => $pageTitle,
            'pageImage' => $pageImage,
            'metaDescription' => $metaDescription,
            'letters' => $letters,
            'currentLetter' => ''
        ]);
    }

    /**
     * Display a given agency
     * @param $agency
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function viewAgency($agency){
        $agency = Agency::with([
            'branches:id,agency_id,slug,name'
        ])
            ->select('id','slug','name','logo','overview')
            ->where('slug',$agency)
            ->where('active',1)
            ->firstOrFail();
        if($agency->branches->count() == 1){
            return redirect()->route('viewBranch',['agency' => $agency->slug,'branch' => $agency->branches->first()->slug]);
        }
        else{
            $pageTitle = $agency->name;
            $pageImage = URL::asset('/assets/images/agency-logos/'.$agency->logo);
            $metaDescription = 'Propertybook | '.$agency->name;
            return view('agencies',[
                'agency' => $agency,
                'type' => 'branches',
                'active' => 'agents',
                'pageTitle' => $pageTitle,
                'pageImage' => $pageImage,
                'metaDescription' => $metaDescription
            ]);
        }

    }

    /**
     * Display a given branch
     * Can also filter properties for the branch based on sales/rentals
     * @param Request $request
     * @param $agency
     * @param $branch
     * @param string $filter
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewBranch(Request $request,$agency,$branch,$filter = ''){
        $bran = Branch::with([
            'agency'
        ])
            ->whereHas('agency',function ($query) use ($agency){
                $query->select('id');
                $query->where('slug',$agency);
            })
            ->where('slug',$branch)
            ->firstOrFail();


        $queries = $request->query();
        if(isset($queries['sortBy'])){
            if($queries['sortBy'] == 'most-recent'){
                $col = 'created_at';
                $dir = 'desc';
            }
            elseif($queries['sortBy'] == 'price-high'){
                if(Session::get('selected_currency') == 'usd'){
                    $col = 'usd_price';
                }
                else{
                    $col = 'zwl_price';
                }
                $dir = 'desc';
            }
            elseif($queries['sortBy'] == 'price-low'){
                if(Session::get('selected_currency') == 'usd'){
                    $col = 'usd_price';
                }
                else{
                    $col = 'zwl_price';
                }
                $dir = 'asc';
            }
            else{
                $col = 'created_at';
                $dir = 'desc';
            }
            $sortBy = 'sortBy='.$queries['sortBy'];
        }
        else{
            $col = 'created_at';
            $dir = 'desc';
            $sortBy = 'sortBy=default';
        }

        if(isset($queries['page']) && $queries['page'] > 1){
            $skip = ($queries['page'] - 1) * 3;
        }
        else{
            $skip = 0;
        }

        if(strlen($filter) == 0){
            $label = '';
        }
        else{
            if($filter == 'for-sale'){
                $label = 'for Sale';
            }
            elseif($filter = 'to-rent'){
                $label = 'to Rent';
            }
        }


        $properties = Property::with([
            'offer:id,label,suffix',
            'suburb' => function ($query) {
                $query->select('id', 'name', 'slug', 'area_id');
                $query->with([
                    'area' => function ($query) {
                        $query->select('id', 'name', 'slug', 'town_id');
                        $query->with([
                            'town' => function ($query) {
                                $query->select('id', 'name', 'slug', 'province_id');
                                $query->with([
                                    'province' => function ($query) {
                                        $query->select('id', 'slug');
                                    }
                                ]);
                            }
                        ]);
                    }
                ]);
            },
            'type:id,type',
            'agents' => function ($query) {
                $query->select('id', 'property_id', 'agent_1');
                $query->with([
                    'agentDetail' => function ($query) {
                        $query->select('user_id', 'agency_id');
                        $query->with([
                            'agency' => function ($query) {
                                $query->select('id','name','slug','logo','hex_code','text_color','overlay');
                                $query->with([
                                    'settings' => function ($query) {
                                        $query->select('agency_id', 'custom_rate', 'rate');
                                    }
                                ]);
                            }
                        ]);
                    }
                ]);
            },
            'features:property_id,bedrooms,lounges,bathrooms,boreholes',
            'images:property_id,main_image'
        ])
            ->select('id', 'suburb_id', 'type_id', 'offer_id', 'ref_code', 'description', 'created_at', 'zwl_price','usd_price')
            ->whereHas('statuses',function ($query){
                $query->where('status_id','>',1);
            })
            ->where('active',1)
            ->whereHas('offer',function ($query) use ($label){
                if($label != ''){
                    $query->where('label',$label);
                }
            })
            ->whereHas('sites',function ($query){
                $query->where('site_id',siteID());
            })
            ->orderBy($col,$dir)
            ->paginate(15);

        if($properties->currentPage() > $properties->lastPage()){
            abort(404,'Not Found');
        }

        $pageTitle = $bran->agency->name.' | '.$bran->name;
        $pageImage = URL::asset('/assets/images/agency-logos/'.$bran->agency->logo);
        $metaDescription = 'Propertybook | '.$bran->agency->name.' - '.$bran->name;

        return view('agencies',[
            'branch' => $bran,
            'properties' => $properties,
            'type' => 'branch',
            'active' => 'agents',
            'current' => $filter,
            'sortValue' => $sortBy,
            'pageTitle' => $pageTitle,
            'pageImage' => $pageImage,
            'metaDescription' => $metaDescription
        ]);
    }

    /**
     * View properties for a given agency based on sales/rentals filter
     * @param Request $request
     * @param $agency
     * @param $filter
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewAgencyProperties(Request $request,$agency,$filter){
        $agency = Agency::with([
            'branches:id,agency_id,slug,name'
        ])
            ->where('slug',$agency)
            ->where('active',1)
            ->firstOrFail();
        if($filter == 'for-sale'){
            $label = 'for Sale';
        }
        elseif($filter == 'to-rent'){
            $label = 'to Rent';
        }
        else{
            $label = null;
        }

        $queries = $request->query();

        if(isset($queries['sortBy'])){
            if($queries['sortBy'] == 'most-recent'){
                $col = 'created_at';
                $dir = 'desc';
            }
            elseif($queries['sortBy'] == 'price-high'){
                if(Session::get('selected_currency') == 'usd'){
                    $col = 'usd_price';
                }
                else{
                    $col = 'zwl_price';
                }
                $dir = 'desc';
            }
            elseif($queries['sortBy'] == 'price-low'){
                if(Session::get('selected_currency') == 'usd'){
                    $col = 'usd_price';
                }
                else{
                    $col = 'zwl_price';
                }
                $dir = 'asc';
            }
            else{
                $col = 'created_at';
                $dir = 'desc';
            }
            $sortBy = 'sortBy='.$queries['sortBy'];
        }
        else{
            $col = 'created_at';
            $dir = 'desc';
            $sortBy = 'sortBy=default';
        }

        if(isset($queries['page']) && $queries['page'] > 1){
            $skip = ($queries['page'] - 1) * 3;
        }
        else{
            $skip = 0;
        }

        $properties = Property::with([
            'offer:id,label,suffix',
            'suburb' => function ($query) {
                $query->select('id', 'name', 'slug', 'area_id');
                $query->with([
                    'area' => function ($query) {
                        $query->select('id', 'name', 'slug', 'town_id');
                        $query->with([
                            'town' => function ($query) {
                                $query->select('id', 'name', 'slug', 'province_id');
                                $query->with([
                                    'province' => function ($query) {
                                        $query->select('id', 'slug');
                                    }
                                ]);
                            }
                        ]);
                    }
                ]);
            },
            'type:id,type',
            'agents' => function ($query) {
                $query->select('id', 'property_id', 'agent_1');
                $query->with([
                    'agentDetail' => function ($query) {
                        $query->select('user_id', 'agency_id');
                        $query->with([
                            'agency' => function ($query) {
                                $query->select('id','name','slug','logo','hex_code','text_color','overlay');
                                $query->with([
                                    'settings' => function ($query) {
                                        $query->select('agency_id', 'custom_rate', 'rate');
                                    }
                                ]);
                            }
                        ]);
                    }
                ]);
            },
            'features:property_id,bedrooms,lounges,bathrooms,boreholes',
            'images:property_id,main_image'
        ])
            ->select('id', 'suburb_id', 'type_id', 'offer_id', 'ref_code', 'description', 'created_at', 'zwl_price','usd_price')
            ->where('active',1)
            ->whereHas('offer',function ($query) use ($label){
                if($label){
                    $query->where('label',$label);
                }
            })
            ->whereHas('statuses',function ($query){
                $query->where('status_id','>',1);
            })
            ->whereHas('sites',function ($query){
                $query->where('site_id',siteID());
            })
            ->orderBy($col,$dir)
            ->paginate(15);
        if($properties->currentPage() > $properties->lastPage()){
            abort(404,'Not Found');
        }
        $pageTitle = $agency->name;
        $pageImage = imageURL().'agency-logos/'.$agency->logo;
        $metaDescription = 'Propertybook | '.$agency->name;
        return view('agencies',[
            'agency' => $agency,
            'properties' => $properties,
            'type' => 'agencyProperties',
            'active' => 'agents',
            'current' => $filter,
            'sortValue' => $sortBy,
            'pageTitle' => $pageTitle,
            'pageImage' => $pageImage,
            'metaDescription' => $metaDescription
        ]);
    }

    public function competition(){
        return view('competition',[
            'pageTitle' => 'Rebrand Competition',
            'active' => '',
            'metaDescription' => 'To celebrate our 5th birthday as well as our rebranding we are running a Ã¢ÂÂRebrand CompetitionÃ¢ÂÂ during the month of July. Enter now to stand a chance to win one of our weekly prizes and stay in the running for our grand draw on the 31st July.',
            'pageImage' => URL::asset('/assets/img/competition/facebook-image.jpg')
        ]);
    }

    public function newLook(){
        return view('new-look',[
            'pageTitle' => 'The Fresh New Look',
            'active' => '',
            'metaDescription' => 'Evolving business cultures require that businesses adapt. Propertybook, as a trusted, professional and reliable brand is making a concerted effort into providing the best possible user experience while offering a world-class service to Estate Agents.'
        ]);
    }

    public function competitionSub(Request $request){
        $request->validate([
            'sub_name' => 'required',
            'sub_surname' => 'required',
            'sub_email' => 'required|email|unique:competition_subscribers,email'
        ],[
            'sub_name.required' => 'Enter your name',
            'sub_surname.required' => 'Enter your surname',
            'sub_email.required' => 'Enter your email',
            'sub_email.email' => 'Enter a valid email',
            'sub_email.unique' => 'You have already subscribed'
        ]);

        $sub = new CompetitionSubscriber();
        $sub->name = $request['sub_name'];
        $sub->surname = $request['sub_surname'];
        $sub->email = $request['sub_email'];
        $sub->save();

        try {
            $mailchimp = new MailChimp(env('MAILCHIMP_APIKEY'));
            $result = $mailchimp->post("lists/dc749d3a09/members", [
                'email_address' => $sub->email,
                'status' => 'subscribed',
            ]);
        }
        catch (\Exception $e){
            report($e);
        }
        return response()->json('Thank you for subscribing',200);
    }

    public function sellWithUs(){
        return view('sell-with-us',[
            'pageTitle' => 'Sell With Us',
            'active' => '',
            'metaDescription' => 'With over 150,000 monthly visits, Propertybook offers you the best marketing exposure for your properties in Harare along with the rest of Zimbabwe. Sell or rent out your property on ZimbabweÃ¢ÂÂs leading property portal.'
        ]);
    }

    /**
     * Display a general page
     * This covers 'static' pages
     * @param $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getGeneralPage($page){
        if($page == 'home'){
            return redirect()->route('home');
        }
        elseif($page == 'rates-sheet'){
            return  redirect()->route('generalPage',['page' => 'pricing']);
        }
        elseif($page == 'pricing'){
            $packages = Package::where('type','agency')
                ->where('display',1)
                ->orderBy('position')
                ->get();
            $bands = Band::whereHas('packageBands.package',function ($query){
                $query->where('type','agency');
            })
                ->where('display',1)
                ->orderBy('properties')
                ->get();
            $sites = Site::all();
            $addons = Addon::with('addOnSite')->get();

            return view('rate-sheet',[
                'pageTitle' => 'Price Sheet',
                'active' => 'rates',
                'metaDescription' => 'Propertybook price sheet',
                'packages' => $packages,
                'bands' => $bands,
                'sites' => $sites,
                'addons' => $addons
            ]);
        }
        $page = Page::where('slug',$page)
            ->where('published',1)
            ->firstOrFail();
        $pageTitle = $page->title;
        $pageImage = imageURL().'pages/'.$page->image;
        return view('static-page',[
            'active' => 'general',
            'properties' => $this->latestPropertiesSidebar(),
            'page' => $page,
            'blogs' => $this->latestArticles(),
            'pageTitle' => $pageTitle,
            'pageImage' => $pageImage,
            'genPage' => $page->slug,
            'metaDescription' => $page->meta
        ]);
    }

    public function specialPage($type){
        $page = StaticPage::where('slug',$type)
            ->where('visible',1)
            ->firstOrFail();
        if($type == 'vote'){
            $pageTitle = $page->title;
            $pageImage = URL::asset('/assets/images/pages/'.$page->image);
            $agencies = Agency::where('active',1)->orderBy('name','asc')->get();
            return view('voting',[
                'active' => 'general',
                'properties' => $this->latestPropertiesSidebar(),
                'page' => $page,
                'blogs' => $this->latestArticles(),
                'pageTitle' => $pageTitle,
                'pageImage' => $pageImage,
                'metaDescription' => $page->meta,
                'genPage' => $page->url,
                'agencies' => $agencies
            ]);
        }
        else{
            abort(404);
        }
    }

    public function votingActions(Request $request,$type,$id = null){
        if($type == 'get-branches'){
            $branches = Branch::select('id','name')
                ->where('agency_id',$id)
                ->orderBy('name','asc')
                ->get();
            if(count($branches) == 1){
                $agents = User::select('id','name','surname')
                    ->whereHas('agentDetail',function ($query) use ($id){
                        $query->where('agency_id',$id);
                    })
                    ->orderBy('name','asc')
                    ->orderBy('surname','asc')
                    ->get();
                return response()->json([
                    'branch' => 0,
                    'agents' => $agents
                ],200);
            }
            else{
                return response()->json([
                    'branch' => 1,
                    'branches' => $branches
                ],200);
            }
        }
        elseif($type == 'get-agents'){
            $agents = User::select('id','name','surname')
                ->whereHas('agentDetail',function($query) use ($id){
                    $query->where('branch_id',$id);
                })
                ->orderBy('name','asc')
                ->get();
            return response()->json($agents,200);
        }
        elseif($type == 'cast-vote'){
            $request->validate([
                'name' => 'required',
                'voter_email' => 'required|email|unique:votes,email',
                'agency' => 'required',
                'agent' => 'required',
                //'g-recaptcha-response' => 'required',
                'type' => 'required'
            ],[
                'name.required' => 'Please enter your name',
                'voter_email.required' => 'Please enter your email address',
                'voter_email.email' => 'Please enter a valid email address',
                'voter_email.unique' => 'This email has already voted',
                'agency.required' => 'Please select an agency',
                'agent.required' => 'Please select an agent',
                'g-recaptcha-response.required' => 'Please tick the box to show you are not a robot',
                'type.required' => 'Please select the type of listing you are voting for'
            ]);

            $agent = User::find($request['agent']);

            $vote = new Vote();
            $vote->name = $request['name'];
            $vote->email = $request['voter_email'];
            $vote->type = $request['type'];
            $vote->agency_id = $request['agency'];
            $vote->branch_id = $agent->agentDetail->branch_id;
            $vote->agent_id = $request['agent'];
            $vote->ip_address = $request->getClientIp();
            $vote->save();

            return response()->json('Vote submitted',200);
        }
    }


    /**
     * Get latest properties to be displayed in the sidebar
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Query\Builder[]|\Illuminate\Support\Collection|Property[]
     */
    public function latestPropertiesSidebar(){
        return Property::with([
            'offer:id,label,suffix',
            'suburb' => function ($query) {
                $query->select('id', 'name', 'slug', 'area_id');
                $query->with([
                    'area' => function ($query) {
                        $query->select('id', 'name', 'slug', 'town_id');
                        $query->with([
                            'town' => function ($query) {
                                $query->select('id', 'name', 'slug', 'province_id');
                                $query->with([
                                    'province' => function ($query) {
                                        $query->select('id', 'slug');
                                    }
                                ]);
                            }
                        ]);
                    }
                ]);
            },
            'type:id,type',
            'agents' => function ($query) {
                $query->select('id', 'property_id', 'agent_1');
                $query->with([
                    'agentDetail' => function ($query) {
                        $query->select('user_id', 'agency_id');
                        $query->with([
                            'agency' => function ($query) {
                                $query->select('id','name','slug','logo','hex_code','text_color','overlay');
                                $query->with([
                                    'settings' => function ($query) {
                                        $query->select('agency_id', 'custom_rate', 'rate');
                                    }
                                ]);
                            }
                        ]);
                    }
                ]);
            },
            'features:property_id,bedrooms,lounges,bathrooms,boreholes',
            'images:property_id,main_image'
        ])
            ->select('id', 'suburb_id', 'type_id', 'offer_id', 'ref_code', 'description', 'created_at', 'zwl_price','usd_price')
            ->whereHas('statuses',function ($query){
                $query->where('status_id','>',1);
            })
            ->whereHas('sites',function ($query){
                $query->where('site_id',siteID());
            })
            ->where('active',1)
            ->orderBy('created_at','desc')
            ->take(5)
            ->get();
    }

    /**
     * Get latest articles
     * @param null $articleID
     * @return mixed
     */
    public function latestArticles($articleID = null){
        return $blogs = Blog::select('title','slug','image')
            ->where('published',1)
            ->where(function ($query) use ($articleID){
                if($articleID){
                    $query->where('id','!=',$articleID);
                }
            })
            ->orderBy('created_at','desc')
            ->take(6)
            ->get();
    }

    /**
     * Display list of blog articles if $article param is empty else display that particular article
     * @param string $article
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getBlog($article = ''){
        if(isset($_GET['page'])){
            $page = $_GET['page'];
        }
        else{
            $page = 1;
        }
        if(!$article){
            $articles = Cache::tags(['blog'])->rememberForever('blog_page_'.$page.'_'.siteID(),function (){
                return Blog::select('slug','title','image','content')
                    ->where('site_id',siteID())
                    ->where('published',1)
                    ->orderBy('created_at','desc')
                    ->paginate(10);
            });
            if($articles->count() > 0){
                $pageImage = imageURL().'blog/'.$articles->first()->image;
            }
            else{
                $pageImage = URL::asset('/assets/img/generic-site-image.jpg');
            }

            $pageTitle = 'Propertybook Blog';

            $metaDescription = 'Browse through our blog section for property advice, information as well as insights from our extensive list of EAC registered agents.';

            return view('blog-listings',[
                'active' => 'blog',
                'articles' => $articles,
                'properties' => $this->latestPropertiesSidebar(),
                'pageTitle' => $pageTitle,
                'pageImage' => $pageImage,
                'metaDescription' => $metaDescription
            ]);
        }
        else{
            $article = Blog::with([
                'category:id,name',
                'branch.agency'
            ])
                ->select('author','author_link','title','slug','image','content','meta_description','category_id','branch_id')
                ->where('slug',$article)
                ->where('published',1)
                ->firstOrFail();

            return view('blog-article',[
                'active' => 'blog',
                'properties' => $this->latestPropertiesSidebar(),
                'type' => '',
                'article' => $article,
                'blogs' => $this->latestArticles($article->id),
                'pageTitle' => $article->title,
                'pageImage' => URL::asset('/assets/images/blog/'.$article->image),
                'metaDescription' => $article->meta
            ]);
        }
    }

    /**
     * Submit feedback to PB
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function submitFeedback(Request $request){
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'feedback' => 'required'
        ]);
        $feedback = new Feedback();
        $name = $request['name'];
        if(Auth::check()){
            $name .= $name.' ('.Auth::user()->agentDetail->agency->name.')';
        }
        $feedback->name = $name;
        $feedback->email = $request['email'];
        $feedback->feedback = $request['feedback'];
        $feedback->save();
        $admins = User::whereIn('id',[1,4])->get();
        foreach ($admins as $admin){
            $admin->notify(new FeedbackNotification($feedback->id,$admin->name));
        }
        return response()->json('Success',200);
    }

    /**
     * Display latest magazine if $mag param is null else display that particular magazine
     * @param null $mag
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getMagazines($mag = null){
        if(!$mag){
            $mag = Magazine::orderBy('created_at','desc')->firstOrFail();

        }
        else{
            $mag = Magazine::where('slug', $mag)->firstOrFail();
        }

        $mag->views = $mag->views + 1;
        $mag->save();

        $magazines = Magazine::where('id','<>',$mag->id)
            ->orderBy('created_at','desc')
            ->get();
        $pageImage = URL::asset('/assets/magazines/'.$mag->thumbnail);
        $metaDescription = 'With Propertybook Ã¢ÂÂThe MagazineÃ¢ÂÂ we thought we would bridge the gap that we hadnÃ¢ÂÂt filled yet. Now covering web, social, digital & print we can be sure to be in-front of our following anywhere they prefer.';
        return view('magazine',[
            'mag' => $mag,
            'blogs' => $this->latestArticles(),
            'properties' => $this->latestPropertiesSidebar(),
            'active' => 'magazine',
            'magazines' => $magazines,
            'pageTitle' => $mag->title.' - Propertybook The Magazine',
            'pageImage' => $pageImage,
            'metaDescription' => $metaDescription
        ]);
    }

    /**
     * Display commericial properties based of sales/rentals filter
     * @param Request $request
     * @param $filter
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Throwable
     */
    public function getCommercial(Request $request,$filter){
        if($filter == 'for-sale'){
            $label = 'for Sale';
            $title = 'Sales';
        }
        elseif($filter = 'to-rent'){
            $label = 'to Rent';
            $title = 'Rentals';
        }

        $queries = $request->query();

        if(isset($queries['sortBy'])){
            if($queries['sortBy'] == 'most-recent'){
                $col = 'created_at';
                $dir = 'desc';
            }
            elseif($queries['sortBy'] == 'price-high'){
                if(Session::get('selected_currency') == 'usd'){
                    $col = 'usd_price';
                }
                else{
                    $col = 'zwl_price';
                }
                $dir = 'desc';
            }
            elseif($queries['sortBy'] == 'price-low'){
                if(Session::get('selected_currency') == 'usd'){
                    $col = 'usd_price';
                }
                else{
                    $col = 'zwl_price';
                }
                $dir = 'asc';
            }
            else{
                $col = 'created_at';
                $dir = 'desc';
            }
            $sortBy = 'sortBy='.$queries['sortBy'];
        }
        else{
            $col = 'created_at';
            $dir = 'desc';
            $sortBy = 'sortBy=default';
        }

        if(isset($queries['page']) && $queries['page'] > 1){
            $skip = ($queries['page'] - 1) * 3;
        }
        else{
            $skip = 0;
        }

        $featured = $featured = FeaturedListing::with('property')
            ->where('site_id',siteID())
            ->where('start_date','<=',date('Y-m-d'))
            ->where('expiry_date','>=',date('Y-m-d'))
            ->where('pb_approval',1)
            ->whereHas('property',function ($query) use ($label){
                $query->whereHas('statuses',function ($query){
                    $query->where('status_id','>',1);
                    $query->where('status_id','!=',3);
                });
                $query->whereHas('attributes',function ($query){
                    $query->where('commercial',1);
                    $query->orWhere('industrial',1);
                    $query->orWhere('hotel_lodge',1);
                    $query->orWhere('offices',1);
                    $query->orWhere('warehouse_factory',1);
                    $query->orWhere('shop_retail',1);
                });
                $query->whereHas('offer',function ($query) use ($label){
                    $query->where('label',$label);
                });
                $query->where('active',1);
            })
            ->orderBy(DB::raw('RAND()'))
            ->take(3)
            ->get();

        $properties = Property::with([
            'offer:id,label,suffix',
            'suburb' => function ($query) {
                $query->select('id', 'name', 'slug', 'area_id');
                $query->with([
                    'area' => function ($query) {
                        $query->select('id', 'name', 'slug', 'town_id');
                        $query->with([
                            'town' => function ($query) {
                                $query->select('id', 'name', 'slug', 'province_id');
                                $query->with([
                                    'province' => function ($query) {
                                        $query->select('id', 'slug');
                                    }
                                ]);
                            }
                        ]);
                    }
                ]);
            },
            'type:id,type',
            'agents' => function ($query) {
                $query->select('id', 'property_id', 'agent_1');
                $query->with([
                    'agentDetail' => function ($query) {
                        $query->select('user_id', 'agency_id');
                        $query->with([
                            'agency' => function ($query) {
                                $query->select('id','name','slug','logo','hex_code','text_color','overlay');
                                $query->with([
                                    'settings' => function ($query) {
                                        $query->select('agency_id', 'custom_rate', 'rate');
                                    }
                                ]);
                            }
                        ]);
                    }
                ]);
            },
            'features:property_id,bedrooms,lounges,bathrooms,boreholes',
            'images:property_id,main_image'
        ])
            ->select('id', 'suburb_id', 'type_id', 'offer_id', 'ref_code', 'description', 'created_at', 'zwl_price','usd_price')
            ->whereHas('attributes',function ($query){
                $query->where('commercial',1);
                $query->orWhere('industrial',1);
                $query->orWhere('hotel_lodge',1);
                $query->orWhere('offices',1);
                $query->orWhere('warehouse_factory',1);
                $query->orWhere('shop_retail',1);
            })
            ->whereHas('offer',function ($query) use ($label){
                $query->select('id');
                $query->where('label',$label);
            })
            ->whereHas('statuses',function ($query){
                $query->where('status_id','>',1);
            })
            ->whereHas('sites',function ($query){
                $query->where('site_id',siteID());
            })
            ->where('active',1)
            ->orderBy($col,$dir)
            ->paginate(15);

        if(preg_match('(default|most-recent)',$sortBy) === 1){
            $sort_props = $properties->sortByDesc(function ($item){
                return $item->priority.'#'.$item->created_at;
            });
            $properties = new LengthAwarePaginator($sort_props,$properties->total(),$properties->perPage());
            $properties->setPath(URL::current());
        }
        if($properties->currentPage() > $properties->lastPage()){
            abort(404,'Not Found');
        }
        if($request->ajax()){
            $view = view('listing', [
                'properties' => $properties,
                'sortValue' => $sortBy,
            ])
                ->render();
            return [
                'html' => $view,
                'lower_bound' => ($properties->count() > 0 ? ($properties->currentPage() - 1) * $properties->perPage() + 1 : 0),
                'upper_bound' => ($properties->count() < $properties->perPage() ? $properties->total() : ($properties->currentPage() - 1) * $properties->perPage() + $properties->perPage())
            ];
        }
        else{
            $pageImage = URL::asset('/assets/img/generic-site-image.jpg');
            $pageTitle = 'Commercial Properties '.$label;
            return view('listings',[
                'properties' => $properties,
                'active' => 'commercial',
                'type' => $label,
                'listingType' => 'Commercial',
                'sortValue' => $sortBy,
                'featured' => $featured,
                'pageTitle' => $pageTitle,
                'pageImage' => $pageImage,
                'metaDescription' => 'Find your commercial properties '.$label,
                'area' => '',
                'breadcrumb' => '<a href="#" class="breadcrumb-item last">'.$pageTitle.'</a>',
                'searchQuery' => [],
            ]);
        }

    }

    /**
     * Display a single property where the area and suburb have different names
     * @param $filter
     * @param $province
     * @param $town
     * @param $area
     * @param $suburb
     * @param $refCode
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getPropertyWithArea(Request $request,$filter,$province,$town,$area,$suburb,$refCode){
        if($filter == 'for-sale'){
            $type = 'for Sale';
        }
        elseif($filter == 'to-rent'){
            $type = 'to Rent';
        }

        $queries = $request->query();

        $property = Cache::rememberForever('view_property_'.$refCode,function () use ($suburb,$refCode,$type){
            return Property::with(
                [
                    'agents' => function($query){
                        $query->select('property_id','agent_1','agent_2','agent_3','agent_4','agent_5','agent_6');
                        $query->with([
                            'agentDetail' => function($query){
                                $query->select('user_id','agency_id');
                                $query->with([
                                    'agency' => function($query){
                                        $query->select('id','name','slug','logo');
                                    }
                                ]);
                            }
                        ]);
                    },
                    'features',
                    'images:property_id,main_image,youtube_link,facebook_link,image_1,image_2,image_3,image_4,image_5,image_6,image_7,image_8,image_9,image_10,image_11,image_12,image_13,image_14,image_15,image_16,image_17,image_18,image_19',
                    'offer:id,label,suffix',
                    'suburb.area.town.province','type'
                ]
            )
                ->where('ref_code',$refCode)
                ->whereHas('statuses',function ($query){
                    $query->where('status_id','>',1);
                })
                ->whereHas('offer',function ($query) use ($type){
                    $query->select('id');
                    $query->where('label',$type);
                })
                ->whereHas('suburb',function($query) use ($suburb){
                    $query->select('id');
                    $query->where('slug',$suburb);
                })
                ->whereHas('sites',function ($query) {
                    $query->where('site_id',siteID());
                })
                ->where('active',1)
                ->firstOrFail();
        });

        if($property->suburb->area->slug == $property->suburb->area->town->slug){
            return redirect()->route('propertyWithoutArea',['filter' => $filter,'province' => $property->suburb->area->town->province->slug,'town' => $property->suburb->area->town->slug,'suburb' =>  $property->suburb->slug,'refCode' => $refCode]);
        }

        if(Auth::check()){
            $pLink = route('editProperty',['propertyID' => $property->id]);
        }
        else{
            $pLink = '#';

        }

        $breadcrumb = '
        <a href="'.route('all-properties',['filter' => $filter]).'" class="breadcrumb-item">Property '.$type.'</a>
        <a href="'.route('filterProperties',['filter' => $filter,'province' => $property->suburb->area->town->province->slug]).'" class="breadcrumb-item">'.$property->suburb->area->town->province->name.'</a>
        <a href="'.route('filterProperties',['filter' => $filter,'province' => $property->suburb->area->town->province->slug,'town' => $property->suburb->area->town->slug]).'" class="breadcrumb-item">'.$property->suburb->area->town->name.'</a>
        <a href="'.route('filterProperties',['filter' => $filter,'province' => $property->suburb->area->town->province->slug,'town' => $property->suburb->area->town->slug,'area' => $property->suburb->area->slug]).'" class="breadcrumb-item">'.$property->suburb->area->name.'</a>
        <a href="'.route('filterProperties',['filter' => $filter,'province' => $property->suburb->area->town->province->slug,'town' => $property->suburb->area->town->slug,'area' => $property->suburb->area->slug,'suburb' => $property->suburb->slug]).'" class="breadcrumb-item">'.$property->suburb->name.'</a>
        <a href="'.$pLink.'" class="breadcrumb-item last">'.strtoupper($refCode).'</a>
        ';
        $back = route('filterProperties',['filter' => $filter,'province' => $property->suburb->area->town->province->slug,'town' => $property->suburb->area->town->slug,'area' => $property->suburb->area->slug,'suburb' => $property->suburb->slug]);


        $pageTitle = self::generatePropertyTitle($property->type_id,$property->type->type,$property->features->bedrooms,$property->offer->label,$property->suburb->name).', '.$property->suburb->area->town->name;
        $metaDescription = $property->title;
        $features = Cache::tags('features')->rememberForever('property_features',function (){
            return Feature::orderBy('name')->get();
        });

        return view('singleListing',[
            'active' => $filter,
            'breadcrumb' => $breadcrumb,
            'property' => $property,
            'type' => $type,
            'back' => $back,
            'pageTitle' => $pageTitle,
            'metaDescription' => $metaDescription,
            'features' => $features
        ]);
    }

    /**
     * Display a single property where the area and the suburb have the same name
     * @param $filter
     * @param $province
     * @param $town
     * @param $suburb
     * @param $refCode
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getPropertyWithoutArea(Request $request,$filter,$province,$town,$suburb,$refCode){
        if($filter == 'for-sale'){
            $type = 'for Sale';
        }
        elseif($filter == 'to-rent'){
            $type = 'to Rent';
        }

        $queries = $request->query();


        $property = Cache::rememberForever('view_property_'.$refCode,function () use ($suburb,$refCode,$type){
            return Property::with(
                [
                    'agents' => function($query){
                        $query->select('property_id','agent_1','agent_2','agent_3','agent_4','agent_5','agent_6');
                        $query->with([
                            'agentDetail' => function($query){
                                $query->select('user_id','agency_id');
                                $query->with([
                                    'agency' => function($query){
                                        $query->select('id','name','slug','logo');
                                    }
                                ]);
                            }
                        ]);
                    },
                    'features',
                    'images:property_id,main_image,youtube_link,facebook_link,image_1,image_2,image_3,image_4,image_5,image_6,image_7,image_8,image_9,image_10,image_11,image_12,image_13,image_14,image_15,image_16,image_17,image_18,image_19',
                    'offer:id,label,suffix',
                    'suburb.area.town.province','type'
                ]
            )
                ->where('ref_code',$refCode)
                ->whereHas('statuses',function ($query){
                    $query->where('status_id','>',1);
                })
                ->whereHas('offer',function ($query) use ($type){
                    $query->select('id');
                    $query->where('label',$type);
                })
                ->whereHas('suburb',function($query) use ($suburb){
                    $query->select('id');
                    $query->where('slug',$suburb);
                })
                ->whereHas('sites',function ($query) {
                    $query->where('site_id',siteID());
                })
                ->where('active',1)
                ->firstOrFail();
        });

       /* if(isset($queries['map']) && $queries['map'] == 'show'){
            Mapper::map($property->latitude, $property->longitude,[
                'zoom' => 15,
                'draggable' => true,
                'eventDragEnd' => '$("#latitude").val(event.latLng.lat());$("#longitude").val(event.latLng.lng())',
                'markers' => [
                    'title' => 'Property Location',
                    'animation' => 'DROP'
                ]
            ]);
            $map = true;
        }*/
        if($property->suburb->area->slug !== $property->suburb->area->town->slug){
            return redirect()->route('propertyWithArea',['filter' => $filter,'province' => $property->suburb->area->town->province->slug,'town' => $property->suburb->area->town->slug,'area' => $property->suburb->area->slug,'suburb' =>  $property->suburb->slug,'refCode' => $refCode]);
        }

        if(Auth::check()){
            $pLink = route('editProperty',['propertyID' => $property->id]);
        }
        else{
            $pLink = '#';
        }

        $breadcrumb = '
        <a href="'.route('all-properties',['filter' => $filter]).'" class="breadcrumb-item">Property '.$type.'</a>
        <a href="'.route('filterProperties',['filter' => $filter,'province' => $property->suburb->area->town->province->slug]).'" class="breadcrumb-item">'.$property->suburb->area->town->province->name.'</a>
        <a href="'.route('filterProperties',['filter' => $filter,'province' => $property->suburb->area->town->province->slug,'town' => $property->suburb->area->town->slug]).'" class="breadcrumb-item">'.$property->suburb->area->town->name.'</a>
        <a href="'.route('filterProperties',['filter' => $filter,'province' => $property->suburb->area->town->province->slug,'town' => $property->suburb->area->town->slug,'area' => $property->suburb->area->slug,'suburb' => $property->suburb->slug]).'" class="breadcrumb-item">'.$property->suburb->name.'</a>
        <a href="'.$pLink.'" class="breadcrumb-item last">'.strtoupper($refCode).'</a>
        ';
        $back = route('filterProperties',['filter' => $filter,'province' => $property->suburb->area->town->province->slug,'town' => $property->suburb->area->town->slug,'area' => $property->suburb->area->slug,'suburb' => $property->suburb->slug]);


        $pageTitle = self::generatePropertyTitle($property->type_id,$property->type->type,$property->features->bedrooms,$property->offer->label,$property->suburb->name).', '.$property->suburb->area->town->name;
        $metaDescription = $property->title;

        $features = Cache::tags('features')->rememberForever('property_features',function (){
            return Feature::orderBy('name')->get();
        });

        return view('singleListing',[
            'active' => $filter,
            'breadcrumb' => $breadcrumb,
            'property' => $property,
            'type' => $type,
            'back' => $back,
            'pageTitle' => $pageTitle,
            'pageImage' => resolvePropertyImage('social',$property->images->main_image,$property,'standard'),
            'metaDescription' => $metaDescription,
            'features' => $features
        ]);
    }

    /**
     * Send an enquiry email for a property
     * @param Request $request
     * @param $propertyID
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendEnquiryEmail(Request $request,$propertyID){

        $request->validate([
            'full_name' => 'required',
            'email' => 'required|email',
            'phoneNumber' => 'required|numeric',
            'message' => 'required'
        ],[
            'name.required' => 'Please enter your name',
            'email.required' => 'Please enter your email',
            'email.email' => 'Please enter a valid email',
            'phoneNumber.required' => 'Please enter your phone number',
            'phoneNumber.numeric' => 'Only numeric values allowed',
            'message.required' => 'Please enter a message'
        ]);

        $property = Property::with('agents')->where('id',$propertyID)->first();
        $enquiry = new PropertyEnquiry();
        $enquiry->property_id = $propertyID;
        $enquiry->agent_1 = $property->agents->agent_1;
        $enquiry->name = $request['full_name'];
        $enquiry->email = $request['email'];
        $enquiry->phone_number = $request['phoneNumber'];
        $enquiry->message = $request['message'];
        $enquiry->offer_id = $property->offer_id;
        $enquiry->ref_code = $property->ref_code;
        $enquiry->type_id = $property->type_id;
        $enquiry->suburb_id = $property->suburb_id;
        $enquiry->price = $property->usd_price;
        $enquiry->ip_address = $request->getClientIp();
        $enquiry->site_id = siteID();
        $enquiry->agent_2 = $property->agents->agent_2;
        $enquiry->agent_3 = $property->agents->agent_3;
        $enquiry->agent_4 = $property->agents->agent_4;
        $enquiry->agent_5 = $property->agents->agent_5;
        $enquiry->agent_6 = $property->agents->agent_6;
        $enquiry->save();

        /*foreach ($property->agents->toArray() as $key => $agent){
            if(!in_array($key,['id','property_id','created_at','updated_at','user','agent_detail'])  && $agent){
                $user = User::where('id',$agent)->first();
                if(!empty($user)){
                    $user->notify(new EnquiryNotification($user,$enquiry));
                }
            }
        }

        if($property->agents->agentDetail->agency->carbonContacts->count() > 0){
            foreach ($property->agents->agentDetail->agency->carbonContacts as $notification){
                if($notification->phone_number){
                    if(!$notification->branch_id){
                        Notification::route('infobip',$notification->phone_number)
                            ->notify(new CarbonTextNotification($enquiry));
                    }
                    else{
                        if($notification->branch_id == $enquiry->agent->agentDetail->branch_id){
                            Notification::route('infobip',$notification->phone_number)
                                ->notify(new CarbonTextNotification($enquiry));
                        }
                    }

                }
            }

        }*/

        logPropertyStat('enquiries',$property);

        return response()->json('Enquiry sent',200);
    }

    /**
     * Get properties to display in sliders at the bottom of the singe property page
     * @param $propertyID
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPropertySliders($propertyID){
        $property = Property::with([
            'offer:id,label'
        ])
            ->select('id','zwl_price','usd_price','type_id','longitude','latitude','offer_id','type_id')
            ->where('id',$propertyID)
            ->firstOrFail();

        //Similar Properties
        $similar = Property::with([
            'offer:id,label,suffix',
            'suburb' => function ($query) {
                $query->select('id', 'area_id', 'slug', 'name');
                $query->with(['area' => function ($query) {
                    $query->select('id', 'town_id', 'slug', 'name');
                    $query->with(['town' => function ($query) {
                        $query->select('id', 'province_id', 'slug');
                        $query->with(['province' => function ($query) {
                            $query->select('id', 'slug');
                        }]);
                    }]);
                }]);
            },
            'type:id,type',
            'images:id,property_id,main_image',
            'features:id,property_id,bedrooms,bathrooms,boreholes,lounges',
        ])
            ->selectRaw('id,suburb_id,usd_price,zwl_price,ref_code,offer_id,type_id, ( 6367 * acos( cos( radians( ? ) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians( ? ) ) + sin( radians( ? ) ) * sin( radians( latitude ) ) ) ) AS distance', [$property->latitude, $property->longitude, $property->latitude])
            ->where('type_id',$property->type_id)
            ->whereHas('offer',function($query) use ($property){
                $query->where('label',$property->offer->label);
            })
            ->whereHas('statuses',function ($query){
                $query->whereIn('status_id',[2,4,5,8]);
            })
            ->whereHas('sites',function ($query){
                $query->where('site_id',siteID());
            })
            ->whereBetween('usd_price',[$property->price/1.25,$property->price*1.20])
            ->where('id','!=',$property->id)
            ->having('distance', '<=', 10)
            ->orderBy('distance')
            ->take(15)
            ->get();
        if(count($similar) < 3){ //First Fallback
            $similar = Property::with([
                'offer:id,label,suffix',
                'suburb' => function ($query) {
                    $query->select('id', 'area_id', 'slug', 'name');
                    $query->with(['area' => function ($query) {
                        $query->select('id', 'town_id', 'slug', 'name');
                        $query->with(['town' => function ($query) {
                            $query->select('id', 'province_id', 'slug');
                            $query->with(['province' => function ($query) {
                                $query->select('id', 'slug');
                            }]);
                        }]);
                    }]);
                },
                'type:id,type',
                'images:id,property_id,main_image',
                'features:id,property_id,bedrooms,bathrooms,boreholes,lounges',
            ])
                ->selectRaw('id,suburb_id,usd_price,zwl_price,ref_code,offer_id,type_id, ( 6367 * acos( cos( radians( ? ) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians( ? ) ) + sin( radians( ? ) ) * sin( radians( latitude ) ) ) ) AS distance', [$property->latitude, $property->longitude, $property->latitude])
                ->where('type_id',$property->type_id)
                ->whereHas('offer',function($query) use ($property){
                    $query->where('label',$property->offer->label);
                })
                ->whereHas('statuses',function ($query){
                    $query->whereIn('status_id',[2,4,5,8]);
                })
                ->whereHas('sites',function ($query){
                    $query->where('site_id',siteID());
                })
                ->whereBetween('usd_price',[$property->price/1.25,$property->price*1.20])
                ->where('id','!=',$property->id)
                ->having('distance', '<=', 15)
                ->orderBy('distance')
                ->take(15)
                ->get();
            if(count($similar) < 3){//Second Fallback
                $similar = Property::with([
                    'offer:id,label,suffix',
                    'suburb' => function ($query) {
                        $query->select('id', 'area_id', 'slug', 'name');
                        $query->with(['area' => function ($query) {
                            $query->select('id', 'town_id', 'slug', 'name');
                            $query->with(['town' => function ($query) {
                                $query->select('id', 'province_id', 'slug');
                                $query->with(['province' => function ($query) {
                                    $query->select('id', 'slug');
                                }]);
                            }]);
                        }]);
                    },
                    'type:id,type',
                    'images:id,property_id,main_image',
                    'features:id,property_id,bedrooms,bathrooms,boreholes,lounges',
                ])
                    ->selectRaw('id,suburb_id,usd_price,zwl_price,ref_code,offer_id,type_id, ( 6367 * acos( cos( radians( ? ) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians( ? ) ) + sin( radians( ? ) ) * sin( radians( latitude ) ) ) ) AS distance', [$property->latitude, $property->longitude, $property->latitude])
                    ->whereHas('offer',function($query) use ($property){
                        $query->where('label',$property->offer->label);
                    })
                    ->where('type_id',$property->type_id)
                    ->whereHas('statuses',function ($query){
                        $query->whereIn('status_id',[2,4,5,8]);
                    })
                    ->whereHas('sites',function ($query){
                        $query->where('site_id',siteID());
                    })
                    ->whereBetween('usd_price',[$property->price/1.25,$property->price*1.20])
                    ->where('id','!=',$property->id)
                    ->having('distance', '<=', 15)
                    ->orderBy('distance')
                    ->take(15)
                    ->get();
                if(count($similar) < 3){//Third Fallback
                    $similar = Property::with([
                        'offer:id,label,suffix',
                        'suburb' => function ($query) {
                            $query->select('id', 'area_id', 'slug', 'name');
                            $query->with(['area' => function ($query) {
                                $query->select('id', 'town_id', 'slug', 'name');
                                $query->with(['town' => function ($query) {
                                    $query->select('id', 'province_id', 'slug');
                                    $query->with(['province' => function ($query) {
                                        $query->select('id', 'slug');
                                    }]);
                                }]);
                            }]);
                        },
                        'type:id,type',
                        'images:id,property_id,main_image',
                        'features:id,property_id,bedrooms,bathrooms,boreholes,lounges',
                    ])
                        ->selectRaw('id,suburb_id,usd_price,zwl_price,ref_code,offer_id,type_id, ( 6367 * acos( cos( radians( ? ) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians( ? ) ) + sin( radians( ? ) ) * sin( radians( latitude ) ) ) ) AS distance', [$property->latitude, $property->longitude, $property->latitude])
                        ->where('type_id',$property->type_id)
                        ->whereHas('offer',function($query) use ($property){
                            $query->where('label',$property->offer->label);
                        })
                        ->whereHas('statuses',function ($query){
                            $query->whereIn('status_id',[2,4,5,8]);
                        })
                        ->whereHas('sites',function ($query){
                            $query->where('site_id',siteID());
                        })
                        ->whereBetween('usd_price',[$property->price/1.25,$property->price*1.20])
                        ->where('id','<>',$property->id)
                        ->having('distance', '<=', 15)
                        ->orderBy('distance')
                        ->take(15)
                        ->get();
                    if(count($similar) < 3){//Fourth Fallback
                        $similar = Property::with([
                            'offer:id,label,suffix',
                            'suburb' => function ($query) {
                                $query->select('id', 'area_id', 'slug', 'name');
                                $query->with(['area' => function ($query) {
                                    $query->select('id', 'town_id', 'slug', 'name');
                                    $query->with(['town' => function ($query) {
                                        $query->select('id', 'province_id', 'slug');
                                        $query->with(['province' => function ($query) {
                                            $query->select('id', 'slug');
                                        }]);
                                    }]);
                                }]);
                            },
                            'type:id,type',
                            'images:id,property_id,main_image',
                            'features:id,property_id,bedrooms,bathrooms,boreholes,lounges',
                        ])
                            ->selectRaw('id,suburb_id,usd_price,zwl_price,ref_code,offer_id,type_id, ( 6367 * acos( cos( radians( ? ) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians( ? ) ) + sin( radians( ? ) ) * sin( radians( latitude ) ) ) ) AS distance', [$property->latitude, $property->longitude, $property->latitude])
                            ->where('type_id',$property->type_id)
                            ->whereHas('offer',function($query) use ($property){
                                $query->where('label',$property->offer->label);
                            })
                            ->whereHas('statuses',function ($query){
                                $query->whereIn('status_id',[2,4,5,8]);
                            })
                            ->whereHas('sites',function ($query){
                                $query->where('site_id',siteID());
                            })
                            ->where('id','<>',$property->id)
                            ->having('distance', '<=', 15)
                            ->orderBy('distance')
                            ->take(15)
                            ->get();
                        if(count($similar) < 3){//Fifth Fallback
                            $similar = Property::with([
                                'offer:id,label,suffix',
                                'suburb' => function ($query) {
                                    $query->select('id', 'area_id', 'slug', 'name');
                                    $query->with(['area' => function ($query) {
                                        $query->select('id', 'town_id', 'slug', 'name');
                                        $query->with(['town' => function ($query) {
                                            $query->select('id', 'province_id', 'slug');
                                            $query->with(['province' => function ($query) {
                                                $query->select('id', 'slug');
                                            }]);
                                        }]);
                                    }]);
                                },
                                'type:id,type',
                                'images:id,property_id,main_image',
                                'features:id,property_id,bedrooms,bathrooms,boreholes,lounges',
                            ])
                                ->selectRaw('id,suburb_id,usd_price,zwl_price,ref_code,offer_id,type_id, ( 6367 * acos( cos( radians( ? ) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians( ? ) ) + sin( radians( ? ) ) * sin( radians( latitude ) ) ) ) AS distance', [$property->latitude, $property->longitude, $property->latitude])
                                ->whereHas('offer',function($query) use ($property){
                                    $query->where('label',$property->offer->label);
                                })
                                ->whereHas('statuses',function ($query){
                                    $query->whereIn('status_id',[2,4,5,8]);
                                })
                                ->whereHas('sites',function ($query){
                                    $query->where('site_id',siteID());
                                })
                                ->whereBetween('usd_price',[$property->price/1.25,$property->price*1.20])
                                ->where('id','<>',$property->id)
                                ->orderBy('distance')
                                ->take(15)
                                ->get();
                        }
                    }
                }
            }
        }

        $similar = $similar->sortBy('distance')->sortByDesc('priority');

        //Other Properties
        $other = Property::with([
            'offer:id,label,suffix',
            'suburb' => function ($query) {
                $query->select('id', 'area_id', 'slug', 'name');
                $query->with(['area' => function ($query) {
                    $query->select('id', 'town_id', 'slug', 'name');
                    $query->with(['town' => function ($query) {
                        $query->select('id', 'province_id', 'slug');
                        $query->with(['province' => function ($query) {
                            $query->select('id', 'slug');
                        }]);
                    }]);
                }]);
            },
            'type:id,type',
            'images:id,property_id,main_image',
            'features:id,property_id,bedrooms,bathrooms,boreholes,lounges',
        ])
            ->selectRaw('id,suburb_id,usd_price,zwl_price,ref_code,offer_id,type_id, ( 6367 * acos( cos( radians( ? ) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians( ? ) ) + sin( radians( ? ) ) * sin( radians( latitude ) ) ) ) AS distance', [$property->latitude, $property->longitude, $property->latitude])
            ->where('type_id',$property->type_id)
            ->whereHas('offer',function($query) use ($property){
                $query->where('label',$property->offer->label);
            })
            ->whereHas('statuses',function ($query){
                $query->whereIn('status_id',[2,4,5,8]);
            })
            ->whereHas('sites',function ($query){
                $query->where('site_id',siteID());
            })
            ->whereNotIn('id',$similar)
            ->where('id','<>',$propertyID)
            ->having('distance', '<=', 15)
            ->orderBy(DB::raw("RAND()"))
            ->take(15)
            ->get();
        if(count($other) < 3){//First Fallback
            $other = Property::with([
                'offer:id,label,suffix',
                'suburb' => function ($query) {
                    $query->select('id', 'area_id', 'slug', 'name');
                    $query->with(['area' => function ($query) {
                        $query->select('id', 'town_id', 'slug', 'name');
                        $query->with(['town' => function ($query) {
                            $query->select('id', 'province_id', 'slug');
                            $query->with(['province' => function ($query) {
                                $query->select('id', 'slug');
                            }]);
                        }]);
                    }]);
                },
                'type:id,type',
                'images:id,property_id,main_image',
                'features:id,property_id,bedrooms,bathrooms,boreholes,lounges',
            ])
                ->selectRaw('id,suburb_id,usd_price,zwl_price,ref_code,offer_id,type_id, ( 6367 * acos( cos( radians( ? ) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians( ? ) ) + sin( radians( ? ) ) * sin( radians( latitude ) ) ) ) AS distance', [$property->latitude, $property->longitude, $property->latitude])
                ->whereHas('offer',function($query) use ($property){
                    $query->where('label',$property->offer->label);
                })
                ->whereHas('statuses',function ($query){
                    $query->whereIn('status_id',[2,4,5,8]);
                })
                ->whereHas('sites',function ($query){
                    $query->where('site_id',siteID());
                })
                ->where('id','<>',$propertyID)
                ->having('distance', '<=', 15)
                ->orderBy(DB::raw("RAND()"))
                ->take(15)
                ->get();
            if(count($other) < 3){//Second Fallback
                $other = Property::with([
                    'offer:id,label,suffix',
                    'suburb' => function ($query) {
                        $query->select('id', 'area_id', 'slug', 'name');
                        $query->with(['area' => function ($query) {
                            $query->select('id', 'town_id', 'slug', 'name');
                            $query->with(['town' => function ($query) {
                                $query->select('id', 'province_id', 'slug');
                                $query->with(['province' => function ($query) {
                                    $query->select('id', 'slug');
                                }]);
                            }]);
                        }]);
                    },
                    'type:id,type',
                    'images:id,property_id,main_image',
                    'features:id,property_id,bedrooms,bathrooms,boreholes,lounges',
                ])
                    ->selectRaw('id,suburb_id,usd_price,zwl_price,ref_code,offer_id,type_id, ( 6367 * acos( cos( radians( ? ) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians( ? ) ) + sin( radians( ? ) ) * sin( radians( latitude ) ) ) ) AS distance', [$property->latitude, $property->longitude, $property->latitude])
                    ->whereHas('offer',function($query) use ($property){
                        $query->where('label',$property->offer->label);
                    })
                    ->whereHas('statuses',function ($query){
                        $query->whereIn('status_id',[2,4,5,8]);
                    })
                    ->whereHas('sites',function ($query){
                        $query->where('site_id',siteID());
                    })
                    ->where('id','<>',$propertyID)
                    ->orderBy(DB::raw("RAND()"))
                    ->take(15)
                    ->get();
            }
        }

        $other = $other->sortBy('distance')->sortByDesc('priority');

        if($property->agents->agentDetail->first()) {
            //Other by Agency
            $otherAgency = Property::with([
                'offer:id,label,suffix',
                'suburb' => function ($query) {
                    $query->select('id', 'area_id', 'slug', 'name');
                    $query->with(['area' => function ($query) {
                        $query->select('id', 'town_id', 'slug', 'name');
                        $query->with(['town' => function ($query) {
                            $query->select('id', 'province_id', 'slug');
                            $query->with(['province' => function ($query) {
                                $query->select('id', 'slug');
                            }]);
                        }]);
                    }]);
                },
                'type:id,type',
                'images:id,property_id,main_image',
                'features:id,property_id,bedrooms,bathrooms,boreholes,lounges',
            ])
                ->selectRaw('id,suburb_id,usd_price,zwl_price,ref_code,offer_id,type_id')
                ->whereHas('offer', function ($query) use ($property) {
                    $query->where('label', $property->offer->label);
                })
                ->whereHas('agents', function ($query) use ($property) {
                    $query->select('property_id', 'agent_1');
                    $query->whereHas('agentDetail', function ($query) use ($property) {
                        $query->select('user_id', 'agency_id');
                        $query->where('agency_id', $property->agents->agentDetail->agency_id);
                    });
                })
                ->whereHas('statuses',function ($query){
                    $query->whereIn('status_id',[2,4,5,8]);
                })
                ->whereHas('sites',function ($query){
                    $query->where('site_id',siteID());
                })
                ->where('id', '<>', $propertyID)
                ->orderBy(DB::raw("RAND()"))
                ->take(15)
                ->get();
        }
        else{
            $otherAgency = [];
        }


        $similarData = '';
        $simIDs = [];
        foreach ($similar as $prop){
            $simIDs[] = $prop->id;
            $similarData .= '<li id="similar-property-'.$prop->id.'">
                                <a
                                        href="'.propertyLink($prop).'"
                                        onclick="gtag(\'event\',\'Similar Property Click\',{
                                                \'event_category\': \'Similar Properties\',
                                                \'event_label\': \'Ref Code: '.$prop->ref_code.'\',
                                                \'value\': \'\'
                                                });"
                                >
                                    <div class="latestProperty">
                                        <div class="listingImage">
                                            <picture>
                                                <source srcset="'.resolvePropertyImage('mobile',$prop->images->main_image,$prop,'webp').'" type="image/webp">
                                                <img src="'.resolvePropertyImage('mobile',$prop->images->main_image,$prop,'standard').'"
                                                     class="img-fluid"
                                                     alt="'.$prop->type->type.' in '.$prop->suburb->name.'"
                                                >
                                            </picture>
                                            '.self::propertyStatusRibbon($prop->status_id,Str::slug($prop->offer->label),$prop->created_at).'
                                        </div>
                                        <div class="details">
                                            <div class="price">'.self::checkPrice($prop->price,$prop->price_zwl,$prop->offer->suffix,$prop->offer_id).'</div>
                                            <div class="description">'.$prop->type->type.' '.$prop->offer->label.' in '.$prop->suburb->name.'</div>
                                            <div class="icons">
                                                <ul class="list-inline">
                                                '.($prop->features->bedrooms > 0 ? '<li class="list-inline-item"><i class="fa fa-bed"></i> '.$prop->features->bedrooms.' Bed </li>' : '').'
                                                '.($prop->features->bathrooms > 0 ? '<li class="list-inline-item"><i class="fa fa-bath"></i> '.$prop->features->bathrooms.' Bath </li>' : '').'
                                                '.($prop->features->lounges > 0 ? '<li class="list-inline-item"><i class="fa fa-couch"></i> '.$prop->features->lounges.' Lounge </li>' : '').'
                                                '.($prop->features->boreholes > 0 ? '<li class="list-inline-item"><i class="fa fa-tint"></i> '.$prop->features->boreholes.' Borehole </li>' : '').'
                                             </ul>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>';
            logPropertyStat('impressions',$prop);
        }

        $otherData = '';
        $otherIDs = [];
        foreach ($other as $prop){
            $otherIDs[] = $prop->id;
            $otherData .= '<li id="other-property-'.$prop->id.'">
                                <a
                                        href="'.propertyLink($prop).'"
                                        onclick="gtag(\'event\',\'Properties Nearby Click\',{
                                                \'event_category\': \'Properties Nearby\',
                                                \'event_label\': \'Ref Code: '.$prop->ref_code.'\',
                                                \'value\': \'\'
                                                });"
                                >
                                    <div class="latestProperty">
                                        <div class="listingImage">
                                            <picture>
                                                <source srcset="'.resolvePropertyImage('mobile',$prop->images->main_image,$prop,'webp').'" type="image/webp">
                                                <img src="'.resolvePropertyImage('mobile',$prop->images->main_image,$prop,'standard').'"
                                                     class="img-fluid"
                                                     alt="'.$prop->type->type.' in '.$prop->suburb->name.'"
                                                >
                                            </picture>
                                            '.propertyStatusRibbon($prop->status_id,Str::slug($prop->offer->label),$prop->created_at).'
                                        </div>
                                        <div class="details">
                                            <div class="price">'.displayPrice($prop).'</div>
                                            <div class="description">'.$prop->type->type.' '.$prop->offer->label.' in '.$prop->suburb->name.'</div>
                                            <div class="icons">
                                                <ul class="list-inline">
                                                '.($prop->features->bedrooms > 0 ? '<li class="list-inline-item"><i class="fa fa-bed"></i> '.$prop->features->bedrooms.' Bed </li>' : '').'
                                                '.($prop->features->bathrooms > 0 ? '<li class="list-inline-item"><i class="fa fa-bath"></i> '.$prop->features->bathrooms.' Bath </li>' : '').'
                                                '.($prop->features->lounges > 0 ? '<li class="list-inline-item"><i class="fa fa-couch"></i> '.$prop->features->lounges.' Lounge </li>' : '').'
                                                '.($prop->features->boreholes > 0 ? '<li class="list-inline-item"><i class="fa fa-tint"></i> '.$prop->features->boreholes.' Borehole </li>' : '').'
                                             </ul>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>';
        }

        $otherAgencyData = '';
        $other_IDs = [];
        if($property->agents->agentDetail->first()){
            foreach ($otherAgency as $prop){
                $other_IDs[] = $prop->id;
                $otherAgencyData .= '<li id="agency-property-'.$prop->id.'">
                                <a
                                        href="'.propertyLink($prop).'"
                                        onclick="gtag(\'event\',\'Other By Agency Click\',{
                                                \'event_category\': \'Other By Agency\',
                                                \'event_label\': \'Ref Code: '.$prop->ref_code.'\',
                                                \'value\': \'\'
                                                });"
                                >
                                    <div class="latestProperty">
                                        <div class="listingImage">
                                            <picture>
                                                <source srcset="'.resolvePropertyImage('mobile',$prop->images->main_image,$prop,'webp').'" type="image/webp">
                                                <img src="'.resolvePropertyImage('mobile',$prop->images->main_image,$prop,'standard').'"
                                                     class="img-fluid"
                                                     alt="'.$prop->type->type.' in '.$prop->suburb->name.'"
                                                >
                                            </picture>
                                            '.propertyStatusRibbon($prop->status_id,Str::slug($prop->offer->label),$prop->created_at).'
                                        </div>
                                        <div class="details">
                                            <div class="price">'.displayPrice($prop).'</div>
                                            <div class="description">'.$prop->type->type.' '.$prop->offer->label.' in '.$prop->suburb->name.'</div>
                                            <div class="icons">
                                                <ul class="list-inline">
                                                '.($prop->features->bedrooms > 0 ? '<li class="list-inline-item"><i class="fa fa-bed"></i> '.$prop->features->bedrooms.' Bed </li>' : '').'
                                                '.($prop->features->bathrooms > 0 ? '<li class="list-inline-item"><i class="fa fa-bath"></i> '.$prop->features->bathrooms.' Bath </li>' : '').'
                                                '.($prop->features->lounges > 0 ? '<li class="list-inline-item"><i class="fa fa-couch"></i> '.$prop->features->lounges.' Lounge </li>' : '').'
                                                '.($prop->features->boreholes > 0 ? '<li class="list-inline-item"><i class="fa fa-tint"></i> '.$prop->features->boreholes.' Borehole </li>' : '').'
                                             </ul>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>';

            }
        }

        $data = [
            'similar' => $similarData,
            'other' => $otherData,
            'otherAgency' => $otherAgencyData,
            'simIDs' => $simIDs,
            'otherIDs' => $otherIDs,
            'other_IDs' => $other_IDs
        ];

        return response()->json($data,200);
    }

    /**
     * Log a property enquiry
     * @param $type
     * @param $ref_code
     * @return \Illuminate\Http\JsonResponse
     */
    public function logPropertyEnquiry($type,$propertyID){
        $property = Property::where('id',$propertyID)->firstOrFail();
        logPropertyStat($type,$property);
        return response()->json('Statistics saved',200);
    }

    /**
     * Save a property search for alerts
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveSearch(Request $request){
        $request->validate([
            'email_address' => 'required|email'
        ],[
            'email_address.required' => 'Please enter your email address',
            'email_address.email' => 'Please enter a valid email'
        ]);

        $search = new SavedSearch();
        $search->email = $request['email_address'];
        $search->offer_type = $request['offerType'];
        $searchQuery = json_decode($request['searchQuery'],true);
        $search->types = (isset($searchQuery['parameters']['type']) ? explode('-',$searchQuery['parameters']['type']) : null);
        $search->min_price = (isset($searchQuery['parameters']['minPrice']) ? $searchQuery['parameters']['minPrice'] : null);
        $search->max_price = (isset($searchQuery['parameters']['maxPrice']) ? $searchQuery['parameters']['maxPrice'] : null);
        $search->bedrooms = (isset($searchQuery['parameters']['beds']) ? $searchQuery['parameters']['beds'] : null);
        $search->bathrooms = (isset($searchQuery['parameters']['baths']) ? $searchQuery['parameters']['baths'] : null);

        if(isset($searchQuery['provinces'])){
            $provinces = [];
            foreach ($searchQuery['provinces'] as $province){
                $provinces[] = str_replace('Province_','',$province['id']);
            }
            $search->provinces = $provinces;
        }

        if(isset($searchQuery['towns'])){
            $towns = [];
            foreach ($searchQuery['towns'] as $town){
                $towns[] = str_replace('Town_','',$town['id']);
            }
            $search->towns = $towns;
        }

        if(isset($searchQuery['areas'])){
            $areas = [];
            foreach ($searchQuery['areas'] as $area){
                $areas[] = str_replace('Area_','',$area['id']);
            }
            $search->areas = $areas;
        }

        if(isset($searchQuery['suburbs'])){
            $suburbs = [];
            foreach ($searchQuery['suburbs'] as $suburb){
                $suburbs[] = str_replace('Suburb_','',$suburb['id']);
            }
            $search->suburbs = $suburbs;
        }
        $search->usd_price = (Session::get('selected_currency') == 'usd' ? 1 : 0);

        $search->save();




        return response()->json('Search saved',200);
    }

    /**
     * Cancel property alert
     * @param $alertID
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function cancelAlert($alertID){
        try{
            $id = decrypt($alertID);

            $alert = SavedSearch::where('id',$id)->first();
            if(empty($alert)){
                abort(403,'Alert has already been cancelled');
            }
            $alert->delete();
            return view('generic-success',[
                'message' => 'Your alert has been successfully cancelled'
            ]);
        }
        catch (DecryptException $e){
            abort(403,'Invalid cancellation link provided.');
        }
    }

    /**
     * Switch currency displayed
     * @param $currency
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeCurrency(){
        $current = Session::get('selected_currency');
        Session::forget('selected_currency');
        if($current == 'usd'){
            $currency = 'zwl';
        }
        else{
            $currency = 'usd';
        }
        Session::put('selected_currency',$currency);
        return response()->json('Currency updated',200);
    }

    public function viewMap($refCode){
        $property = Property::where('ref_code',$refCode)
            ->where('status_id','>',1)
            ->firstOrFail();
        Mapper::map($property->latitude, $property->longitude,[
            'zoom' => 15,
            'draggable' => true,
            'eventDragEnd' => '$("#latitude").val(event.latLng.lat());$("#longitude").val(event.latLng.lng())',
            'markers' => [
                'title' => 'Property Location',
                'animation' => 'DROP'
            ]
        ]);
        return view('view-map',[
            'pageTitle' => self::generatePropertyTitle($property->type_id,$property->type->type,$property->features->bedrooms,$property->offer->label,$property->suburb->name),
            'active' => '',
            'pageImage' => URL::asset('/assets/images/map-holder.jpg'),
            'metaDescription' => 'Property map location',
            'property' => $property,
            'properties' => $this->latestPropertiesSidebar(),
        ]);
    }

    /*
     * Begin static functions
     */

    /**
     * Generate the full url of a property
     * @param $filter
     * @param $province
     * @param $town
     * @param $area
     * @param $suburb
     * @param $ref_code
     * @return string
     */
    public static function generatePropertyUrl($filter, $province, $town, $area, $suburb, $ref_code)
    {
        if ($town == $area) {
            return route('propertyWithoutArea', ['filter' => $filter, 'province' => $province, 'town' => $town, 'suburb' => $suburb, 'refCode' => strtolower($ref_code)]);
        } else {
            return route('propertyWithArea', ['filter' => $filter, 'province' => $province, 'town' => $town, 'area' => $area, 'suburb' => $suburb, 'refCode' => strtolower($ref_code)]);
        }
    }

    /**
     * Check if an image exists for a property and display a generic image if not
     * @param $folder
     * @param $filter
     * @param $type
     * @param $file
     * @param $format
     * @return string
     */
    public static function resolvePropertyImage($folder, $filter, $type, $file, $format)
    {
        if(!$file){
            if ($filter == 'for-sale') {
                $land = [11, 16];
                if (in_array($type, $land)) {
                    if ($format == 'standard') {
                        return $folder . '/land-for-sale.jpg';
                    } elseif ($format == 'webp') {
                        return $folder . '/land-for-sale.webp';
                    }
                } else {
                    if ($format == 'standard') {
                        return $folder . '/for-sale.jpg';
                    } elseif ($format == 'webp') {
                        return $folder . '/for-sale.webp';
                    }
                }
            }
            elseif ($filter == 'to-rent') {
                if ($format == 'standard') {
                    return $folder . '/to-rent.jpg';
                }
                elseif ($format == 'webp') {
                    return $folder . '/to-rent.webp';
                }
            }
        }
        else{
            $path = base_path('/../assets/images/properties/' . $folder . '/' . $file);
            if (file_exists($path)) {
                if ($format == 'standard') {
                    return $folder . '/' . $file;
                } elseif ($format == 'webp') {
                    $ext = new \SplFileInfo($path);
                    $ext = $ext->getExtension();
                    $webp = str_replace('.' . $ext, '.webp', $path);
                    if (file_exists($webp)) {
                        return $folder . '/' . str_replace('.' . $ext, '.webp', $file);
                    } else {
                        return $folder . '/' . $file;
                    }
                }
            }
            else {
                if ($filter == 'for-sale') {
                    $land = [11, 16];
                    if (in_array($type, $land)) {
                        if ($format == 'standard') {
                            return $folder . '/land-for-sale.jpg';
                        } elseif ($format == 'webp') {
                            return $folder . '/land-for-sale.webp';
                        }
                    } else {
                        if ($format == 'standard') {
                            return $folder . '/for-sale.jpg';
                        } elseif ($format == 'webp') {
                            return $folder . '/for-sale.webp';
                        }
                    }
                }
                elseif ($filter == 'to-rent') {
                    if ($format == 'standard') {
                        return $folder . '/to-rent.jpg';
                    }
                    elseif ($format == 'webp') {
                        return $folder . '/to-rent.webp';
                    }
                }
            }
        }

    }

    /**
     * Display a ribbon on a property based on it's status
     * @param $status
     * @param $filter
     * @param $created_at
     * @param string $property
     * @return string
     */
    public static function propertyStatusRibbon($status, $filter, $created_at, $property = '')
    {
        if ($status == 2) {
            if (strtotime($created_at . ' +2 weeks') >= strtotime('now')) {
                return '<div class="ribbon new' . $property . '">new</div>';
            }
        } elseif ($status == 3) {
            if ($filter == 'for-sale') {
                return '<div class="ribbon taken' . $property . '">sold</div>';
            } elseif ($filter == 'to-rent') {
                return '<div class="ribbon taken' . $property . '">rented</div>';
            }
        } elseif ($status == 4) {
            return '<div class="ribbon new' . $property . '">reduced price</div>';
        } elseif ($status == 5) {
            return '<div class="ribbon new' . $property . '">on show</div>';
        } elseif ($status == 6) {
            return '<div class="ribbon holding' . $property . '">on hold</div>';
        } elseif ($status == 7) {
            return '<div class="ribbon holding' . $property . '">offer pending</div>';
        } elseif ($status == 8) {
            return '<div class="ribbon sole' . $property . '">sole mandate</div>';
        }
    }

    /**
     * Structure the price for a property
     * @param $price_usd
     * @param $price_zwl
     * @param $suffix
     * @param $offer
     * @param null $console
     * @return string
     */
    public static function checkPrice($price_usd,$price_zwl, $suffix, $offer,$console = null)
    {
        if ($offer == 5 || $offer == 6 || $price_usd == 0) {
            return 'POA';
        } else {
            if($console == null){
                if(Session::get('selected_currency') == 'usd'){
                    $price = 'USD '.number_format($price_usd);
                }
                else{
                    $price = 'ZWL '.number_format($price_zwl);
                }
            }
            else{
                $price = 'USD '.number_format($price_usd);
            }

            return $price.$suffix;
        }
    }

    public function recordStats(Request $request){
        $property = Property::where('ref_code',$request['refCode'])->firstOrFail();
        if($request['type'] == 'home-page-featured' || $request['type'] == 'listing-featured'){
            saveFeaturedView($request['propertyID']);
            logPropertyStat('impressions',$property);
        }
        elseif($request['type'] == 'priority-listing'){
            logPropertyStat('impressions',$property);
        }
        elseif($request['type'] == 'standard-listing' || $request['type'] == 'single-listing-slider'){
            logPropertyStat('impressions',$property);
        }
    }

    /**
     * Log a stat for a property
     * @param $type
     * @param Property $property
     */
    public static function logPropertyStat($type, Property $property)
    {
        dispatch((new LogPropertyStats($type, $property))->onQueue('low-priority'));
    }

    /**
     * Get the webp version of an image
     * @param $type
     * @param $image
     * @return mixed|string
     */
    public static function getWebPImage($type, $image)
    {
        if ($type == 'agencyLogo') {
            $link = URL::asset('/assets/images/agency-logos/' . $image);
            $path = base_path('/../assets/images/agency-logos/' . $image);
            $ext = new \SplFileInfo($path);
        } elseif ($type == 'magazine') {
            $link = URL::asset('/assets/images/magazines/' . $image);
            $path = base_path('/../assets/images/magazines/' . $image);
            $ext = new \SplFileInfo($path);
        } elseif ($type == 'blog-main') {
            $link = URL::asset('/assets/images/blog/' . $image);
            $path = base_path('/../assets/images/blog/' . $image);
            $ext = new \SplFileInfo($path);
        } elseif ($type == 'blog-thumb') {
            $link = URL::asset('/assets/images/blog/thumbnail/' . $image);
            $path = base_path('/../assets/images/blog/thumbnail/' . $image);
            $ext = new \SplFileInfo($path);
        } elseif ($type == 'general-page') {
            $link = URL::asset('/assets/images/pages/' . $image);
            $path = base_path('/../assets/images/pages/' . $image);
            $ext = new \SplFileInfo($path);
        }
        $ext = $ext->getExtension();
        if (file_exists(str_replace($ext, 'webp', $path))) {
            return str_replace($ext, 'webp', $link);
        } else {
            return $link;
        }

    }

    /**
     * Generate a title for a property
     * @param $type_id
     * @param $type
     * @param $bedrooms
     * @param $label
     * @param $suburb
     * @return string
     */
    public static function generatePropertyTitle($type_id,$type,$bedrooms,$label,$suburb){
        $residential = [1,4,5,8,9,17];
        return (in_array($type_id,$residential) && $bedrooms > 0 ? $bedrooms.' Bedroom ' : '').$type.' '.$label.' in '.$suburb;
    }



    public function getFeaturedProperties($type){
        $featured = FeaturedListing::with('property')
            ->where('site_id',siteID())
            ->where('start_date','<=',date('Y-m-d'))
            ->where('expiry_date','>=',date('Y-m-d'))
            ->where('pb_approval',1)
            ->whereHas('property',function ($query) use ($type){
                $query->whereDoesntHave('featuredView',function ($query){
                    $query->where('session_id',Session::getId());
                });
                $query->whereHas('statuses',function ($query){
                    $query->where('status_id','>',1);
                    $query->where('status_id','!=',3);
                });
                $query->whereHas('offer',function ($query) use ($type){
                    if($type != 'home'){
                        $query->where('label',$type);
                    }
                });
                $query->where('active',1);
            })
            ->orderBy(DB::raw('RAND()'))
            ->take(($type == 'home' ? 6 : 3))
            ->get();

        if($featured->count() == 0){
            FeaturedView::where('session_id',Session::getId())
                ->delete();
            $featured = FeaturedListing::with('property')
                ->where('site_id',siteID())
                ->where('start_date','<=',date('Y-m-d'))
                ->where('expiry_date','>=',date('Y-m-d'))
                ->whereHas('property',function ($query) use ($type){
                    $query->whereDoesntHave('featuredView',function ($query){
                        $query->where('session_id',Session::getId());
                    });
                    $query->whereHas('statuses',function ($query){
                        $query->where('status_id','>',1);
                        $query->where('status_id','!=',3);
                    });
                    $query->whereHas('offer',function ($query) use ($type){
                        if($type != 'home'){
                            $query->where('label',$type);
                        }
                    });
                    $query->where('active',1);
                })
                ->orderBy(DB::raw('RAND()'))
                ->take(($type == 'home' ? 6 : 3))
                ->get();
            if($type == 'home'){
                if($featured->count() == 0){
                    $featured = FeaturedListing::with('property')
                        ->where('site_id',pbID())
                        ->where('start_date','<=',date('Y-m-d'))
                        ->where('expiry_date','>=',date('Y-m-d'))
                        ->whereHas('property',function ($query) use ($type){
                            $query->whereDoesntHave('featuredView',function ($query){
                                $query->where('session_id',Session::getId());
                            });
                            $query->whereHas('statuses',function ($query){
                                $query->where('status_id','>',1);
                                $query->where('status_id','!=',3);
                            });
                            $query->whereHas('offer',function ($query) use ($type){
                                if($type != 'home'){
                                    $query->where('label',$type);
                                }
                            });
                            $query->where('active',1);
                        })
                        ->orderBy(DB::raw('RAND()'))
                        ->take(($type == 'home' ? 6 : 3))
                        ->get();
                }
            }

        }
        return $featured;
    }


    public function reportProperty(Request $request){
        $queries = $request->query();
        if(isset($queries['link'])){
            dispatch(new SaveUserReport($queries['propertyID'],$request->ip(),$request['link']));
        }
        return response()->json('Success',200);
    }

    public function filteredFeaturedListings(string $type,string $location,string $slug,array $queries,array $pTypes,$try = 0){
        $featured = FeaturedListing::with('property')
            ->where('site_id',siteID())
            ->where('start_date','<=',date('Y-m-d'))
            ->where('expiry_date','>=',date('Y-m-d'))
            ->where('pb_approval',1)
            ->whereHas('property',function ($query) use ($type,$location,$slug,$queries,$pTypes){
                $query->whereDoesntHave('featuredView',function ($query){
                    $query->where('session_id',Session::getId());
                });
                $query->whereHas('statuses',function ($query){
                    $query->where('status_id','>',1);
                    $query->where('status_id','!=',3);
                });
                $query->whereHas('offer',function ($query) use ($type){
                    $query->where('label',$type);
                });
                $query->whereIn('type_id', $pTypes);
                $query->whereHas('suburb',function ($query) use ($location,$slug){
                    if($location == 'suburb'){
                        $query->where('slug',$slug);
                    }
                    $query->whereHas('area',function ($query) use ($location,$slug){
                       if($location == 'area'){
                           $query->where('slug',$slug);
                       }
                       $query->whereHas('town',function ($query) use ($location,$slug){
                           if($location == 'town'){
                               $query->where('slug',$slug);
                           }
                           $query->whereHas('province',function ($query) use ($location,$slug){
                               if($location == 'province' && $slug != 'zimbabwe'){
                                   $query->where('slug',$slug);
                               }
                           });
                       });
                    });
                });
                $query->whereHas('features', function ($query) use ($queries) {
                    $query->select('id');
                    if (isset($queries['beds']) && $queries['beds'] > 0) {
                        $query->where('bedrooms', '>=', $queries['beds']);
                    }
                    if (isset($queries['baths']) && $queries['baths'] > 0) {
                        $query->where('bathrooms', '>=', $queries['baths']);
                    }
                    if (isset($queries['propertyArea']) && $queries['propertyArea'] > 0) {
                        $query->where('property_size',  '>=', $queries['propertyArea']);
                    }
                });
                $query->where('active',1);
            })
            ->orderBy(DB::raw('RAND()'))
            ->take(3)
            ->get();

        if($featured->count() == 0){
            FeaturedView::where('session_id',Session::getId())
                ->delete();
            if($try == 0){
                return $this->filteredFeaturedListings($type,$location,$slug,$queries,$pTypes,1);
            }

        }
        return $featured;
    }

    public function filteredProperties(string $type,string $location,string $slug,array $queries,array $pTypes,string $col,string $dir,Request $request){
        return Property::with([
            'offer:id,label,suffix',
            'suburb' => function ($query) {
                $query->select('id', 'name', 'slug', 'area_id');
                $query->with([
                    'area' => function ($query) {
                        $query->select('id', 'name', 'slug', 'town_id');
                        $query->with([
                            'town' => function ($query) {
                                $query->select('id', 'name', 'slug', 'province_id');
                                $query->with([
                                    'province' => function ($query) {
                                        $query->select('id', 'slug');
                                    }
                                ]);
                            }
                        ]);
                    }
                ]);
            },
            'type:id,type',
            'agents' => function ($query) {
                $query->select('id', 'property_id', 'agent_1');
                $query->with([
                    'agentDetail' => function ($query) {
                        $query->select('user_id', 'agency_id');
                        $query->with([
                            'agency' => function ($query) {
                                $query->select('id', 'slug', 'name', 'hex_code', 'overlay', 'text_color', 'logo');
                                $query->with([
                                    'settings' => function ($query) {
                                        $query->select('agency_id', 'custom_rate', 'rate');
                                    }
                                ]);
                            }
                        ]);
                    }
                ]);
            },
            'features:property_id,bedrooms,lounges,bathrooms,boreholes',
            'images:property_id,main_image'
        ])
            ->select('id', 'suburb_id', 'type_id', 'offer_id', 'ref_code', 'description', 'created_at', 'zwl_price','usd_price')
            ->whereHas('suburb', function ($query) use ($location,$slug) {
                $query->select('id', 'area_id');
                if($location == 'suburb'){
                    $query->where('slug',$slug);
                }
                $query->whereHas('area', function ($query) use ($location,$slug) {
                    $query->select('id', 'town_id');
                    if($location == 'area'){
                        $query->where('slug',$slug);
                    }
                    $query->whereHas('town', function ($query) use ($location,$slug) {
                        $query->select('id', 'province_id');
                        if($location == 'town'){
                            $query->where('slug',$slug);
                        }
                        $query->whereHas('province', function ($query) use ($location,$slug) {
                            $query->select('id');
                            if($location == 'province' && $slug != 'zimbabwe'){
                                $query->where('slug',$slug);
                            }
                        });
                    });
                });
            })
            ->whereHas('offer', function ($query) use ($type) {
                $query->where('label', $type);
            })
            ->whereHas('sites',function ($query){
                $query->where('site_id',siteID());
            })
            ->whereHas('features', function ($query) use ($queries) {
                if (isset($queries['beds'])) {
                    $query->where('bedrooms', '>=', $queries['beds']);
                }
                if (isset($queries['baths'])) {
                    $query->where('bathrooms', '>=', $queries['baths']);
                }
                if (isset($queries['propertyArea'])) {
                    $query->where('property_size',  '>=', $queries['propertyArea']);
                }
            })
            ->whereHas('statuses',function ($query){
                $query->where('status_id','>',1);
            })
            ->whereIn('type_id', $pTypes)
            ->where(function ($query) use ($queries) {
                if (isset($queries['minPrice'])) {
                    if(Session::get('selected_currency') == 'usd'){
                        $query->where('usd_price', '>=', $queries['minPrice']);
                    }
                    else{
                        $query->where('zwl_price', '>=', $queries['minPrice']);
                    }
                }
                if (isset($queries['maxPrice']) && $queries['maxPrice'] > 0) {
                    if(Session::get('selected_currency') == 'usd'){
                        $query->where('usd_price', '<=', $queries['maxPrice']);
                    }
                    else{
                        $query->where('zwl_price', '<=', $queries['maxPrice']);
                    }
                }
            })
            ->whereHas('sites',function($query){
                $query->where('site_id',siteID());
            })
            ->where('active',1)
            ->orderBy($col, $dir)
            ->paginate(15)
            ->appends($request->query());
    }

    public function advancedFeaturedFilter(string $type,array $suburbs,array $areas,array $towns,array $provinces,array $queries,array $pTypes,$try = 0){
        $featured = FeaturedListing::with('property')
            ->where('site_id',siteID())
            ->where('start_date','<=',date('Y-m-d'))
            ->where('expiry_date','>=',date('Y-m-d'))
            ->where('pb_approval',1)
            ->whereHas('property',function ($query) use ($type,$suburbs,$areas,$towns,$provinces,$queries,$pTypes){
                $query->whereDoesntHave('featuredView',function ($query){
                    $query->where('session_id',Session::getId());
                });
                $query->whereIn('type_id',$pTypes)
                    ->whereHas('offer',function ($query) use ($type){
                        $query->where('label',$type);
                    })
                    ->whereHas('statuses',function ($query){
                        $query->where('status_id','>',1);
                        $query->where('status_id','!=',3);
                    })
                    ->whereHas('sites',function ($query){
                        $query->where('site_id',siteID());
                    })
                    ->whereHas('suburb',function ($query) use ($suburbs,$areas,$towns,$provinces){
                        if(count($suburbs) > 0){
                            $query->whereIn('id',$suburbs);
                            if(count($areas) > 0){
                                $query->orWhereHas('area',function ($query) use ($areas){
                                    $query->whereIn('id',$areas);
                                });
                            }
                            if(count($towns) > 0){
                                $query->orWhereHas('area.town',function ($query) use ($towns){
                                    $query->whereIn('id',$towns);
                                });
                            }
                            if(count($provinces) > 0){
                                $query->orWhereHas('area.town.province',function ($query) use ($provinces){
                                    $query->whereIn('id',$provinces);
                                });
                            }
                        }
                        else{
                            if(count($areas) > 0){
                                $query->whereHas('area',function ($query) use ($areas){
                                    $query->whereIn('id',$areas);
                                });
                                if(count($towns) > 0){
                                    $query->orWhereHas('area.town',function ($query) use ($towns){
                                        $query->whereIn('id',$towns);
                                    });
                                }
                                if(count($provinces) > 0){
                                    $query->orWhereHas('area.town.province',function ($query) use ($provinces){
                                        $query->whereIn('id',$provinces);
                                    });
                                }
                            }
                            else{
                                if(count($towns) > 0){
                                    $query->whereHas('area.town',function ($query) use ($towns){
                                        $query->whereIn('id',$towns);
                                    });
                                    if(count($provinces) > 0){
                                        $query->orWhereHas('area.town.province',function ($query) use ($provinces){
                                            $query->whereIn('id',$provinces);
                                        });
                                    }
                                }
                                else{
                                    if(count($provinces) > 0){
                                        $query->whereHas('area.town.province',function ($query) use ($provinces){
                                            $query->whereIn('id',$provinces);
                                        });
                                    }
                                }
                            }

                        }
                    })
                    ->whereHas('features',function ($query) use($queries){
                        if(isset($queries['beds'])){
                            $query->where('bedrooms','>=',$queries['beds']);
                        }
                        if(isset($queries['baths'])){
                            $query->where('bathrooms','>=',$queries['baths']);
                        }
                        if(isset($queries['propertyArea'])){
                            $query->where('property_size', '>=',$queries['propertyArea']);
                        }
                    })
                    ->where(function ($query) use ($queries){
                        if(isset($queries['minPrice'])){
                            if(Session::get('selected_currency') == 'usd'){
                                $query->where('usd_price', '>=', $queries['minPrice']);
                            }
                            else{
                                $query->where('zwl_price', '>=', $queries['minPrice']);
                            }
                        }
                        if(isset($queries['maxPrice'])  && $queries['maxPrice'] > 0){
                            if(Session::get('selected_currency') == 'usd'){
                                $query->where('usd_price', '<=', $queries['maxPrice']);
                            }
                            else{
                                $query->where('zwl_price', '<=', $queries['maxPrice']);
                            }
                        }
                    })
                    ->where('active',1);
            })
            ->orderBy(DB::raw('RAND()'))
            ->take(3)
            ->get();
        if($featured->count() == 0){
            FeaturedView::where('session_id',Session::getId())
                ->delete();
            if($try == 0){
                return $this->advancedFeaturedFilter($type,$suburbs,$areas,$towns,$provinces,$queries,$pTypes,1);
            }
        }
        return $featured;
    }

    public function mailShot(){
        $img = base64_encode(file_get_contents(base_path('/../15671_1596012265_vbVi16.JPG')));
        $html = view('mail-shot',[
            'img' => $img
        ])->render();
        $pdf = SnappyImage::loadHtml($html);
        $file = base_path('/../property.jpg');
        $pdf->save($file);

        Image::make($file)
            ->crop(334,414)
            ->save($file);

        echo 'Done';
    }

    public function returnPages(Request $request){

        if($request->pageName == 'listing'){
            return view('listing',[
                'properties' => $request->properties,
                'active' => $request->filter,
                'type' =>$request->type,
               /* 'area' => (isset($scope) ? $location : $location->name),*/
                'searchQuery' => $request->searchQuery,
                'sortValue' => $request->sortValue,
                'featured' => $request->featured,
                'pageTitle' => $request->pageTitle,
                'metaDescription' => $request->metaDescription
            ]);
        }
    }
}
