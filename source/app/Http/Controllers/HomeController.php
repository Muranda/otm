<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        return view('index',[
            'pageTitle' => 'OTM Home'
        ])  ;
    }

    public function listings($type ){
        if($type == 'forSale'){
            $pageTitle = 'Properties to Rent';
        }
        if($type == 'toRent'){
            $pageTitle = 'Properties for Sale';
        }

        return view('listing',[
            'pageTitle' => $pageTitle
        ]);
    }

    public function propertyDetail($ref){
        return view('singleListing',[
           'pageTitle' => 'Property Details'
        ]);
    }
}
