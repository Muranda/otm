<?php

namespace App\Notifications;

use App\Channels\InfobipChannel;
use App\Channels\Messages\InfobipMessage;
use App\Models\PropertyEnquiry;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class CarbonTextNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    private $enquiry;
    public function __construct(PropertyEnquiry $enquiry)
    {
        $this->enquiry = $enquiry;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [InfobipChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toInfobip($notifiable){
        $text = 'Please contact '.$this->enquiry->name.' on '.$this->enquiry->country_code.$this->enquiry->phone_number.' or '.$this->enquiry->email.' regarding your listing '.$this->enquiry->property->ref_code.' in '.$this->enquiry->property->suburb->name;
        return (new InfobipMessage())
            ->content($text);
    }
}
