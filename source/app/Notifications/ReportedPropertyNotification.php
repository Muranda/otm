<?php

namespace App\Notifications;

use App\Models\Property;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ReportedPropertyNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    private $user,$link,$property,$phrase;
    public function __construct(User $user,string $link,Property $property,string $phrase)
    {
        $this->user = $user;
        $this->link = $link;
        $this->property = $property;
        $this->phrase = $phrase;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting('Good day '.$this->user->name)
            ->subject($this->property->ref_code.' reported as '.$this->phrase)
            ->line('Please be advised that a user has reported your property <strong>'.$this->property->ref_code.'</strong> ('.$this->link.') as '.$this->phrase.' on '.config('system.FRIENDLY_URL'))
            ->line('If the property is indeed '.$this->phrase.' please log into your account and either hide it or update status to Rented/Sold.')
            ->line('However, if the property is still available, kindly click the button to let us know.')
            ->action('Notification Action', url('/'))
            ->line('Thank you for using our application!')
            ->action('Property Is Available', cloudURL().'/mark-property-available/'.$this->property->id);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
