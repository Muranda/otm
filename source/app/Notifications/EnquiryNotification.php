<?php

namespace App\Notifications;

use App\Channels\InfobipChannel;
use App\Channels\Messages\InfobipMessage;
use App\Models\PropertyEnquiry;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class EnquiryNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    private $user,$enquiry;
    public function __construct(User $user,PropertyEnquiry $enquiry)
    {
        $this->user = $user;
        $this->enquiry = $enquiry;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail',InfobipChannel::class,'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $title = generatePropertyTitle($this->enquiry->type_id,$this->enquiry->property->type->type,$this->enquiry->property->features->bedrooms,$this->enquiry->property->offer->label,$this->enquiry->property->suburb->name);
        $link = propertyLink($this->enquiry->property);
        $cc = [];
        if($this->enquiry->property->agents->agentDetail->first()){
            if($this->enquiry->property->agents->agentDetail->agency->carbonContacts->count() > 0){
                foreach ($this->enquiry->property->agents->agentDetail->agency->carbonContacts as $contact){
                    if($contact->email){
                        if(!$contact->branch_id){
                            $cc[] = $contact->email;
                        }
                        else{
                            if($contact->branch_id == $this->enquiry->property->agents->agentDetail->branch_id){
                                $cc = $contact->email;
                            }
                        }
                    }
                }
            }
        }

        if((is_countable($cc)?$cc:[])){
            $data = (new MailMessage)
                ->greeting('Good day '.$this->user->name)
                ->from('enquiries@cloud.propertybook.co.zw','Propertybook Cloud Enquiries')
                ->replyTo($this->enquiry->email,$this->enquiry->name)
                ->cc($cc)
                ->subject('New Property Enquiry - '.displayRefCode($this->enquiry->property->ref_code))
                ->line('You have received a new enquiry from your listing on <a href="'.route('homePage').'">'.str_replace('https://','',config('system.APP_URL')).'</a> for <a href="'.$link.'">'.$title.'</a>')
                ->line('Name: <span style="font-weight: bold">'.$this->enquiry->name.'</span>')
                ->line('Email: <span style="font-weight: bold">'.$this->enquiry->email.'</span>')
                ->line('Phone Number: <span style="font-weight: bold">'.$this->enquiry->phone_number.'</span>')
                ->line('<span style="font-weight: bold">Message:</span><br />'.$this->enquiry->message);
        }
        else{
            $data = (new MailMessage)
                ->greeting('Good day '.$this->user->name)
                ->from('enquiries@cloud.propertybook.co.zw','Propertybook Cloud Enquiries')
                ->replyTo($this->enquiry->email,$this->enquiry->name)
                ->subject('New Property Enquiry - '.displayRefCode($this->enquiry->property->ref_code))
                ->line('You have received a new enquiry from your listing on <a href="'.route('homePage').'">'.str_replace('https://','',config('system.APP_URL')).'</a> for <a href="'.$link.'">'.$title.'</a>')
                ->line('Name: <span style="font-weight: bold">'.$this->enquiry->name.'</span>')
                ->line('Email: <span style="font-weight: bold">'.$this->enquiry->email.'</span>')
                ->line('Phone Number: <span style="font-weight: bold">'.$this->enquiry->phone_number.'</span>')
                ->line('<span style="font-weight: bold">Message:</span><br />'.$this->enquiry->message);
        }

        return $data;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'notification_type' => 'enquiry',
            'property' => $this->enquiry->property->ref_code,
            'enquiryID' => $this->enquiry->id
        ];
    }

    public function toInfobip($notifiable){
        $text = 'Hi .'.$this->user->name.PHP_EOL.'Please contact '.$this->enquiry->name.' on '.$this->enquiry->phone_number.' or '.$this->enquiry->email.' regarding your listing '.$this->enquiry->property->ref_code.' in '.$this->enquiry->property->suburb->name;
        return (new InfobipMessage())
            ->content($text);
    }
}
