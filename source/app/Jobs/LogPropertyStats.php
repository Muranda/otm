<?php

namespace App\Jobs;

use App\Models\AgencyStat;
use App\Models\AgentStat;
use App\Models\BranchStat;
use App\Models\Property;
use App\Models\PropertyStat;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class LogPropertyStats implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $type,$property;
    public function __construct(string $type,Property $property)
    {
        $this->type = $type;
        $this->property = $property;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $column = $this->type;
        $property = PropertyStat::firstOrNew([
            'property_id' => $this->property->id,
            'site_id' => siteID(),
            'date' => date('Y-m-d')
        ]);
        $property->$column += 1;
        $property->ref_code = $this->property->ref_code;
        $property->save();

        if($this->property->offer->label == 'for Sale'){
            $tag = 'sales';
        }
        else{
            $tag = 'rentals';
        }

        $column = $column.'_'.$tag;

        foreach ($this->property->agents->toArray() as $key => $value){
            if(in_array($key,['agent_1','agent_2','agent_3','agent_4','agent_5','agent_6']) && $value){
                $agent = AgentStat::firstOrNew([
                    'agent_id' => $value,
                    'site_id' => siteID(),
                    'date' => date('Y-m-d')
                ]);
                $agent->$column += 1;
                $agent->save();
            }
        }

        if($this->property->agents->agentDetail){
            $branch = BranchStat::firstOrNew([
                'branch_id' => $this->property->agents->agentDetail->branch_id,
                'site_id' => siteID(),
                'date' => date('Y-m-d')
            ]);
            $branch->$column += 1;
            $branch->save();

            $agency = AgencyStat::firstOrNew([
                'agency_id' => $this->property->agents->agentDetail->agency_id,
                'site_id' => siteID(),
                'date' => date('Y-m-d')
            ]);
            $agency->$column += 1;
            $agency->agency_name = $this->property->agents->agentDetail->agency->name;
            $agency->save();
        }


    }
}
