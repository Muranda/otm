<?php

namespace App\Jobs;

use App\Models\Property;
use App\Models\ReportedProperty;
use App\Notifications\ReportedPropertyNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SaveUserReport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $propertyID,$ip,$link;
    public function __construct($propertyID,$ip,$link)
    {
        $this->propertyID = $propertyID;
        $this->ip = $ip;
        $this->link = $link;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $property = Property::where('id',$this->propertyID)->firstOrFail();
        $report = new ReportedProperty();
        $report->site_id = siteID();
        $report->property_id = $this->propertyID;
        $report->agent_1 = $property->agents->agent_1;
        $report->ref_code = $property->ref_code;
        $report->user_ip = $this->ip;
        $report->save();

       // $property->agents->user->nofity(new ReportedPropertyNotification($property->agents->user,$this->link,$property,($property->offer->label == 'for Sale' ? 'sold' : 'rented')));
    }
}
