@extends('layouts.master')


@section('site-content')
    <div class="hidden-xs">
        @include('layouts.innerSearch')
    </div>
    <!-- Properties details page start -->
    <div class="content-area  properties-details-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <!-- Header -->
                     <div class="singleListing heading-properties clearfix sidebar-widget hidden-sm hidden-xs">
                        <div class="pull-left">
                            <h3>{{ generatePropertyTitle($property->type_id,$property->type->type,$property->offer->label,$property->suburb->name) }}</h3>
                            <p>
                                <i class="fa fa-map-marker-alt"></i> {{ $property->suburb->area->name.' , '.$property->suburb->area->town->province->name  }}
                            </p>
                        </div>
                        <div class="pull-right">

                            <h3><span>{{ str_replace('/pm','',displayPrice($property))  }}</span> <span class="per-month hidden-md hidden-lg hidden-sm">Per Month</span> </h3>
                            <h5 class="hidden-xs {{ ($property->offer->label == 'to Rent' ? '' : 'hide') }}">
                                Per Month
                            </h5>
                        </div>
                    </div>
                    <!-- Properties details section start -->
                        <div class="Properties-details-section sidebar-widget">
                        <!-- Properties detail slider start -->
                        <div class="properties-detail-slider simple-slider mb-40">

                            <div id="carousel-custom" class="carousel slide" data-ride="carousel">
                                <div class="carousel-outer">
                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner">
                                        @if($property->images->youtube_link || $property->images->facebook_link)
                                            <div class="item active">
                                                <div class="myIframe">
                                                    {!! generateIframe(($property->images->youtube_link ? 'youtube' : 'facebook'),($property->images->youtube_link ? $property->images->youtube_link : $property->images->facebook_link)) !!}
                                                </div>
                                            </div>
                                        @endif

                                        <div class="item {{ ($property->images->youtube_link || $property->images->facebook_link) ? '' : 'active' }}">
                                            <picture>
                                                <source srcset="{{resolvePropertyImage('medium',$property->images->main_image,$property,'webp') }}" type="image/webp">
                                                <img src="{{resolvePropertyImage('medium',$property->images->main_image,$property,'standard') }}" alt="{{ $property->pageTitle  }}" class="thumb-preview" >
                                            </picture>
                                        </div>
                                        @foreach($property->images->toArray() as $key=>$value)
                                            @if(!in_array($key,['id','property_id','facebook_link','youtube_link','main_image','created_at','updated_at']) && $value)
                                                <div class="item">
                                                    <picture>
                                                        <source srcset="{{resolvePropertyImage('medium',$value,$property,'webp') }}" type="image/webp">
                                                        <img src="{{resolvePropertyImage('medium',$value,$property,'standard') }}" alt="{{ $property->pageTitle  }}" class="thumb-preview" >
                                                    </picture>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                    <!-- Controls -->
                                    <a class="left carousel-control" href="#carousel-custom" role="button" data-slide="prev">
                                    <span class="slider-mover-left no-bg t-slider-r pojison" aria-hidden="true">
                                        <i class="fa fa-angle-left"></i>
                                    </span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="right carousel-control" href="#carousel-custom" role="button" data-slide="next">
                                    <span class="slider-mover-right no-bg t-slider-l pojison" aria-hidden="true">
                                        <i class="fa fa-angle-right"></i>
                                    </span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <!-- Indicators -->
                                <ol class="carousel-indicators thumbs visible-lg visible-md">
                                    @if(!$property->images->youtube_link && !$property->images->facebook_link)
                                        <li data-target="#carousel-custom" data-slide-to="0" class="">
                                            <picture>
                                                <source srcset="{{resolvePropertyImage('mobile',$property->images->main_image,$property,'webp') }}" type="image/webp">
                                                <img src="{{resolvePropertyImage('mobile',$property->images->main_image,$property,'standard') }}" alt="{{ $property->pageTitle  }}" class="thumb-preview" >
                                            </picture>
                                        </li>
                                        @php
                                            $count = 1;
                                        @endphp
                                    @else
                                        <li data-target="#carousel-custom" data-slide-to="0" class="">
                                            <img id="vt-image" src="{{ URL::asset('/assets/v-tour.jpg') }}" alt="{{ $property->pageTitle  }}" class="thumb-preview" >
                                        </li>
                                        <li data-target="#carousel-custom" data-slide-to="1" class="">
                                            <picture>
                                                <source srcset="{{resolvePropertyImage('mobile',$property->images->main_image,$property,'webp') }}" type="image/webp">
                                                <img src="{{resolvePropertyImage('mobile',$property->images->main_image,$property,'standard') }}" alt="{{ $property->pageTitle  }}" class="thumb-preview" >
                                            </picture>
                                        </li>
                                        @php
                                            $count = 2;
                                        @endphp
                                    @endif

                                    @foreach($property->images->toArray() as $key=>$value)
                                        @if((!in_array($key,['id','property_id','facebook_link','youtube_link','main_image','created_at','updated_at']) && $value))

                                            <li data-target="#carousel-custom" data-slide-to="{{ $count }}" class="">
                                                <picture>
                                                    <source srcset="{{resolvePropertyImage('mobile',$value,$property,'webp') }}" type="image/webp">
                                                    <img src="{{resolvePropertyImage('mobile',$value,$property,'standard') }}" alt="{{ $property->pageTitle  }}" class="thumb-preview" >
                                                </picture>
                                            </li>
                                                @php
                                                    $count++ ;
                                                @endphp
                                        @endif
                                    @endforeach
                                </ol>
                            </div>
                        </div>
                        <!-- Properties detail slider end -->
                        <div class="singleListing heading-properties clearfix sidebar-widget hidden-lg hidden-md">
                            <div class="pull-right">
                                <h3>
                                    <span>{{ str_replace('/pm','',displayPrice($property))  }}</span>
                                    <span class="per-month hidden-md hidden-lg hidden-sm {{ ($property->offer->label == 'to Rent' ? '' : 'hide') }}">Per Month</span> </h3>
                                <h5 class="hidden-xs">
                                    Per Month
                                </h5>
                            </div>
                            <div class="pull-left">
                                <h3>{{ generatePropertyTitle($property->type_id,$property->type->type,$property->offer->label,$property->suburb->name) }}</h3>
                                <p>
                                    <i class="fa fa-map-marker-alt"></i>{{ $property->suburb->area->name.' , '.$property->suburb->area->town->province->name  }}
                                </p>
                            </div>
                        </div>

                        <!-- Property description start -->
                        <div class="panel-box properties-panel-box Property-description">
                            <ul class="nav nav-tabs">
                                <li class="active extra-with"><a href="#tab1default" data-toggle="tab" aria-expanded="true">Description</a></li>
                                <li class=""><a href="#tab2default" data-toggle="tab" aria-expanded="false">General Features</a></li>
                                <li class="half-width"><a href="#tab3default" data-toggle="tab" aria-expanded="false">External </a></li>
                                <li class="half-width"><a href="#tab4default" data-toggle="tab" aria-expanded="false">Internal</a></li>
                            {{--    <li class=""><a href="#tab5default" data-toggle="tab" aria-expanded="false">Map</a></li>
                           --}} </ul>
                            <div class="panel with-nav-tabs panel-default">
                                <div class="panel-body">
                                    <div class="tab-content">
                                        <div class="tab-pane fade active in" id="tab1default">
                                            <div class="main-title-2">
                                                <h1><span>{{ $property->title }}</span></h1>
                                            </div>
                                            <p>
                                                {!! nl2br($property->description)  !!}
                                            </p>
                                            <br>
                                        </div>
                                        <div class="tab-pane fade features" id="tab2default">
                                            <!-- Properties condition start -->
                                            <div class="properties-condition">
                                                <div class="main-title-2">
                                                    <h1><span>General Features</span></h1>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="row">
                                                            @foreach( $features->where('category','general')->where('data_type','Integer') as $feature)
                                                                @php($key = dbColumn($feature->name))
                                                                @if($property->features->$key )
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 condition ">
                                                                    <ul class="condition">
                                                                        <li class="features">
                                                                            @if($feature->id == 19 or $feature->id == 31)
                                                                                <i class="fa fa-check-square"></i>
                                                                                <span style="position: absolute;top: 5px">{{ $feature->name }}: {{ propertySize($property->features->$key) }}</span>
                                                                            @else
                                                                             <span class="number-box">{{ $property->features->$key }}</span> {{ $feature->name }}
                                                                            @endif
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                @endif
                                                            @endforeach
                                                            @foreach($features->where('category','general')->where('data_type','Boolean') as $booleanFeature)
                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 condition ">
                                                                        <ul class="condition">
                                                                            <li class="features"> <i class="fa fa-check-square"></i>
                                                                                <span style="position: absolute;top: 5px">
                                                                                    {{ $booleanFeature->name }}
                                                                                </span>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                            @endforeach
                                                       </div>
                                                   </div>
                                                </div>
                                            </div>
                                            <!-- Properties condition end -->
                                        </div>
                                        <div class="tab-pane fade technical" id="tab3default">
                                            <!-- Properties amenities start -->
                                            <div class="properties-amenities">
                                                <div class="main-title-2">
                                                    <h1><span>External Features</span></h1>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="row">
                                                            @foreach( $features->where('category','exterior')->where('data_type','Integer') as $exfeature)
                                                                @if($property->features->$key )
                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 condition ">
                                                                        <ul class="condition">
                                                                            <li class="features">
                                                                               <span class="number-box">{{ $property->features->$key }}</span> {{ $feature->name }}
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                @endif
                                                            @endforeach
                                                                @foreach($features->where('category','exterior')->where('data_type','Boolean') as $booleanExFeature)
                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 condition ">
                                                                        <ul class="condition">
                                                                            <li class="features"> <i class="fa fa-check-square"></i>
                                                                                <span style="position: absolute;top: 5px">
                                                                                    {{ $booleanExFeature->name }}
                                                                                </span>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Properties amenities end -->
                                        </div>
                                        <div class="tab-pane fade" id="tab4default">
                                            <!-- Inside properties start  -->
                                            <div class="properties-amenities">
                                                <!-- Main Title 2 -->
                                                <div class="main-title-2">
                                                    <h1><span>Internal Features</span></h1>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="row">
                                                            @foreach( $features->where('category','interior')->where('data_type','Integer') as $Interiorfeature)
                                                                @if($property->features->$key )
                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 condition ">
                                                                        <ul class="condition">
                                                                            <li class="features">
                                                                                <span class="number-box">{{ $property->features->$key }}</span> {{ $Interiorfeature->name }}
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                @endif
                                                            @endforeach
                                                            @foreach($features->where('category','interior')->where('data_type','Boolean') as $booleanInteriorFeature)
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 condition ">
                                                                    <ul class="condition">
                                                                        <li class="features"> <i class="fa fa-check-square"></i>
                                                                            <span style="position: absolute;top: 5px">
                                                                                {{ $booleanInteriorFeature->name }}
                                                                            </span>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                   </div>
                                                </div>
                                            </div>
                                            <!-- Inside properties end -->
                                        </div>
                                 </div>
                                </div>
                            </div>
                        </div>
                        <!-- Property description end -->
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <!-- Sidebar start -->
                    <div class="sidebar right">
                        <!-- Search contents sidebar start -->
                        <div class="sidebar-widget">
                            @php($agency = $property->agents->agentDetail)
                            <div class="main-title-2 siberBar-agency-logo">
                               <picture>
                                    <source srcset="{{ ( $agency ? getWebPImage('agencyLogo',$agency->agency->logo)  : imageURL().'private-lister-logo.png') }}" type="image/webp">
                                    <img src="{{ imageURL().($agency ? 'agency-logos/'.$agency->agency->logo : 'private-lister-logo.png')  }}" class="img-responsive" alt="{{ ($agency ? $agency->agency->name : 'Private Lister')  }}">
                                </picture>
                            </div>
                            <div class="contact-1">
                                <div class="main-title-2 agency-name">
                                    <h1>{{ $agency->agency->name }}</h1>
                                </div>
                            </div>

                            <div class="contact-1">
                                <a id="agent-details-btn" class="btn otm-button btn-block" onclick="hideButton('agent-details')" data-toggle="collapse" data-target=".agent-details"> Call Agent</a>
                            </div>

                            <div class="contact-1 agent-details collapse">
                                <div class="ref-div">
                                    Please reference  <span class="singleList-refCode">{{ displayRefCode($property->ref_code) }}</span> when making your enquiry
                                </div>
                                {!! propertyAgentDetails($property->agents,$property->ref_code,generatePropertyTitle($property->type_id,$property->type->type,$property->offer->label,$property->suburb->name),propertyLink($property)) !!}
                             </div>

                            {{--<div class="contact-1">
                                <a id="send-email" onclick="hideButton('send-email')" class="btn btn-subscribe btn-block"  data-toggle="collapse" data-target=".send-email"> Email Agent</a>
                            </div>--}}
                            <div class="contact-1 send-email ">
                                <div class="main-title-2 send-email">
                                    <h1>Send Email</h1>
                                </div>
                                <div class="contact-form">
                                    <form id="contact_form" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group fullname">
                                                    <input type="text" name="full_name" class="input-text" placeholder="Full Name" value="{{ old('full_name') }}">
                                                    <span id="error-full_name" class="help-block error-full_name"></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group enter-email">
                                                    <input type="email" name="email" class="input-text"  placeholder="Enter email" value="{{ old('email') }}" >
                                                    <span id="error-email" class="help-block error-email"></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group number">
                                                    <input type="text" name="phoneNumber" class="input-text" placeholder="Phone Number" value="{{ old('phoneNumber') }}">
                                                    <span id="error-phoneNumber" class="help-block error-phoneNumber"></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group message">
                                                    <textarea class="input-text" name="message" placeholder="Write message">{!! old('message') !!}</textarea>
                                                    <span id="error-message" class="help-block error-message"></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group send-btn text-center">
                                                    <button type="button" class="btn search-button btn-block" id="send-enquiry-btn">
                                                        <span id="processingForm" style="display: none"><i class="fa fa-spinner fa-spin"></i></span>
                                                        <span class="send-enquiry">Send Email</span>
                                                    </button>
                                                    <span class="enquiry-sent" style="display: none;color: #3282C4;font-weight: bold">Enquiry Sent <i class="fa fa-check"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- Social media start -->
                            <div class="social-media sidebar-widget clearfix">
                                <!-- Main Title 2 -->
                                <div class="main-title-2">
                                    <h1><span>Share</span></h1>
                                </div>
                                <!-- Social list -->
                                <ul class="social-list">
                                    <li>
                                        <a class="facebook-bg" href="https://www.facebook.com/sharer/sharer.php?u={{ URL::current() }}" target="_blank"> <i class="fab fa-facebook-f"></i></a>
                                    </li>
                                    <li>
                                        <a class="twitter-bg" href="http://twitter.com/share?text=Check out this property&url={{ URL::current() }}" target="_blank"> <i class="fab fa-twitter"></i></a>
                                    </li>
                                    <li>
                                       <a  class="whatsapp-bg" href="https://wa.me/?text={{ urlencode('Check out this property - ').URL::current() }}" target="_blank"> <i class="fab fa-whatsapp"></i></a>
                                    </li>
                                    <li>
                                        <a class="linkedin-bg" href="mailto:?subject={{ config('system.APP_NAME') }} - {{ $pageTitle }}&body=Check out this property {{ URL::current() }}"> <i class="fa fa-envelope"></i></a>
                                    </li>
                               </ul>
                            </div>

                            <!-- Contact 1 end -->
                            <div class="contact-1 report-btn text-center">
                                <button class="btn btn-danger btn-block" id="property_sold_btn">
                                    <span id="reporting-span" style="display: none">
                                        <i class="fa fa-spinner fa-spin"></i>
                                    </span>
                                    <span id="report-span"> Report Property as Sold</span>
                                </button>
                                <span id="report-sent-span" style="display: none; color: #3282C4"><i class="fa fa-check"> </i> Report Sent</span>
                            </div>
                        </div>
                        <!-- Category posts start -->
                        <div class="sidebar-widget category-posts">
                            <div class="main-title-2">
                                <h1><span>Popular</span> Locations</h1>
                            </div>
                            <ul class="list-unstyled list-cat">
                                @foreach( popularLocationsSidebar() as $location)
                                    <li>
                                        <a href="{{ route('filterProperties',['filter'=>$active,'province'=>$location->area->town->province->slug,'town'=>$location->area->town->slug,'area'=>$location->area->slug,'suburb'=>$location->slug]) }}">
                                            {{ $location->name  }}
                                        </a> <span>({{ $location->properties_count }})  </span>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- Sidebar end -->
                </div>
            </div>
        </div>
    </div>
    <!-- Properties details page end -->
@endsection
@section('scripts')
    <script>
        function hideButton(button){
            if(button === 'agent-details' ){
                $('#agent-details-btn').hide();
            }
            if(button === 'send-email'){
                $('#send-email').hide();
            }
        }
    </script>
    <script>
        form = $('#contact_form');
        $('#send-enquiry-btn').click(function(e){
            form.submit();
        });
        form.submit(function (e){
            $('#processingForm').show();
            $('.send-enquiry').hide();
            $('.form-group').removeClass('has-error');
            $('.help-block').text('');
            $.ajax({
                url:'{{ route('sendEnquiryEmail',['propertyID' => $property->id]) }}',
                data:  new FormData(this),
                method:'post',
                processData: false,
                contentType: false,
                success: function (data) {
                    $('#processingForm').hide();
                    $('.enquiry-sent').show();
                    form[0].reset();
                    $('#send-enquiry-btn').hide();
                    $('.form-group').removeClass('has-error');
                    $('.help-block').text('');
                },
                error: function (data) {
                    $('#processingForm').hide();
                    $('.send-enquiry').show();
                    var errorData = jQuery.parseJSON(data.responseText);
                    var errors = errorData.errors;
                    $.each(errors,function (index,value) {
                        $('.'+index).addClass('has-error');
                        $('.error-'+index).text(value);
                    });
                }
            }),
            e.preventDefault();
        })


    </script>
    <script>
        $('#property_sold_btn').click( function (e){
            $('#report-span').hide();
            $('#reporting-span').show();
            $.ajax({
                url: '{{ URL::to('/report-property') }}' + '?link={{url()->current()}}&propertyID='+{{$property->id}},
                method: 'get',
                success: function (data) {
                    $('#property_sold_btn').hide();
                    $('#report-span').hide();
                    $('#reporting-span').hide();
                    $('#report-sent-span').show();
                },
                error: function (data) {
                    $('#report-span').show();
                    $('#reporting-span').hide();
                }
            });
        });

    </script>
    <script>
        function logClick(type,phone,text){
            $.ajax({
                url: '{{ URL::to('/logPropertyEnquiry') }}' + '/' + type + '/' + '{{ $property->id }}',
                method: 'get',
                success: function (data) {

                },
                error: function (data) {

                }
            });
            /*gtag('event','Show Phone Number',{
                'event_category': type,
                'event_label': '',
                'value': ''
            });*/
            if(type === 'click_to_call'){
                window.location.href="tel:"+phone;
            }
            else if(type === 'click_to_whatsapp'){
                var link = window.open("https://wa.me/"+phone+"?text="+text, '_blank');
                link.focus();
            }
        }
    </script>


@endsection
