@extends('layouts.master')
@section('site-content')
    <!-- Banner start -->
    @php
        $search = 'for-sale';
    @endphp
    <div class="banner">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item item-100vh  active">
                    <img src="{{ URL::asset('assets/img/background.jpg') }}" alt="banner-slider-1">
                    <div class="carousel-caption banner-slider-inner">
                        <div class="banner-content container banner-content-left">
                            <!-- Search area -->
                            <div class="search-area-inner">
                                <h3 class="text-left">Search for Properties in Zimbabwe </h3>
                                <div class="search-contents">
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 remove-padding-left">
                                        <div class="property-types-tabs text-left ">
                                            <ul class="list-inline">
                                                <li class="search-type">
                                                    <a class="tab-for-sale active" onclick="searchInnerCategoryTab('for-sale')">For Sale</a>
                                                </li>
                                                <li class="search-type">
                                                    <a class="tab-to-rent" onclick="searchInnerCategoryTab('to-rent')">To Rent</a>
                                                </li>
                                            </ul>
                                        </div>

                                        <input type="text" id="search-filter" class="hidden" name="searchType" value="for-sale" />

                                    </div>
                                    <form id="search-inner-form" action="{{ route('findProperties',['filter'=>'for-sale']) }}" method="post">
                                        @csrf
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <div class="homepage form-group locations">
                                                    <select id="location-txt" class="form-control location-select  search-fields" multiple="multiple"  name="searchText[]" data-live-search="true" data-live-search-placeholder="Search Location">
                                                        <option>Select Location</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                                <div class="homepage form-group bed-bath">
                                                    <select id="search-location" class="selectpicker search-fields" name="propertyType[]" data-live-search="true" data-live-search-placeholder="Search value">
                                                        <option value="">Property Types</option>
                                                        @foreach($globalPropertyTypes as $globalPropertyType)
                                                            <option value="{{ $globalPropertyType->id }}" >{{$globalPropertyType->type}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                                <div class="homepage form-group bed-bath">
                                                    <select class="selectpicker search-fields" name="beds" data-live-search="true" data-live-search-placeholder="Bedrooms" >
                                                        <option value="">Bedrooms</option>
                                                        <option>1+</option>
                                                        <option>2+</option>
                                                        <option>3+</option>
                                                        <option>4+</option>
                                                        <option>5+</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                                <div class="homepage form-group bed-bath">
                                                    <select id="baths" class="selectpicker search-fields bathrooms" name="baths" data-live-search="true" data-live-search-placeholder="Bathrooms" >
                                                        <option value="">Bathrooms</option>
                                                        <option value="1">1+ </option>
                                                        <option value="2">2+ </option>
                                                        <option value="3">3+ </option>
                                                        <option value="4">4+ </option>
                                                        <option value="5">5+ </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                                <div class="form-group text-left">
                                                    <input id="search-area-txt" type="text" class="form-control" name="propertyArea" placeholder="Area" oninput="validate('area')">
                                                    <span id="home-area-message" class="error-text"></span>
                                                    <a href="#" class="btn area-button" data-toggle="collapse" data-target=".area-si-unit">
                                                       <span class="selected-si">ac</span> <i class="fa fa-chevron-down"></i>
                                                    </a>

                                                    <div class="area-si-unit collapse">
                                                        <ul class="si-unit text-left">
                                                            <li class="unit m2" onclick="changeTo('m2')">m <sup>2</sup></li>
                                                            <li class="unit ac" onclick="changeTo('ac')">Arces (ac)</li>
                                                            <li class="unit ha" onclick="changeTo('ha')">Hecters (ha)</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="minPrice change-float col-lg-2 col-md-2 col-sm-6 col-xs-6">
                                                <div class="homepage form-group bed-bath">
                                                    <select id="min-price" class="selectpicker search-fields bathrooms" name="minPrice" data-live-search="true" data-live-search-placeholder="Search value" >
                                                        <option id="min-price-placeholder" value="">Min Price</option>
                                                       @foreach($priceFilters as $price)
                                                            <option>{{ ($selected_currency == 'usd' ? $price->usd : $price->zwl)  }}</option>
                                                       @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="maxPrice col-lg-2 col-md-2 col-sm-6 col-xs-6">
                                                <div class="homepage form-group bed-bath">
                                                    <select id="max-price" class="selectpicker search-fields bathrooms" name="maxPrice" data-live-search="true" data-live-search-placeholder="Search value" >
                                                        <option id="max-price-placeholder" value="">Max Price</option>
                                                        @foreach($priceFilters as $price)
                                                            <option>{{ ($selected_currency == 'usd' ? $price->usd : $price->zwl)  }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                           </div>
                                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <button type="submit" id="search-btn" class="search-button search-btn">
                                                        <span id="processingForm" style="display: none"><i class="fa fa-spinner fa-spin"></i></span>
                                                        <span id="search-btn-label">Search</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- End search area -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Banner end -->
    <!-- Partners block start -->
    <div class="partners-block">
        <div class="container">
            <h2>Featured Agencies</h2>
            <div class="row">
                @foreach($agencies as $agency)
                    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 {{ ($loop->first ? '' : ($loop->count == 1 ? 'box' : ($loop->count == 2 ? 'third-logo box' : ''))) }}">
                        <div class="partner-box">
                            <a href="#">
                                <picture>
                                    <source srcset="{{ imageURL().'agency-logos/'.$agency->agency->logo }}" type="image/webp">
                                    <img src="{{ imageURL().'agency-logos/'.$agency->agency->logo }}" class="img-responsive" alt="{{ $agency->agency_name }}">
                                </picture>
                            </a>
                        </div>
                    </div>
                @endforeach
              </div>
        </div>
    </div>
    <!-- Partnersexitr block end -->

    <!-- Featured properties start -->
    <div class="content-area featured-properties">
        <div class="container">
            <!-- Main title -->
            <div class="custom-main-title">
                <h1>Featured Properties</h1>
            </div>
            <div class="row">
                @foreach($featured as $property)
                    <div id="featured-{{$property->property->id}}" class="homepage col-lg-4 col-md-6 col-sm-6 col-xs-12  filtr-item">
                        <a href="{{ propertyLink($property->property) }}">
                            <div class="property">
                                <!-- Property img -->
                                <div class="property-img">
                                    <div class="property-tag button alt featured hidden">Featured</div>
                                    <div class="home property-tag button sale">
                                        {{ strtoupper($property->property->offer->label )  }}
                                    </div>
                                    <div class="property-price">
                                        <span class="featured-price"> {{ displayPrice($property->property) }} </span>
                                        <span class="featured-label property-tag sale hidden">Featured Listings</span></div>
                                    <picture>
                                        <source srcset="{{resolvePropertyImage('medium',$property->property->images->main_image,$property->property,'webp') }}" type="image/webp">
                                        <img src="{{resolvePropertyImage('medium',$property->property->images->main_image,$property->property,'standard') }}" alt="{{ $property->property->pageTitle  }}" class="img-responsive">
                                    </picture>
                                </div>
                                <!-- Property content -->
                                <div class="property-content">
                                    <!-- title -->
                                    <div class="row one-px-left">
                                        <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                                            <div class="title-location">
                                                <h1 class="title">
                                                    {{--javascript:void(0)--}}
                                                    <a href=" {{ propertyLink($property->property) }} ">
                                                        {{ generatePropertyTitle($property->property->type_id,$property->property->type->type,$property->property->offer->label,$property->property->suburb->name)  }}
                                                    </a>
                                                </h1>
                                            </div>
                                        </div>
                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 remove-padding-left">
                                            <div class="property-agent-logo">
                                                <picture>
                                                    <source srcset="{{ imageURL().'agency-logos/'.$property->property->agents->agentDetail->agency->logo }}" type="image/webp">
                                                    <img src="{{ imageURL().'agency-logos/'.$property->property->agents->agentDetail->agency->logo }}" class="img-responsive" alt="{{ $property->property->agents->agentDetail->agency_name }}">
                                                </picture>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Property address -->
                                    <h3 class="property-address one-px-left">
                                        <a href="{{ propertyLink($property->property) }}">
                                            <i class="fa fa-map-marker-alt"></i>{{ $property->property->suburb->area->name.' , '.$property->property->suburb->area->town->province->name  }}
                                        </a>
                                    </h3>

                                    <!-- Facilities List -->
                                    <ul class="custom-facilities-list clearfix one-px-left">
                                        @foreach(iconDisplay($property->property->features ) as $key => $value)
                                            <li class="li-line-height">
                                                <i class="@if($key == "boreholes" )fa fa-tint @endif
                                                @if($key == "bedrooms") fa fa-bed @endif
                                                @if($key == "bathrooms") fa fa-bath @endif
                                                @if($key == "lounges") fa fa-couch @endif
                                                    "></i>
                                                <span> {{ $value }} @if($key == "bedrooms") {{ ($value > 1 ? 'beds' : 'bed') }}
                                                    @elseif($key == "bathrooms") {{ ($value > 1 ? 'baths' : 'bath')}}
                                                    @elseif($key == "lounges") {{ ($value > 1 ? 'lounges' : 'lounge')}}
                                                    @endif
                                                </span>
                                                <span class="pipe {{ ($loop->last ? 'hidden' :'') }} ">|</span>
                                            </li>
                                        @endforeach

                                    </ul>

                                    <div class="description text-left one-px-left">
                                            {{  Str::limit($property->property->description, 80)  }}
                                    </div>

                                    <!-- Property footer -->
                                    <div class="property-footer" style="margin-top: 0!important;">
                                        <a class="btn btn-block otm-button" href="{{ propertyLink($property->property) }}"> View More </a>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
              </div>
        </div>
    </div>
    <!-- Featured properties end -->
    <!-- Recently properties start -->
    <div class="mb-70 recently-properties chevron-icon">
        <div class="container">
            <!-- Main title -->
            <div class="custom-main-title">
                <h1><span>Recent</span> Properties</h1>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 carousel our-partners slide" id="ourPartners2">
                    @foreach($newProperties as $newProperty)
                        <div id="recent-property-{{$newProperty->id}}" class="item">
                            <div class="recent-property col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <!-- Property 2 start -->
                                <div class="property-2">
                                    <!-- Property img -->
                                    <div class="property-img">
                                        <div class="featured">
                                            {{ strtoupper($newProperty->offer->label )  }}
                                        </div>
                                        <div class="price-ratings">
                                            <div class="price">{{ displayPrice($newProperty) }}</div>
                                        </div>
                                        <picture>
                                            <source srcset="{{resolvePropertyImage('mobile',$newProperty->images->main_image,$newProperty,'webp') }}" type="image/webp">
                                            <img src="{{resolvePropertyImage('mobile',$newProperty->images->main_image,$newProperty,'standard') }}" class="img-responsive" alt="{{ $newProperty->title }}">
                                        </picture>
                                    </div>
                                    <!-- content -->
                                    <div class="content home">
                                        <!-- title -->
                                        <h4 class="title">
                                            <a href="{{ propertyLink($newProperty) }}">
                                                {{ generatePropertyTitle($newProperty->type_id,$newProperty->type->type,$newProperty->offer->label,$newProperty->suburb->name)  }}
                                            </a>
                                        </h4>
                                        <!-- Property address -->
                                        <h3 class="property-address">
                                            <a href="{{ propertyLink($newProperty) }}">
                                                <i class="fa fa-map-marker-alt"></i>{{ $newProperty->suburb->area->name.' , '.$newProperty->suburb->area->town->province->name  }},
                                            </a>
                                        </h3>
                                    </div>
                                    <!-- Facilities List -->
                                    <ul class="facilities-list clearfix">
                                        @foreach(iconDisplay($newProperty->features ) as $key => $value)
                                            <li class="li-line-height">
                                                <i class="@if($key == "boreholes" )fa fa-tint @endif
                                                @if($key == "bedrooms") fa fa-bed @endif
                                                @if($key == "bathrooms") fa fa-bath @endif
                                                @if($key == "lounges") fa fa-couch @endif
                                                    "></i>
                                                <span> {{ $value }} </span>
                                                <span class="pipe {{ ($loop->last ? 'hidden' :'') }} ">|</span>
                                            </li>
                                        @endforeach

                                    </ul>
                                    <div class="content">
                                        <a class="btn otm-button btn-block" href="{{ propertyLink($newProperty) }}"> View More</a>
                                    </div>
                                </div>
                                <!-- Property 2 end -->
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="hr-custom"></div>
    </div>
    <!-- Our service start -->
    <div class="mb-100 our-service">
        <div class="container">
            <!-- Main title -->
            <div class="custom-main-title">
                    <h1><span>Join Our </span> Community</h1>
            </div>
            <div class="row mgn-btm wow">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 wow fadeInLeft delay-04s ">
                    <div class="content facebook-section facebook-no-hover">
                        <img src="{{ URL::asset('assets/img/facebook.png') }}">
                        <h4>Like Us on Facebook</h4>
                        <p class="social-media-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et</P>
                        <div class="like-us-btn">
                            <a class="btn btn-outline">
                                Like Our Page
                            </a>
                        </div>
                    </div>
                    <div class="bottom-border"></div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 wow fadeInRight delay-04s left-border">
                    <div class="content facebook-section facebook-no-hover">
                        <img src="{{ URL::asset('assets/img/twitter.png') }}">
                        <h4>Follow Us on Twitter</h4>
                        <p class="social-media-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et</P>
                        <div class="like-us-btn">
                            <a class="btn btn-outline">
                                Click to Follow Us
                            </a>
                        </div>
                    </div>
                    <div class="bottom-border"></div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 wow fadeInRight delay-04s left-border">
                    <div class="content facebook-section facebook-no-hover">
                        <img src="{{ URL::asset('assets/img/groups.png') }}">
                        <h4>Our Facebook Groups</h4>
                        <p class="social-media-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et</P>
                        <div class="like-us-btn">
                            <a class="btn btn-outline">
                                Join Our Groups
                            </a>
                        </div>
                    </div>
                </div>
            </div>
       </div>
    </div>
    <!-- Our service end -->

    <!-- Blog start -->
    <div class="blog content-area">
        <!-- Main title -->
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                    <h3 id="locations_heading" class="sectionHeading">
                        Popular Locations in Zimbabwe
                    </h3>
                </div>
                <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                    <div class=" locations">
                        {!! $topSection !!}
                        <div class="row">
                            <div class="col-lg-2 col-md-3 col-sm-4 col-6">
                                {!! $col1 !!}
                            </div>
                            <div class="col-lg-2 col-md-3 col-sm-4 col-6" >
                                {!! $col2 !!}
                            </div>
                            <div class="col-lg-2 col-md-3 col-sm-4 col-6">
                                {!! $col3 !!}
                            </div>
                            {!! $col4 !!}
                    </div>

                </div>

            </div>

        </div>
    </div>
@endsection
@section('scripts')
    <script>
        if(screen.width <= 834 && screen.width >= 768){
            document.getElementById("max-price").name = 'minPrice';
            document.getElementById("min-price").name = 'maxPrice';
            document.getElementById("min-price-placeholder").innerText = 'Max Price'
            document.getElementById("max-price-placeholder").innerText  = 'Min Price'
        }
    </script>
            <script>
                url = '{{ route('recordStats') }}';
                @foreach($featured as $the_featured)
                $('#featured-{{$the_featured->property->id}}').bind('inview',function (event,visible){
                    if(visible){
                        $.ajax({
                            url: url,
                            method:'get',
                            data:{featured:1,propertyID:'{{$the_featured->property->id}}'},
                            success: function (data){

                            },
                            error: function (data){

                            }
                        })
                    }
                });
                @endforeach
                @foreach($newProperties as $the_property)
                $('#featured-{{$the_property->id}}').bind('inview',function (event,visible){
                    if(visible){
                        $.ajax({
                            url: url,
                            method:'get',
                            data:{propertyID:'{{$the_property->id}}'},
                            success: function (data){

                            },
                            error: function (data){

                            }
                        })
                    }
                });
                @endforeach
            </script>

@endsection
