<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>{{ $pageTitle }} | {{ config('system.APP_NAME') }} </title>
    {{--  <meta name="viewport" content="width=device-width, initial-scale=1.0">--}}
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=0">
    <meta charset="utf-8">
    <meta name="description" content="{{ $metaDescription }}">
    <link rel="canonical" href="{{ str_replace('index.php/','',str_replace('index.php','',URL::current())) }}" />
    <link rel="alternate" href="{{ str_replace('index.php/','',str_replace('index.php','',URL::full())) }}" />
    @if(isset($_GET['page']))
        @if($_GET['page'] > 1)
            <link rel="prev" href="{{ str_replace('page='.$_GET['page'],'page='.($_GET['page'] - 1),URL::full()) }}" />
        @endif
        <link rel="next" href="{{ str_replace('page='.$_GET['page'],'page='.($_GET['page'] + 1),URL::full()) }}" />
    @endif
<!-- External CSS libraries -->
{{--
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/css/bootstrap.min.css') }}">
--}}
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/css/custom_combined.css') }}">
{{--
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/css/animate.min.css') }}">
--}}
{{--
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/css/bootstrap-submenu.css') }}">
--}}
{{--
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/css/bootstrap-select.min.css') }}">
--}}
{{--
    <link rel="stylesheet" href="{{ URL::asset('/assets/css/leaflet.css') }}" type="text/css">
--}}
{{--
    <link rel="stylesheet" href="{{ URL::asset('/assets/css/map.css') }}" type="text/css">
--}}
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/fontawesome/css/all.min.css') }}">
    {{--<link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/fonts/font-awesome/css/font-awesome.min.css') }}">--}}
{{--
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/fonts/flaticon/font/flaticon.css') }}">
--}}
{{--
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/fonts/linearicons/style.css') }}">
--}}
  {{--  <link rel="stylesheet" type="text/css"  href="{{ URL::asset('/assets/css/jquery.mCustomScrollbar.css') }}">
    <link rel="stylesheet" type="text/css"  href="{{ URL::asset('/assets/css/dropzone.css') }}">
    <link rel="stylesheet" type="text/css"  href="{{ URL::asset('/assets/css/magnific-popup.css') }}">
  --}}  <!-- Custom stylesheet -->
{{--
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/css/style.css') }}">
--}}
{{--
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/css/custom-default.css') }}">
--}}
{{--
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/pb-custom.css') }}">
--}}
{{--
    <link href="{{ URL::asset('/assets/css/select2.css') }}" rel="stylesheet"/>
--}}
    <!-- Favicon icon -->
    <link href="{{ imageURL().'sites/'.$site->icon }}" rel="shortcut icon" type="image/png">

    <!-- Google fonts -->
    {{-- <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com!!/css?family=Open+Sans:400,300,600,700,800%7CPlayfair+Display:400,700%7CRoboto:100,300,400,400i,500,700">
    --}}
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
{{--
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/css/ie10-viewport-bug-workaround.css') }}">
--}}

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]><script type="text/javascript" src="{{ URL::asset('/assets/js/ie8-responsive-file-warning.js') }}"></script><![endif]-->
    <script src="{{ URL::asset('/assets/js/ie-emulation-modes-warning.js') }}"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script type="text/javascript" src="{{ URL::asset('/assets/js/html5shiv.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('/assets/js/respond.min.js') }}"></script>
    <![endif]-->
    @yield('css')
</head>
<body>
<div class="page_loader"></div>
@include('layouts.header')
<div class="content">

    @yield('site-content')

</div>
<!-- Footer start -->
<!-- Modal -->
<div id="noSearchTerm" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Search Error</h4>
            </div>
            <div class="modal-body text-center">
                <div class="row">
                    <div class="col-lg-12">
                        <span class="searchTerm-Error">
                            <i class="fa fa-times"></i>
                        </span>
                    </div>
                    <div class="col-lg-12">
                        <span id="error-message">
                            Please select atleast one location to begin your search
                        </span>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

@include('layouts.footer')
<!-- Footer end -->


<script src="{{ URL::asset('/assets/js/custom-combined.js') }}"></script>
<script src="{{ URL::asset('/assets/js/jquery-2.2.0.min.js') }}"></script>
<script src="{{ URL::asset('/assets/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('/assets/js/bootstrap-submenu.js') }}"></script>
<script src="{{ URL::asset('/assets/js/rangeslider.js') }}"></script>
<script src="{{ URL::asset('/assets/js/jquery.mb.YTPlayer.js') }}"></script>
<script src="{{ URL::asset('/assets/js/wow.min.js') }}"></script>
<script src="{{ URL::asset('/assets/js/bootstrap-select.min.js') }}"></script>
<script src="{{ URL::asset('/assets/js/jquery.easing.1.3.js') }}"></script>
<script src="{{ URL::asset('/assets/js/jquery.scrollUp.js') }}"></script>
<script src="{{ URL::asset('/assets/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script src="{{ URL::asset('/assets/js/leaflet.js') }}"></script>
<script src="{{ URL::asset('/assets/js/leaflet-providers.js') }}"></script>
<script src="{{ URL::asset('/assets/js/leaflet.markercluster.js') }}"></script>
<script src="{{ URL::asset('/assets/js/dropzone.js') }}"></script>
<script src="{{ URL::asset('/assets/js/jquery.filterizr.js') }}"></script>
<script src="{{ URL::asset('/assets/js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ URL::asset('/assets/js/maps.js') }}"></script>
<script src="{{ URL::asset('/assets/js/app.js') }}"></script>
<script src="{{ URL::asset('/assets/js/select2.full.js') }}"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="{{ URL::asset('/assets/js/ie10-viewport-bug-workaround.js') }}"></script>
<!-- Custom javascript -->
<script src="{{ URL::asset('/assets/js/ie10-viewport-bug-workaround.js') }}"></script>
<script src="{{ URL::asset('/assets/js/jquery.inview.min.js') }}"></script>
<script>

    search_form = $('#search-form');
    $('#search-btn').click( function (e) {
        if($('#location-txt').val() === null){
            $('#noSearchTerm').modal();
            e.preventDefault();
        }
        else{
            $('#processingForm').show();
            $('#search-btn-label').hide();

        }
    })
    /*** inner search submission java script ***/
    innerSearch_form = $('#inner-search-form');
    $('#inner-search-btn').click( function (e) {
        if($('#location-txt').val() === null){
            $('#noSearchTerm').modal();
            e.preventDefault();
        }
        else{
            $('.processingForm').show();
            $('.search-btn-label').hide();

        }
    })
    /*** mobile search submission java script ***/
    innerSearch_form = $('#mobile-search-form');
    $('#mobile-search-btn').click( function (e) {
        if($('#mobile-location-txt').val() === null){
            $('#noSearchTerm').modal();
            e.preventDefault();
        }
        else{
            $('.processingForm').show();
            $('.search-btn-label').hide();

        }
    })
</script>
<script>
    function searchInnerCategoryTab(type){
        document.getElementById("search-inner-form").action = "{{ url('/findProperties') }}/"+type;
        if(type === 'for-sale'){
            $('.tab-for-sale').addClass('active');
            $('.tab-to-rent').removeClass('active');
        }
        if(type === 'to-rent'){
            $('.tab-to-rent').addClass('active');
            $('.tab-for-sale').removeClass('active');

        }
    }

    function searchMobileCategoryTab(type){
        document.getElementById("mobile-search-form").action = "{{ url('/findProperties') }}/"+type;
        if(type === 'for-sale'){
            $('.tab-for-sale').addClass('active');
            $('.tab-to-rent').removeClass('active');
        }
        if(type === 'to-rent'){
            $('.tab-to-rent').addClass('active');
            $('.tab-for-sale').removeClass('active');

        }
    }
</script>
<script>
    function changeTo(unit){
        selected_si = $('.selected-si');
        $('.unit').removeClass('active');
        if(unit === 'm2'){
            selected_si.replaceWith('<span class="selected-si">m<sup>2</sup></span>')
            $('.m2').addClass('active');
        }
        if(unit === 'ac'){
            selected_si.replaceWith('<span class="selected-si">ac</span>')
            $('.ac').addClass('active');
        }
        if(unit === 'ha'){
            selected_si.replaceWith('<span class="selected-si">ha</span>')
            $('.ha').addClass('active');
        }
        $('.area-button').click();
    }
</script>
<script>
    $('#mobile-location-txt').select2({
        placeholder: "Search for a City or Suburb...",
        minimumInputLength: '3',
        language: {
            inputTooShort: function(args) {
                return "Please enter at least 3 characters to begin seeing results";
            }
        }
    });
</script>
<script>
    locationTxt = $('.location-select');
    locationTxt.select2({
        placeholder: "Search for a City or Suburb...",
        minimumInputLength: '3',
        ajax:{
            url:'{{ route('searchLocations') }}',
            dataType: 'json',
            method: 'get',
            _token: '{{ csrf_token() }}',
            data: function (params) {
                return{
                    q: $.trim(params.term)
                };
            },
            processResults: function (data){
                return{
                    results: data
                };
            },
            cache: true
        },
        escapeMarkup: function(markup){
            return markup;
        },
        tags: false,
        templateSelection: formatSelection,
        language: {
            inputTooShort: function(args) {
                return "Please enter at least 3 characters to begin seeing results";
            }
        },

    });
    function formatSelection(val) {
        console.log(val);
        if(val.display){
            return val.display;
        }
        else{
            return val.text;
        }
    }
</script>

<script>
    function validate(element){
        if(element === 'area'){
            value = $('#search-area-txt').val();
            var regx = /^[0-9]+$/;
            if (regx.test(value) === false){
                $('#search-area-txt').val("");
                $('#home-area-message').replaceWith('<span id="home-area-message" class="error-text">Area should be a numeric value</span>');
            }
            else{
                $('#home-area-message').replaceWith('<span id="home-area-message" class="error-text"></span>');
            }
        }
    }
</script>
<script>
    /*  PriceRangeLabel =  document.getElementById("search-price-label");
      PriceRangeLabel.innerHTML ="$ "+  $('#search-price-range').val();
      function updateSearchPrice(){
          mobilePriceRange = $('#search-price-range').val();
          if( mobilePriceRange === '0'){
              PriceRangeLabel.innerHTML = "$ "+ mobilePriceRange;
          }
          if(mobilePriceRange > 100){
              PriceRangeLabel.innerHTML = "$ 100 - $ "+ mobilePriceRange;
          }
          if(mobilePriceRange < 100){
              PriceRangeLabel.innerHTML = "$ "+ mobilePriceRange +" -  $ 100";
          }
      }*/
</script>
<script>
    window.onscroll = function() {myFunction()};

    var header = document.getElementById("main-header");
    var sticky = header.offsetTop;

    function myFunction() {
        if (window.pageYOffset > sticky) {
            header.classList.add("sticky");
        } else {
            header.classList.remove("sticky");
        }
    }
</script>
<script>
    footer_subscribe = $('#form-subscribe');
    $('#btn-submit-subscription').click(function (e){
        e.preventDefault();
        footer_subscribe.submit();
    });
    footer_subscribe.submit(function (e){
        $('#btn-subscribe-text').hide();
        $('#subscribing').show();
        $('.form-group').removeClass('has-error');
        $('.help-block').text('');
        $.ajax({
            url:'{{ route('newsletterSubscription') }}',
            data:  new FormData(this),
            method:'POST',
            processData: false,
            contentType: false,
            success: function (data) {
                $('#btn-subscribe-text').hide();
                $('#subscribing').hide();
                $('#subscribed').show();
                footer_subscribe[0].reset();
                $('#btn-submit-subscription').hide();
                $('.form-group').removeClass('has-error');
                $('.help-block').text('');
            },
            error: function (data) {
                $('#btn-subscribe-text').show();
                $('#subscribing').hide();
                var errorData = jQuery.parseJSON(data.responseText);
                var errors = errorData.errors;
                $.each(errors,function (index,value) {
                    $('.'+index).addClass('has-error');
                    $('.error-'+index).text(value);
                });
            }
        }),
            e.preventDefault();
    })
</script>

@yield('scripts')
</body>
</html>
