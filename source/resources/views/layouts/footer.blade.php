<footer class="main-footer clearfix">
    <div class="container">
        <!-- Footer info-->
        <div class="footer-info text-center">
            <div class="row">
                <!--- Subscribe -->
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="footer-item">
                        <div class="main-title-2">
                            <h1>Subscribe</h1>
                        </div>
                        <div class="newsletter clearfix text-left">
                            <p>
                                Subscribe to our Propertybook Cloud mailing list to receive the latest on property news and updates
                             </p>

                            <form method="POST" id="form-subscribe">
                                @csrf
                                <div class="form-group">
                                    <input class="nsu-field btn-block" id="nsu-email-0" type="text" name="email" placeholder="Email Address">
                                    <span id="error-email" class="help-block error-email"></span>
                                </div>
                                <div class="form-group">
                                    <input class="nsu-field btn-block" id="nsu-Phone-0" type="text" name="phone_number" placeholder="Phone Number">
                                    <span id="error-phone_number" class="help-block error-phone_number"></span>
                                </div>
                                <div class="form-group mb-0 text-center">
                                    <button class="button-sm button-theme btn-block" id="btn-submit-subscription">
                                        <span id="btn-subscribe-text"> Subscribe</span>
                                        <span id="subscribing" style="display: none"><i class="fa fa-spinner fa-spin"></i></span>
                                    </button>
                                    <span id="subscribed" style="display: none"><i class="fa fa-check"></i> Subscribed</span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- Links -->
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="footer-item">
                        <div class="main-title-2">
                            <h1>IMPORTANT LINKS</h1>
                        </div>
                        <div class="row text-left">
                            <div class="col-md-2 col-lg-3 col-xs-12">

                            </div>
                            <div class="col-md-8 col-lg-8 col-sm- col-xs-12">
                                <ul class="links">
                                    <li>
                                        <a href="{{ pbLink().'/info/about-us' }}">About Us</a>
                                    </li>
                                    <li>
                                        <a href="{{ pbLink().'/info/contact-us' }}">Contact Us</a>
                                    </li>
                                    <li>
                                        <a href="{{ pbLink().'/info/disclaimer' }}">Disclaimer</a>
                                    </li>
                                    <li>
                                        <a href="{{ pbLink().'/info/terms-and-conditions' }}">Terms & Conditions</a>
                                    </li>
                                    <li>
                                        <a href="{{ pbLink().'/info/privacy-policy' }}">Privacy</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- About us -->
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="footer-item">
                        <div class="main-title-2">
                            <h1>OTM is Powered By</h1>
                        </div>

                        <ul class="personal-info">
                            <li id="bottom-logo">
                                <img src="{{ URL::asset('assets/img/cloud-logo.png') }}">
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
</footer>

<!-- Copy right start -->
<div class="copy-right">
    <div class="container">
        <div class="row clearfix">
            <div class="col-md-12 col-sm-12 text-center">
                &copy; {{ date('Y') }} This site is protected by copyright and trademark laws under Zimbabwe and International Law.
            </div>
        </div>
    </div>
</div>
<!-- Copy end right-->
