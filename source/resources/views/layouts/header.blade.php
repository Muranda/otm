<!-- Main header start -->
<header id="main-header" class="main-header">
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="navbar-header ">
                <button onclick="displayTabletMenu()" type="button" class="navbar-toggle collapsed custom-collapsed" data-toggle="collapse" data-target="#app-navigation" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a id="currency-changer-link" href="javascript:void(0)" class="navbar-toggle collapsed custom-collapsed-search-icon" data-toggle="collapse" data-target=".currency-container"  aria-expanded="false" >
                    <span id="currency-changer-span">
                        <img src="{{ URL::asset('/assets/img/change-currency-blue.png') }}" class="img-fluid" alt="currency-changer"/>
                    </span>
                </a>
                <a id="search-icon" type="button" class="navbar-toggle collapsed {{ $active == 'home' ? 'hide': '' }} custom-collapsed-2" data-toggle="collapse" data-target="#mobile-search" aria-expanded="false">
                    <i class="fa fa-search"></i>
                </a>

                <a href="{{ route('homePage') }}" class="logo-class">
                    <img src="{{ imageURL().'sites/'.($site->website_logo ? $site->website_logo : $site->logo) }}"/>
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="navbar-collapse collapse" role="navigation" aria-expanded="true" id="app-navigation">
                <ul class="nav navbar-nav">
                    <li class="dropdown {{ ($active == 'for-sale' ? 'active' : '') }}" >
                        <a  href="{{ route('all-properties',['filter' => 'for-sale']) }}" class="otm-nav">
                            <span class="nav-links">For Sale</span>
                        </a>
                    </li>
                    <li class="dropdown {{ ($active == 'to-rent' ? 'active' : '') }}">
                        <a class="otm-nav" href="{{ route('all-properties',['filter' => 'to-rent']) }}">
                            <span class="nav-links">To Rent</span>
                        </a>
                    </li>

                    <li class="dropdown ">
                        <a href="{{ cloudRegURL() }}" class=" btn" id="list-property-link" target="_blank">
                            <span class="otm-button add-button" style="padding: 1rem">
                                 <i class="fa fa-plus"></i> LIST FOR FREE
                            </span>
                            <span class="drop-list-property">
                                 <i class="fa fa-plus"></i> LIST FOR FREE
                            </span>
                        </a>
                    </li>
                    <li class="dropdown mobile-hide">
                        <a  aria-expanded="false" id="currency-changer-link" data-toggle="collapse" data-target=".currency-container">
                            <span id="currency-changer-span">
                                <img src="{{ URL::asset('/assets/img/change-currency-blue.png') }}" class="img-fluid" alt="currency-changer"/>
                            </span>
                        </a>
                    </li>
                    <li class="dropdown login-icon-link">
                        <a id="login-icon-link" href="{{ cloudURL().'/login' }}">
                            <span id="login-icon" class="mobile-hide">
                                <img src="{{ URL::asset('/assets/img/login-icon-blue.png') }}" class="img-fluid" alt="login-icon"/>
                            </span> Log in
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</header>
<div  class=" for-tablet-search navbar-collapse collapse" role="navigation" aria-expanded="true" id="mobile-search">
    <div class="row">
        <div class="container">
            <div class="row hidden-lg hidden-md">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  ">
                    <form method="POST" id="mobile-search-form" action="{{ route('findProperties',['filter'=>'for-sale']) }}">
                        @csrf
                        <h3 class="mobile-search-heading">Search for Properties in Zimbabawe</h3>
                        <!-- #search filerter is selected by the active variable -->
                        <div class="row">
                            <div class="col-md-6 col-lg-3 col-sm-6 col-xs-12">
                                <div class="property-types-tabs text-left">
                                    <ul class="list-inline">
                                        <li class="search-type">
                                            <a class="tab-for-sale active" onclick="searchMobileCategoryTab('for-sale')">For Sale</a>
                                        </li>
                                        <li class="search-type">
                                            <a class="tab-to-rent" onclick="searchMobileCategoryTab('to-rent')">To Rent</a>
                                        </li>
                                    </ul>
                                </div>
                                <input type="text" id="search-filter" class="hidden" name="searchType" value="for-sale" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="mobile form-group locations">
                                    <select id="mobile-location-txt" class="form-control location-select search-fields" multiple="multiple"  name="searchText[]" data-live-search="true" data-live-search-placeholder="Search Location">
                                        <option>Select Location</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="mobile form-group bed-bath">
                                    <select class="selectpicker search-fields" name="property-types" data-live-search="true" data-live-search-placeholder="Search value">
                                        <option>Property Types</option>
                                        <option>Residential</option>
                                        <option>Commercial</option>
                                        <option>Land</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="mobile form-group bed-bath">
                                    <select class="selectpicker search-fields" name="bedrooms" data-live-search="true" data-live-search-placeholder="Search value" >
                                        <option>Bedrooms</option>
                                        <option>1+</option>
                                        <option>2+</option>
                                        <option>3+</option>
                                        <option>4+</option>
                                        <option>5+</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="mobile form-group bed-bath ">
                                    <select id="baths" class="selectpicker search-fields bathrooms" name="bathrooms" data-live-search="true" data-live-search-placeholder="Search value" >
                                        <option>Bathrooms</option>
                                        <option value="1">1+ </option>
                                        <option value="2">2+ </option>
                                        <option value="3">3+ </option>
                                        <option value="4">4+ </option>
                                        <option value="5">5+ </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="mobile form-group bed-bath ">
                                    <select class="selectpicker search-fields" name="bedrooms">
                                        <option>Min Price</option>
                                        @foreach($priceFilters as $price)
                                            <option>{{ ($selected_currency == 'usd' ? $price->usd : $price->zwl)  }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="mobile form-group bed-bath ">
                                    <select class="selectpicker search-fields" name="bedrooms">
                                        <option>Max Price</option>
                                        @foreach($priceFilters as $price)
                                            <option>{{ ($selected_currency == 'usd' ? $price->usd : $price->zwl)  }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <button type="submit" id="mobile-search-btn" class="btn search-button">
                                <span  class="processingForm" style="display: none">
                                     <i class="fa fa-spinner fa-spin"></i>
                                </span>
                                    Search
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!----------- Start of ipad dropdown menu ----------------->

<div   class=" for-tablet-search navbar-collapse s" role="navigation" aria-expanded="true" id="tablet-menu">
    <ul class="nav navbar-nav">
        <li class="dropdown {{ ($active == 'for-sale' ? 'active' : '') }}" >
            <a  href="{{ route('all-properties',['filter' => 'for-sale']) }}" class="otm-nav">
                <span class="nav-links">FOR SALE</span>
            </a>
        </li>
        <li class="dropdown {{ ($active == 'to-rent' ? 'active' : '') }}">
            <a class="otm-nav" href="{{ route('all-properties',['filter' => 'to-rent']) }}">
                <span class="nav-links">TO RENT</span>
            </a>
        </li>

        <li class="dropdown ">
            <a href="{{ cloudRegURL() }}" id="list-property-link" target="_blank">
                <span class="drop-list-property">
                     <i class="fa fa-plus"></i> LIST FOR FREE
                </span>
            </a>
        </li>

        <li class="dropdown login-icon-link">
            <a id="login-icon-link" href="{{ cloudURL().'/login' }}"> LOG IN
            </a>
        </li>
    </ul>
</div>
<div class="currency-container collapse">
    <div class="currency-menu">
        <ul>
            <li class="li-currency {{ ($selected_currency == 'usd' ? 'active' : '') }}" >
                <a href="javascript:void(0)" onclick="updateCurrency('usd')">USD - Foreign Transactions</a>
            </li>
            <li class="li-currency {{ ($selected_currency == 'zwl' ? 'active' : '') }} ">
                <a href="javascript:void(0)" onclick="updateCurrency('zwl')">ZWL - Local Transactions</a>
            </li>
        </ul>
    </div>
</div>
<script>
    function updateCurrency(currency){
        $.ajax({
            url: "{{ route('changeCurrency') }}",
            success: function(data){
               location.reload(true);
            },
            error: function(){
                alert("Currency not updated");
            }
        });
    }
</script>
<script>

    function displayTabletMenu(){
        if(screen.width <= 834 && screen.width >=768){
            $('#tablet-menu').toggle('slideDown');
        }
    }

</script>



<!-- Main header end -->

