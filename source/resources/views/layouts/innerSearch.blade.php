<?php

if(isset($searchQuery['parameters']['minPrice'])){
    $minPriceSearch = $searchQuery['parameters']['minPrice'];
}
else{
    $minPriceSearch = '';
}

if(isset($searchQuery['parameters']['maxPrice'])){
    $maxPriceSearch = $searchQuery['parameters']['maxPrice'];
}
else{
    $maxPriceSearch = '';
}

if(isset($searchQuery['parameters']['propertyArea'])){
    $propertyArea = $searchQuery['parameters']['propertyArea'];
}
else{
    $propertyArea = '';
}


if(isset($searchQuery['parameters']['baths'])){
    $bathrooms = $searchQuery['parameters']['baths'];
}
else{
    $bathrooms = '';
}

if(isset($searchQuery['parameters']['beds'])){
    $beds = $searchQuery['parameters']['beds'];
}
else{
    $beds = '';
}
if(isset($searchQuery['parameters']['type'])){
    $searchType = $searchQuery['parameters']['type'];
}
else{
    $searchType = '';
}

$innerSearch = 'set';

$route = '#';


?>
<!-- Sub banner start -->
<div class="sub-banner overview-bgi">
    <div class="overlay">
        <div class="container">
            <div class="breadcrumb-area">
                <div class="inner-search-container container">
                    <div class="inner-search">
                        <form method="POST" id="inner-search-form" action="{{ route('findProperties',['filter'=>$active]) }}">
                        @csrf
                        <!-- #search filerter is selected by the active variable -->
                            <input type="text" id="search-filter" class="hidden" name="searchType" value="{{ $active }}" />
                            <!-- end #search filerter is selected by the active variable -->
                            <div class="row">
                                <div class="col-xs-5 col-sm-5 col-md-4 col-lg-5 custom-left-padding">
                                    <div class="form-group innerSearch-location">
                                        <select id="location-txt" class="form-control location-select   search-fields" multiple="multiple"  name="searchText[]" data-live-search="true"  data-live-search-placeholder="Search Location" >
                                            <option>Select Location</option>
                                            @if(isset($searchQuery['suburbs']))
                                                @foreach($searchQuery['suburbs'] as $place)
                                                    <option value="{{ $place['id'] }}" selected="selected">{{ $place['display'] }}</option>
                                                @endforeach
                                            @endif
                                            @if(isset($searchQuery['areas']))
                                                @foreach($searchQuery['areas'] as $place)
                                                    <option value="{{ $place['id'] }}" selected="selected">{{ $place['display'] }}</option>
                                                @endforeach
                                            @endif
                                            @if(isset($searchQuery['towns']))
                                                @foreach($searchQuery['towns'] as $place)
                                                    <option value="{{ $place['id'] }}" selected="selected">{{ $place['display'] }}</option>
                                                @endforeach
                                            @endif
                                            @if(isset($searchQuery['provinces']))
                                                @foreach($searchQuery['provinces'] as $place)
                                                    <option value="{{ $place['id'] }}" selected="selected">{{ $place['display'] }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-7 col-md-8 col-lg-7 search-parameter remove-padding">
                                    <div class="parameters property-content search">
                                        <ul class="innerSearch-facilities-list ">
                                            <li>
                                                <div class="form-group width-11" >
                                                    <select class="selectpicker search-fields" name="minPrice">
                                                        <option value="">Min Price</option>
                                                        @foreach($priceFilters as $price)
                                                            <option value="{{ ($selected_currency == 'usd' ? $price->usd : $price->zwl)  }}" {{ ($selected_currency == 'usd' ? ($minPriceSearch == $price->usd ? 'selected' : '') : ($minPriceSearch == $price->zwl ? 'selected' : '')) }}>
                                                                {{ ($selected_currency == 'usd' ? $price->usd : $price->zwl)  }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </li>

                                            <li>
                                                <div class="form-group width-11" >
                                                    <select class="selectpicker search-fields" name="maxPrice">
                                                        <option value="">Max Price</option>
                                                        @foreach($priceFilters as $price)
                                                            <option value="{{ ($selected_currency == 'usd' ? $price->usd : $price->zwl)  }}" {{ ($maxPriceSearch == ($selected_currency == 'usd' ? $price->usd : $price->zwl) ? 'selected' : '' )}} >{{ ($selected_currency == 'usd' ? $price->usd : $price->zwl)  }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </li>
                                            <li>

                                                <div class="form-group width-11" >
                                                    <select class="selectpicker search-fields" name="baths">
                                                        <option>Beds</option>
                                                        <option value="1" {{ ($bathrooms == '1' ? 'selected' : '') }}>1+</option>
                                                        <option value="2" {{ ($bathrooms == '2' ? 'selected' : '' )}}>2+</option>
                                                        <option value="3" {{ ($bathrooms == '3' ? 'selected' : '' )}}>3+</option>
                                                        <option value="4" {{ ($bathrooms == '4' ? 'selected' : '' )}}>4+</option>
                                                        <option value="5" {{ ($bathrooms == '5' ? 'selected' : '' )}}>5+</option>
                                                    </select>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-group width-11">
                                                    <select class="selectpicker search-fields" name="beds">
                                                        <option>Baths</option>
                                                        <option value="1" {{ ($beds == '1' ? 'selected' : '' )}}>1+</option>
                                                        <option value="2" {{ ($beds == '2' ? 'selected' : '' )}}>2+</option>
                                                        <option value="3" {{ ($beds == '3' ? 'selected' : '' )}}>3+</option>
                                                        <option value="4" {{ ($beds == '4' ? 'selected' : '' )}}>4+</option>
                                                        <option value="5" {{ ($beds == '5' ? 'selected' : '' )}}>5+</option>
                                                    </select>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-group width-11">
                                                    <select class="selectpicker search-fields" name="propertyType[]">
                                                        <option value="">Type</option>
                                                       @foreach($globalPropertyTypes as $globalPropertyType)
                                                            <option value="{{ $globalPropertyType->id }}" {{ ($searchType == $globalPropertyType->id? 'selected' : '' ) }}>{{$globalPropertyType->type}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </li>
                                        </ul>
                                        <div class="inner-search-btn">
                                            <button type="submit" id="inner-search-btn" class="btn search-button">
                                                <span  class="processingForm" style="display: none">
                                                     <i class="fa fa-spinner fa-spin"></i>
                                                </span>
                                                <i class="fa fa-search search-btn-label"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Sub Banner end -->
