{{--<nav aria-label="Page navigation" class="text-center">
    <ul class="pagination">
        <li>
            <a class="nav-arrow" href="properties-list-leftside.html" aria-label="Previous">
                <span>&#xab;</span>
            </a>
        </li>
        <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
        <li><a href="properties-list-leftside.html">2</a></li>
        <li><a href="properties-list-fullwidth.html">3</a></li>
        <li>
            <a class="nav-arrow" href="properties-list-fullwidth.html" aria-label="Next">
                <span>»</span>
            </a>
        </li>
    </ul>
</nav>--}}
@if ($paginator->hasPages())
    <nav aria-label="Page navigation" class="text-center">
        <ul class="pagination">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                {{--<li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                    <span class="page-link" aria-hidden="true">&lsaquo;</span>
                </li>--}}
                <li>
                    <a class="nav-arrow" aria-disabled="true" href="javascript:void(0)" aria-label="Previous">
                        <span>&#xab;</span>
                    </a>
                </li>
            @else
               {{-- <li class="page-item">
                    <a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')">&lsaquo;</a>
                </li>--}}
                <li>
                    <a class="nav-arrow" href="{{ $paginator->previousPageUrl() }}" aria-label="Previous">
                        <span>&#xab;</span>
                    </a>
                </li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="page-item disabled" aria-disabled="true"><span class="page-link">{{ $element }}</span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            {{--<li class="page-item active" aria-current="page"><span class="page-link">{{ $page }}</span></li>
                            --}}<li class="active"><a href="javascript:void(0)">{{ $page }}<span class="sr-only">(current)</span></a></li>
                        @else
                           {{-- <li class="page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                           --}} <li><a href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
               {{-- <li class="page-item">
                    <a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">&rsaquo;</a>
                </li>--}}
                <li>
                    <a class="nav-arrow" href="{{ $paginator->nextPageUrl() }}" aria-label="Next">
                        <span>»</span>
                    </a>
                </li>
            @else
              {{--  <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                    <span class="page-link" aria-hidden="true">&rsaquo;</span>
                </li>--}}
                <li>
                    <a class="nav-arrow" href="javascript:void(0)" aria-label="Next">
                        <span>»</span>
                    </a>
                </li>
            @endif
        </ul>
    </nav>
@endif
