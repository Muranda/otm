@extends('layouts.master')

@section('site-content')
    <div class="hidden-xs hidden-sm">
        @include('layouts.innerSearch')
    </div>
    <!-- Properties section body start -->
    <div class="properties-section-body content-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-xs-12 col-sm-12">
                    <!-- Option bar start -->
                    <div class="option-bar">
                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 remove-Lpadding-mobile">
                                <h4>
                                    <span class=" list-heading"> {{$pageTitle}} </span>
                                </h4>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cod-pad text-right remove-Rpadding-mobile">
                                <div class="sorting-options form-group">
                                    <div class="row">
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 remove-Rpadding-mobile">
                                            <div class="text-left">
                                                <ul class="list-inline">
                                                    <li>
                                                        <label class="label-orderby"><span>Order By:<span</label>
                                                    </li>
                                                    <li>
                                                        <select class="selectpicker sorting" id="listingSorter">
                                                            <option value="default" {{ ($sortValue == 'sortBy=default' ? 'selected' : '') }}>Default</option>
                                                            <option value="price-low" {{ ($sortValue == 'sortBy=price-low' ? 'selected' : '') }}>Price (High To Low)</option>
                                                            <option value="price-high" {{ ($sortValue == 'sortBy=price-low' ? 'selected' : '') }}>Price (Low To High)</option>
                                                        </select>
                                                    </li>
                                                </ul>

                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <div class=" hidden-lg hidden-md">
                                                <h4 class="orderby-heading">
                                                    <span class="resutls">14 </span>
                                                    <span>Results</span>
                                                </h4>
                                            </div>
                                            <div class="hidden-sm hidden-xs">
                                                <h4 class="orderby-heading-desktop"> Showing {{ $properties->firstItem() .' - '. $properties->lastItem()}} of {{ $properties->total()}}</h4>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <!-- Option bar end -->

                    <div class="clearfix"></div>
                    <!-- Property start -->
                    @if(isset($featured))
                        @foreach($featured as $featuredProperty)
                            <div class="featured-listing-card property clearfix wow fadeInUp delay-03s">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 col-pad">
                                    <!-- Property img -->
                                    <div class="property-img listing-img">
                                        <div class="custom-property-tag button alt featured">Featured</div>
                                        <div class="property-tag button sale">{{ $featuredProperty->property->offer->label }}</div>
                                        <picture>
                                            <source srcset="{{resolvePropertyImage('listings',$featuredProperty->property->images->main_image,$featuredProperty->property,'webp') }}" type="image/webp">
                                            <img src="{{resolvePropertyImage('listings',$featuredProperty->property->images->main_image,$featuredProperty->property,'standard') }}" alt="{{ $featuredProperty->pageTitle  }}" class="img-responsive">
                                        </picture>
                                        <div class="featured-property-price">{{ displayPrice($featuredProperty->property) }}</div>
                                    </div>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 property-content ">
                                    <!-- Agent Logo -->
                                    <div class="agency-logo">
                                        <picture>
                                            <source srcset="{{ imageURL().'agency-logos/'.$featuredProperty->property->agents->agentDetail->agency->logo }}" type="image/webp">
                                            <img src="{{ imageURL().'agency-logos/'.$featuredProperty->property->agents->agentDetail->agency->logo }}" class="img-responsive" alt="{{ $featuredProperty->property->agents->agentDetail->agency_name  }}">
                                        </picture>
                                    </div>
                                    <!-- title -->
                                    <h1 class="title" >
                                    <span href="properties-details.html">
                                        {{ generatePropertyTitle($featuredProperty->property->type_id,$featuredProperty->property->type->type,$featuredProperty->property->offer->label,$featuredProperty->property->suburb->name)  }}
                                    </span>
                                    </h1>
                                    <!-- Property address -->
                                    <h3 class="property-address">
                                        <a href="{{ propertyLink($featuredProperty->property) }}">
                                            <i class="fa fa-map-marker-alt"></i>
                                            {{ $featuredProperty->property->suburb->area->name.' , '.$featuredProperty->property->suburb->area->town->province->name  }}
                                        </a>
                                    </h3>
                                    <!-- Facilities List -->
                                    <ul class="featured-listing custom-facilities-list clearfix">
                                        @foreach(iconDisplay($featuredProperty->property->features ) as $key => $value)
                                            <li class="li-line-height">
                                                <i class="@if($key == "boreholes" )fa fa-tint @endif
                                                @if($key == "bedrooms") fa fa-bed @endif
                                                @if($key == "bathrooms") fa fa-bath @endif
                                                @if($key == "lounges") fa fa-couch @endif
                                                    ">
                                                </i>
                                                <span> {{ $value }} @if($key == "bedrooms") {{ ($value > 1 ? 'beds' : 'bed') }}
                                                    @elseif($key == "bathrooms") {{ ($value > 1 ? 'baths' : 'bath')}}
                                                    @elseif($key == "lounges") {{ ($value > 1 ? 'lounges' : 'lounge')}}
                                                    @endif
                                                </span>
                                                <span class="pipe {{ ($loop->last ? 'hidden' :'') }} ">|</span>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="second-heading">
                                        <h3 class="title agent-title">{{ $featuredProperty->property->title }}</h3>
                                    </div>
                                    <div class="listing description text-left one-px-left" >
                                        {{  Str::limit($featuredProperty->property->description, 110)  }}
                                    </div>
                                    <!-- Property footer -->
                                </div>
                                @php
                                    $primaryAgent = $featuredProperty->property->agents->user
                                @endphp

                                <div class="custom-property-footer text-right">
                            <span class="left hidden-lg hidden-md hidden-sm">

                                <a class="call-link btn btn-primary viewmore-link"  href="Tel: {{$primaryAgent->country_code.''.$primaryAgent->phone_number}} " ><i class="fa fa-phone"></i> Call</a>
                            </span>
                                    <span class="right">
                               <a class="btn btn-primary viewmore-link" href="{{ propertyLink($featuredProperty->property) }}">View More</a>
                            </span>
                                </div>
                            </div>
                        @endforeach
                    @endif

                    @foreach( $properties as $normalListing)
                            <div class="normal-listing property clearfix wow fadeInUp delay-03s">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 col-pad">
                                    <div class="property-img">
                                        <div class="custom-property-tag button alt featured {{ ($normalListing->priority->first()  ? '' : 'hide') }} ">Priority</div>
                                        <picture>
                                            <source srcset="{{resolvePropertyImage('listings',$normalListing->images->main_image,$normalListing,'webp') }}" type="image/webp">
                                            <img src="{{resolvePropertyImage('listings',$normalListing->images->main_image,$normalListing,'standard') }}" alt="{{ $normalListing->pageTitle  }}" class="img-responsive">
                                        </picture>
                                    </div>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 property-content ">
                                    <!-- Agent Logo -->
                                    <div class="agency-logo">
                                        @php($agency = $normalListing->agents->agentDetail)
                                        <picture>
                                            <source srcset="{{ ( $agency ? getWebPImage('agencyLogo',$agency->agency->logo)  : imageURL().'private-lister-logo.png') }}" type="image/webp">
                                            <img src="{{ imageURL().($agency ? 'agency-logos/'.$agency->agency->logo : 'private-lister-logo.png')  }}" class="img-responsive" alt="{{ ($agency ? $agency->agency->name : 'Private Lister')  }}">
                                        </picture>
                                    </div>
                                    <!-- title -->
                                    <h1 class="title">
                                        <span href="properties-details.html">
                                            {{ generatePropertyTitle($normalListing->type_id,$normalListing->type->type,$normalListing->offer->label,$normalListing->suburb->name)  }}
                                        </span>
                                    </h1>
                                    <!-- Property address -->
                                    <h3 class="property-address">
                                        <a>
                                            <i class="fa fa-map-marker-alt"></i>
                                            {{ $normalListing->suburb->area->name.' , '.$normalListing->suburb->area->town->province->slug  }}
                                        </a>
                                    </h3>
                                    <!-- Facilities List -->
                                    <!-- Facilities List -->
                                    <ul class="normal-listing custom-facilities-list clearfix">
                                        @foreach(iconDisplay( $normalListing->features ) as $key => $value)
                                            <li>
                                                <i class="@if($key == "boreholes" )fa fa-tint @endif
                                                @if($key == "bedrooms") fa fa-bed @endif
                                                @if($key == "bathrooms") fa fa-bath @endif
                                                @if($key == "lounges") fa fa-couch @endif
                                                    ">
                                                </i>
                                                <span>{{ $value }}</span>
                                                <span class="pipe {{ ($loop->last ? 'hidden' :'') }} ">|</span>
                                            </li>
                                        @endforeach

                                    </ul>
                                    <div class="second-heading">
                                        {{ $normalListing->propertytitle }}
                                    </div>

                                    <div class="description text-left">
                                        {{  Str::limit($normalListing->description, 105)  }}
                                    </div>

                                    <!-- Property footer -->

                                    <div class="property-footer">
                                        <span class="left the-price">
                                            {{ displayPrice($normalListing) }}
                                        </span>
                                        <span class="right view-more">
                                            <a class="btn btn-primary" href="{{ propertyLink($normalListing) }}">More Details</a>
                                        </span>
                                    </div>
                                </div>
                            </div>

                    @endforeach
                    {{ $properties->links('layouts.pagination') }}
                   <!-- Property end -->
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <!-- Category posts start -->
                    <div class="listing sidebar-widget category-posts">
                        <div class="main-title-2">
                            <h1><span>popular</span> Areas</h1>
                        </div>
                        <ul class="list-unstyled list-cat">
                            @foreach( popularLocationsSidebar() as $location)
                                <li>
                                    <a href="{{ route('filterProperties',['filter'=>$active,'province'=>$location->area->town->province->slug,'town'=>$location->area->town->slug,'area'=>$location->area->slug,'suburb'=>$location->slug]) }}">
                                        {{ $location->name  }}
                                        <span> ({{ $location->properties_count }})  </span>
                                    </a>
                                 </li>
                            @endforeach
                        </ul>
                    </div>

                    <!-- Popular posts start -->
                    <div class="sidebar-widget popular-posts hidden-xs hidden-sm">
                        <div class="main-title-2">
                            <h1><span>Join</span> Our Community</h1>
                        </div>
                        <div class="sidebar-subscribe">
                            <Form id="form-subscribe" Method="POST">
                                @csrf
                                    <div class="form-group">
                                        <input type="text" name="email" class="form-control" placeholder="Email Address ........."/>
                                        <span id="error-email" class="help-block error-email"></span>
                                    </div>
                                <div class="form-group">
                                    <input type="text" name="phone_number" class="form-control" placeholder="WhatsApp Number"/>
                                    <span id="error-phone_number" class="help-block error-phone_number"></span>
                                </div>
                                <div class="form-group text-center">
                                    <button class="button-sm button-theme btn-block" id="btn-submit-subscription">
                                        <span id="btn-subscribe-text"> Subscribe</span>
                                        <span id="subscribing" style="display: none"><i class="fa fa-spinner fa-spin"></i></span>
                                    </button>
                                    <span id="subscribed" style="display: none"><i class="fa fa-check"></i> Subscribed</span>
                                </div>
                            </Form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Properties section body end -->
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('#listingSorter').on('change',function () {
                var str = '{{ URL::full() }}';

                var link = str.replace('{{ $sortValue }}','');
                var url = '';

                if(str.indexOf('?') >= 0 ){
                    url = link+'&sortBy='+$('#listingSorter').val();
                }
                else{
                    url = link+'?sortBy='+$('#listingSorter').val();
                }
                var final = url.replace(/amp;/g,'');
                window.location.href= final.replace(/&&/g,'&');
            });
        });
    </script>
@endsection
